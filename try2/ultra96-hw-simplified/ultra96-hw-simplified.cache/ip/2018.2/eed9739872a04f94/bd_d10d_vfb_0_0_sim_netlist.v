// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Sat Feb  1 12:31:52 2020
// Host        : tester2-ThinkPad-X1-Extreme-2nd running 64-bit Ubuntu 18.04.3 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ bd_d10d_vfb_0_0_sim_netlist.v
// Design      : bd_d10d_vfb_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xczu3eg-sbva484-1-i
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_dwidth_converter_v1_1_16_axis_dwidth_converter
   (Q,
    m_axis_tlast,
    m_axis_tid,
    m_axis_tdata,
    m_axis_tkeep,
    m_axis_tuser,
    s_axis_tkeep,
    s_axis_tvalid,
    aclk,
    m_axis_tready,
    s_axis_tdata,
    s_axis_tlast,
    s_axis_tid,
    s_axis_tuser,
    aresetn);
  output [1:0]Q;
  output m_axis_tlast;
  output [31:0]m_axis_tid;
  output [15:0]m_axis_tdata;
  output [1:0]m_axis_tkeep;
  output [5:0]m_axis_tuser;
  input [3:0]s_axis_tkeep;
  input s_axis_tvalid;
  input aclk;
  input m_axis_tready;
  input [31:0]s_axis_tdata;
  input s_axis_tlast;
  input [31:0]s_axis_tid;
  input [11:0]s_axis_tuser;
  input aresetn;

  wire [1:0]Q;
  wire aclk;
  wire areset_r;
  wire areset_r_i_1_n_0;
  wire aresetn;
  wire [15:0]m_axis_tdata;
  wire [31:0]m_axis_tid;
  wire [1:0]m_axis_tkeep;
  wire m_axis_tlast;
  wire m_axis_tready;
  wire [5:0]m_axis_tuser;
  wire [31:0]s_axis_tdata;
  wire [31:0]s_axis_tid;
  wire [3:0]s_axis_tkeep;
  wire s_axis_tlast;
  wire [11:0]s_axis_tuser;
  wire s_axis_tvalid;

  LUT1 #(
    .INIT(2'h1)) 
    areset_r_i_1
       (.I0(aresetn),
        .O(areset_r_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    areset_r_reg
       (.C(aclk),
        .CE(1'b1),
        .D(areset_r_i_1_n_0),
        .Q(areset_r),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_dwidth_converter_v1_1_16_axisc_downsizer \gen_downsizer_conversion.axisc_downsizer_0 
       (.Q(Q),
        .SR(areset_r),
        .aclk(aclk),
        .m_axis_tdata(m_axis_tdata),
        .m_axis_tid(m_axis_tid),
        .m_axis_tkeep(m_axis_tkeep),
        .m_axis_tlast(m_axis_tlast),
        .m_axis_tready(m_axis_tready),
        .m_axis_tuser(m_axis_tuser),
        .s_axis_tdata(s_axis_tdata),
        .s_axis_tid(s_axis_tid),
        .s_axis_tkeep(s_axis_tkeep),
        .s_axis_tlast(s_axis_tlast),
        .s_axis_tuser(s_axis_tuser),
        .s_axis_tvalid(s_axis_tvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_dwidth_converter_v1_1_16_axisc_downsizer
   (Q,
    m_axis_tlast,
    m_axis_tid,
    m_axis_tdata,
    m_axis_tkeep,
    m_axis_tuser,
    s_axis_tlast,
    aclk,
    SR,
    s_axis_tkeep,
    s_axis_tvalid,
    m_axis_tready,
    s_axis_tdata,
    s_axis_tid,
    s_axis_tuser);
  output [1:0]Q;
  output m_axis_tlast;
  output [31:0]m_axis_tid;
  output [15:0]m_axis_tdata;
  output [1:0]m_axis_tkeep;
  output [5:0]m_axis_tuser;
  input s_axis_tlast;
  input aclk;
  input [0:0]SR;
  input [3:0]s_axis_tkeep;
  input s_axis_tvalid;
  input m_axis_tready;
  input [31:0]s_axis_tdata;
  input [31:0]s_axis_tid;
  input [11:0]s_axis_tuser;

  wire [1:0]Q;
  wire [0:0]SR;
  wire aclk;
  wire [15:0]m_axis_tdata;
  wire [31:0]m_axis_tid;
  wire [1:0]m_axis_tkeep;
  wire m_axis_tlast;
  wire m_axis_tready;
  wire [5:0]m_axis_tuser;
  wire p_0_in;
  wire [31:0]r0_data;
  wire [31:0]r0_id;
  wire [0:0]r0_is_end;
  wire \r0_is_null_r[1]_i_1_n_0 ;
  wire [3:0]r0_keep;
  wire r0_last_reg_n_0;
  wire r0_load;
  wire \r0_out_sel_r[0]_i_1_n_0 ;
  wire \r0_out_sel_r_reg_n_0_[0] ;
  wire [11:0]r0_user;
  wire [15:0]r1_data;
  wire \r1_data[15]_i_1_n_0 ;
  wire [31:0]r1_id;
  wire [1:0]r1_keep;
  wire r1_last_reg_n_0;
  wire [5:0]r1_user;
  wire [31:0]s_axis_tdata;
  wire [31:0]s_axis_tid;
  wire [3:0]s_axis_tkeep;
  wire s_axis_tlast;
  wire [11:0]s_axis_tuser;
  wire s_axis_tvalid;
  wire [2:0]state;
  wire \state_reg_n_0_[2] ;

  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m_axis_tdata[0]_INST_0 
       (.I0(r1_data[0]),
        .I1(\r0_out_sel_r_reg_n_0_[0] ),
        .I2(r0_data[0]),
        .O(m_axis_tdata[0]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m_axis_tdata[10]_INST_0 
       (.I0(r1_data[10]),
        .I1(\r0_out_sel_r_reg_n_0_[0] ),
        .I2(r0_data[10]),
        .O(m_axis_tdata[10]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m_axis_tdata[11]_INST_0 
       (.I0(r1_data[11]),
        .I1(\r0_out_sel_r_reg_n_0_[0] ),
        .I2(r0_data[11]),
        .O(m_axis_tdata[11]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m_axis_tdata[12]_INST_0 
       (.I0(r1_data[12]),
        .I1(\r0_out_sel_r_reg_n_0_[0] ),
        .I2(r0_data[12]),
        .O(m_axis_tdata[12]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m_axis_tdata[13]_INST_0 
       (.I0(r1_data[13]),
        .I1(\r0_out_sel_r_reg_n_0_[0] ),
        .I2(r0_data[13]),
        .O(m_axis_tdata[13]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m_axis_tdata[14]_INST_0 
       (.I0(r1_data[14]),
        .I1(\r0_out_sel_r_reg_n_0_[0] ),
        .I2(r0_data[14]),
        .O(m_axis_tdata[14]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m_axis_tdata[15]_INST_0 
       (.I0(r1_data[15]),
        .I1(\r0_out_sel_r_reg_n_0_[0] ),
        .I2(r0_data[15]),
        .O(m_axis_tdata[15]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m_axis_tdata[1]_INST_0 
       (.I0(r1_data[1]),
        .I1(\r0_out_sel_r_reg_n_0_[0] ),
        .I2(r0_data[1]),
        .O(m_axis_tdata[1]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m_axis_tdata[2]_INST_0 
       (.I0(r1_data[2]),
        .I1(\r0_out_sel_r_reg_n_0_[0] ),
        .I2(r0_data[2]),
        .O(m_axis_tdata[2]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m_axis_tdata[3]_INST_0 
       (.I0(r1_data[3]),
        .I1(\r0_out_sel_r_reg_n_0_[0] ),
        .I2(r0_data[3]),
        .O(m_axis_tdata[3]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m_axis_tdata[4]_INST_0 
       (.I0(r1_data[4]),
        .I1(\r0_out_sel_r_reg_n_0_[0] ),
        .I2(r0_data[4]),
        .O(m_axis_tdata[4]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m_axis_tdata[5]_INST_0 
       (.I0(r1_data[5]),
        .I1(\r0_out_sel_r_reg_n_0_[0] ),
        .I2(r0_data[5]),
        .O(m_axis_tdata[5]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m_axis_tdata[6]_INST_0 
       (.I0(r1_data[6]),
        .I1(\r0_out_sel_r_reg_n_0_[0] ),
        .I2(r0_data[6]),
        .O(m_axis_tdata[6]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m_axis_tdata[7]_INST_0 
       (.I0(r1_data[7]),
        .I1(\r0_out_sel_r_reg_n_0_[0] ),
        .I2(r0_data[7]),
        .O(m_axis_tdata[7]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m_axis_tdata[8]_INST_0 
       (.I0(r1_data[8]),
        .I1(\r0_out_sel_r_reg_n_0_[0] ),
        .I2(r0_data[8]),
        .O(m_axis_tdata[8]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m_axis_tdata[9]_INST_0 
       (.I0(r1_data[9]),
        .I1(\r0_out_sel_r_reg_n_0_[0] ),
        .I2(r0_data[9]),
        .O(m_axis_tdata[9]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[0]_INST_0 
       (.I0(r1_id[0]),
        .I1(r0_id[0]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[0]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[10]_INST_0 
       (.I0(r1_id[10]),
        .I1(r0_id[10]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[10]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[11]_INST_0 
       (.I0(r1_id[11]),
        .I1(r0_id[11]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[11]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[12]_INST_0 
       (.I0(r1_id[12]),
        .I1(r0_id[12]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[12]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[13]_INST_0 
       (.I0(r1_id[13]),
        .I1(r0_id[13]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[13]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[14]_INST_0 
       (.I0(r1_id[14]),
        .I1(r0_id[14]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[14]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[15]_INST_0 
       (.I0(r1_id[15]),
        .I1(r0_id[15]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[15]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[16]_INST_0 
       (.I0(r1_id[16]),
        .I1(r0_id[16]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[16]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[17]_INST_0 
       (.I0(r1_id[17]),
        .I1(r0_id[17]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[17]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[18]_INST_0 
       (.I0(r1_id[18]),
        .I1(r0_id[18]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[18]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[19]_INST_0 
       (.I0(r1_id[19]),
        .I1(r0_id[19]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[19]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[1]_INST_0 
       (.I0(r1_id[1]),
        .I1(r0_id[1]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[1]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[20]_INST_0 
       (.I0(r1_id[20]),
        .I1(r0_id[20]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[20]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[21]_INST_0 
       (.I0(r1_id[21]),
        .I1(r0_id[21]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[21]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[22]_INST_0 
       (.I0(r1_id[22]),
        .I1(r0_id[22]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[22]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[23]_INST_0 
       (.I0(r1_id[23]),
        .I1(r0_id[23]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[23]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[24]_INST_0 
       (.I0(r1_id[24]),
        .I1(r0_id[24]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[24]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[25]_INST_0 
       (.I0(r1_id[25]),
        .I1(r0_id[25]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[25]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[26]_INST_0 
       (.I0(r1_id[26]),
        .I1(r0_id[26]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[26]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[27]_INST_0 
       (.I0(r1_id[27]),
        .I1(r0_id[27]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[27]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[28]_INST_0 
       (.I0(r1_id[28]),
        .I1(r0_id[28]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[28]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[29]_INST_0 
       (.I0(r1_id[29]),
        .I1(r0_id[29]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[29]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[2]_INST_0 
       (.I0(r1_id[2]),
        .I1(r0_id[2]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[2]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[30]_INST_0 
       (.I0(r1_id[30]),
        .I1(r0_id[30]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[30]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[31]_INST_0 
       (.I0(r1_id[31]),
        .I1(r0_id[31]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[31]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[3]_INST_0 
       (.I0(r1_id[3]),
        .I1(r0_id[3]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[3]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[4]_INST_0 
       (.I0(r1_id[4]),
        .I1(r0_id[4]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[4]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[5]_INST_0 
       (.I0(r1_id[5]),
        .I1(r0_id[5]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[5]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[6]_INST_0 
       (.I0(r1_id[6]),
        .I1(r0_id[6]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[6]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[7]_INST_0 
       (.I0(r1_id[7]),
        .I1(r0_id[7]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[7]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[8]_INST_0 
       (.I0(r1_id[8]),
        .I1(r0_id[8]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[8]));
  LUT5 #(
    .INIT(32'hCCACACCC)) 
    \m_axis_tid[9]_INST_0 
       (.I0(r1_id[9]),
        .I1(r0_id[9]),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(m_axis_tid[9]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m_axis_tkeep[0]_INST_0 
       (.I0(r1_keep[0]),
        .I1(\r0_out_sel_r_reg_n_0_[0] ),
        .I2(r0_keep[0]),
        .O(m_axis_tkeep[0]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m_axis_tkeep[1]_INST_0 
       (.I0(r1_keep[1]),
        .I1(\r0_out_sel_r_reg_n_0_[0] ),
        .I2(r0_keep[1]),
        .O(m_axis_tkeep[1]));
  LUT6 #(
    .INIT(64'hFBBF088008800880)) 
    m_axis_tlast_INST_0
       (.I0(r1_last_reg_n_0),
        .I1(Q[1]),
        .I2(\state_reg_n_0_[2] ),
        .I3(Q[0]),
        .I4(r0_last_reg_n_0),
        .I5(r0_is_end),
        .O(m_axis_tlast));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m_axis_tuser[0]_INST_0 
       (.I0(r1_user[0]),
        .I1(\r0_out_sel_r_reg_n_0_[0] ),
        .I2(r0_user[0]),
        .O(m_axis_tuser[0]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m_axis_tuser[1]_INST_0 
       (.I0(r1_user[1]),
        .I1(\r0_out_sel_r_reg_n_0_[0] ),
        .I2(r0_user[1]),
        .O(m_axis_tuser[1]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m_axis_tuser[2]_INST_0 
       (.I0(r1_user[2]),
        .I1(\r0_out_sel_r_reg_n_0_[0] ),
        .I2(r0_user[2]),
        .O(m_axis_tuser[2]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m_axis_tuser[3]_INST_0 
       (.I0(r1_user[3]),
        .I1(\r0_out_sel_r_reg_n_0_[0] ),
        .I2(r0_user[3]),
        .O(m_axis_tuser[3]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m_axis_tuser[4]_INST_0 
       (.I0(r1_user[4]),
        .I1(\r0_out_sel_r_reg_n_0_[0] ),
        .I2(r0_user[4]),
        .O(m_axis_tuser[4]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m_axis_tuser[5]_INST_0 
       (.I0(r1_user[5]),
        .I1(\r0_out_sel_r_reg_n_0_[0] ),
        .I2(r0_user[5]),
        .O(m_axis_tuser[5]));
  LUT2 #(
    .INIT(4'h4)) 
    \r0_data[31]_i_1 
       (.I0(\state_reg_n_0_[2] ),
        .I1(Q[0]),
        .O(r0_load));
  FDRE \r0_data_reg[0] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[0]),
        .Q(r0_data[0]),
        .R(1'b0));
  FDRE \r0_data_reg[10] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[10]),
        .Q(r0_data[10]),
        .R(1'b0));
  FDRE \r0_data_reg[11] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[11]),
        .Q(r0_data[11]),
        .R(1'b0));
  FDRE \r0_data_reg[12] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[12]),
        .Q(r0_data[12]),
        .R(1'b0));
  FDRE \r0_data_reg[13] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[13]),
        .Q(r0_data[13]),
        .R(1'b0));
  FDRE \r0_data_reg[14] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[14]),
        .Q(r0_data[14]),
        .R(1'b0));
  FDRE \r0_data_reg[15] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[15]),
        .Q(r0_data[15]),
        .R(1'b0));
  FDRE \r0_data_reg[16] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[16]),
        .Q(r0_data[16]),
        .R(1'b0));
  FDRE \r0_data_reg[17] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[17]),
        .Q(r0_data[17]),
        .R(1'b0));
  FDRE \r0_data_reg[18] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[18]),
        .Q(r0_data[18]),
        .R(1'b0));
  FDRE \r0_data_reg[19] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[19]),
        .Q(r0_data[19]),
        .R(1'b0));
  FDRE \r0_data_reg[1] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[1]),
        .Q(r0_data[1]),
        .R(1'b0));
  FDRE \r0_data_reg[20] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[20]),
        .Q(r0_data[20]),
        .R(1'b0));
  FDRE \r0_data_reg[21] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[21]),
        .Q(r0_data[21]),
        .R(1'b0));
  FDRE \r0_data_reg[22] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[22]),
        .Q(r0_data[22]),
        .R(1'b0));
  FDRE \r0_data_reg[23] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[23]),
        .Q(r0_data[23]),
        .R(1'b0));
  FDRE \r0_data_reg[24] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[24]),
        .Q(r0_data[24]),
        .R(1'b0));
  FDRE \r0_data_reg[25] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[25]),
        .Q(r0_data[25]),
        .R(1'b0));
  FDRE \r0_data_reg[26] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[26]),
        .Q(r0_data[26]),
        .R(1'b0));
  FDRE \r0_data_reg[27] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[27]),
        .Q(r0_data[27]),
        .R(1'b0));
  FDRE \r0_data_reg[28] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[28]),
        .Q(r0_data[28]),
        .R(1'b0));
  FDRE \r0_data_reg[29] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[29]),
        .Q(r0_data[29]),
        .R(1'b0));
  FDRE \r0_data_reg[2] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[2]),
        .Q(r0_data[2]),
        .R(1'b0));
  FDRE \r0_data_reg[30] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[30]),
        .Q(r0_data[30]),
        .R(1'b0));
  FDRE \r0_data_reg[31] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[31]),
        .Q(r0_data[31]),
        .R(1'b0));
  FDRE \r0_data_reg[3] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[3]),
        .Q(r0_data[3]),
        .R(1'b0));
  FDRE \r0_data_reg[4] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[4]),
        .Q(r0_data[4]),
        .R(1'b0));
  FDRE \r0_data_reg[5] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[5]),
        .Q(r0_data[5]),
        .R(1'b0));
  FDRE \r0_data_reg[6] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[6]),
        .Q(r0_data[6]),
        .R(1'b0));
  FDRE \r0_data_reg[7] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[7]),
        .Q(r0_data[7]),
        .R(1'b0));
  FDRE \r0_data_reg[8] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[8]),
        .Q(r0_data[8]),
        .R(1'b0));
  FDRE \r0_data_reg[9] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tdata[9]),
        .Q(r0_data[9]),
        .R(1'b0));
  FDRE \r0_id_reg[0] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[0]),
        .Q(r0_id[0]),
        .R(1'b0));
  FDRE \r0_id_reg[10] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[10]),
        .Q(r0_id[10]),
        .R(1'b0));
  FDRE \r0_id_reg[11] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[11]),
        .Q(r0_id[11]),
        .R(1'b0));
  FDRE \r0_id_reg[12] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[12]),
        .Q(r0_id[12]),
        .R(1'b0));
  FDRE \r0_id_reg[13] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[13]),
        .Q(r0_id[13]),
        .R(1'b0));
  FDRE \r0_id_reg[14] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[14]),
        .Q(r0_id[14]),
        .R(1'b0));
  FDRE \r0_id_reg[15] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[15]),
        .Q(r0_id[15]),
        .R(1'b0));
  FDRE \r0_id_reg[16] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[16]),
        .Q(r0_id[16]),
        .R(1'b0));
  FDRE \r0_id_reg[17] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[17]),
        .Q(r0_id[17]),
        .R(1'b0));
  FDRE \r0_id_reg[18] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[18]),
        .Q(r0_id[18]),
        .R(1'b0));
  FDRE \r0_id_reg[19] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[19]),
        .Q(r0_id[19]),
        .R(1'b0));
  FDRE \r0_id_reg[1] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[1]),
        .Q(r0_id[1]),
        .R(1'b0));
  FDRE \r0_id_reg[20] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[20]),
        .Q(r0_id[20]),
        .R(1'b0));
  FDRE \r0_id_reg[21] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[21]),
        .Q(r0_id[21]),
        .R(1'b0));
  FDRE \r0_id_reg[22] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[22]),
        .Q(r0_id[22]),
        .R(1'b0));
  FDRE \r0_id_reg[23] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[23]),
        .Q(r0_id[23]),
        .R(1'b0));
  FDRE \r0_id_reg[24] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[24]),
        .Q(r0_id[24]),
        .R(1'b0));
  FDRE \r0_id_reg[25] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[25]),
        .Q(r0_id[25]),
        .R(1'b0));
  FDRE \r0_id_reg[26] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[26]),
        .Q(r0_id[26]),
        .R(1'b0));
  FDRE \r0_id_reg[27] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[27]),
        .Q(r0_id[27]),
        .R(1'b0));
  FDRE \r0_id_reg[28] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[28]),
        .Q(r0_id[28]),
        .R(1'b0));
  FDRE \r0_id_reg[29] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[29]),
        .Q(r0_id[29]),
        .R(1'b0));
  FDRE \r0_id_reg[2] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[2]),
        .Q(r0_id[2]),
        .R(1'b0));
  FDRE \r0_id_reg[30] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[30]),
        .Q(r0_id[30]),
        .R(1'b0));
  FDRE \r0_id_reg[31] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[31]),
        .Q(r0_id[31]),
        .R(1'b0));
  FDRE \r0_id_reg[3] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[3]),
        .Q(r0_id[3]),
        .R(1'b0));
  FDRE \r0_id_reg[4] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[4]),
        .Q(r0_id[4]),
        .R(1'b0));
  FDRE \r0_id_reg[5] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[5]),
        .Q(r0_id[5]),
        .R(1'b0));
  FDRE \r0_id_reg[6] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[6]),
        .Q(r0_id[6]),
        .R(1'b0));
  FDRE \r0_id_reg[7] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[7]),
        .Q(r0_id[7]),
        .R(1'b0));
  FDRE \r0_id_reg[8] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[8]),
        .Q(r0_id[8]),
        .R(1'b0));
  FDRE \r0_id_reg[9] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tid[9]),
        .Q(r0_id[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hBBBFFFFF00040000)) 
    \r0_is_null_r[1]_i_1 
       (.I0(\state_reg_n_0_[2] ),
        .I1(Q[0]),
        .I2(s_axis_tkeep[2]),
        .I3(s_axis_tkeep[3]),
        .I4(s_axis_tvalid),
        .I5(r0_is_end),
        .O(\r0_is_null_r[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \r0_is_null_r_reg[1] 
       (.C(aclk),
        .CE(1'b1),
        .D(\r0_is_null_r[1]_i_1_n_0 ),
        .Q(r0_is_end),
        .R(SR));
  FDRE \r0_keep_reg[0] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tkeep[0]),
        .Q(r0_keep[0]),
        .R(1'b0));
  FDRE \r0_keep_reg[1] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tkeep[1]),
        .Q(r0_keep[1]),
        .R(1'b0));
  FDRE \r0_keep_reg[2] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tkeep[2]),
        .Q(r0_keep[2]),
        .R(1'b0));
  FDRE \r0_keep_reg[3] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tkeep[3]),
        .Q(r0_keep[3]),
        .R(1'b0));
  FDRE r0_last_reg
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tlast),
        .Q(r0_last_reg_n_0),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h00000046)) 
    \r0_out_sel_r[0]_i_1 
       (.I0(m_axis_tready),
        .I1(\r0_out_sel_r_reg_n_0_[0] ),
        .I2(r0_is_end),
        .I3(p_0_in),
        .I4(SR),
        .O(\r0_out_sel_r[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \r0_out_sel_r[0]_i_2 
       (.I0(Q[0]),
        .I1(\state_reg_n_0_[2] ),
        .I2(Q[1]),
        .O(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \r0_out_sel_r_reg[0] 
       (.C(aclk),
        .CE(1'b1),
        .D(\r0_out_sel_r[0]_i_1_n_0 ),
        .Q(\r0_out_sel_r_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \r0_user_reg[0] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tuser[0]),
        .Q(r0_user[0]),
        .R(1'b0));
  FDRE \r0_user_reg[10] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tuser[10]),
        .Q(r0_user[10]),
        .R(1'b0));
  FDRE \r0_user_reg[11] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tuser[11]),
        .Q(r0_user[11]),
        .R(1'b0));
  FDRE \r0_user_reg[1] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tuser[1]),
        .Q(r0_user[1]),
        .R(1'b0));
  FDRE \r0_user_reg[2] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tuser[2]),
        .Q(r0_user[2]),
        .R(1'b0));
  FDRE \r0_user_reg[3] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tuser[3]),
        .Q(r0_user[3]),
        .R(1'b0));
  FDRE \r0_user_reg[4] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tuser[4]),
        .Q(r0_user[4]),
        .R(1'b0));
  FDRE \r0_user_reg[5] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tuser[5]),
        .Q(r0_user[5]),
        .R(1'b0));
  FDRE \r0_user_reg[6] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tuser[6]),
        .Q(r0_user[6]),
        .R(1'b0));
  FDRE \r0_user_reg[7] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tuser[7]),
        .Q(r0_user[7]),
        .R(1'b0));
  FDRE \r0_user_reg[8] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tuser[8]),
        .Q(r0_user[8]),
        .R(1'b0));
  FDRE \r0_user_reg[9] 
       (.C(aclk),
        .CE(r0_load),
        .D(s_axis_tuser[9]),
        .Q(r0_user[9]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h04)) 
    \r1_data[15]_i_1 
       (.I0(\state_reg_n_0_[2] ),
        .I1(Q[1]),
        .I2(Q[0]),
        .O(\r1_data[15]_i_1_n_0 ));
  FDRE \r1_data_reg[0] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_data[16]),
        .Q(r1_data[0]),
        .R(1'b0));
  FDRE \r1_data_reg[10] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_data[26]),
        .Q(r1_data[10]),
        .R(1'b0));
  FDRE \r1_data_reg[11] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_data[27]),
        .Q(r1_data[11]),
        .R(1'b0));
  FDRE \r1_data_reg[12] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_data[28]),
        .Q(r1_data[12]),
        .R(1'b0));
  FDRE \r1_data_reg[13] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_data[29]),
        .Q(r1_data[13]),
        .R(1'b0));
  FDRE \r1_data_reg[14] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_data[30]),
        .Q(r1_data[14]),
        .R(1'b0));
  FDRE \r1_data_reg[15] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_data[31]),
        .Q(r1_data[15]),
        .R(1'b0));
  FDRE \r1_data_reg[1] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_data[17]),
        .Q(r1_data[1]),
        .R(1'b0));
  FDRE \r1_data_reg[2] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_data[18]),
        .Q(r1_data[2]),
        .R(1'b0));
  FDRE \r1_data_reg[3] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_data[19]),
        .Q(r1_data[3]),
        .R(1'b0));
  FDRE \r1_data_reg[4] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_data[20]),
        .Q(r1_data[4]),
        .R(1'b0));
  FDRE \r1_data_reg[5] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_data[21]),
        .Q(r1_data[5]),
        .R(1'b0));
  FDRE \r1_data_reg[6] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_data[22]),
        .Q(r1_data[6]),
        .R(1'b0));
  FDRE \r1_data_reg[7] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_data[23]),
        .Q(r1_data[7]),
        .R(1'b0));
  FDRE \r1_data_reg[8] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_data[24]),
        .Q(r1_data[8]),
        .R(1'b0));
  FDRE \r1_data_reg[9] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_data[25]),
        .Q(r1_data[9]),
        .R(1'b0));
  FDRE \r1_id_reg[0] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[0]),
        .Q(r1_id[0]),
        .R(1'b0));
  FDRE \r1_id_reg[10] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[10]),
        .Q(r1_id[10]),
        .R(1'b0));
  FDRE \r1_id_reg[11] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[11]),
        .Q(r1_id[11]),
        .R(1'b0));
  FDRE \r1_id_reg[12] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[12]),
        .Q(r1_id[12]),
        .R(1'b0));
  FDRE \r1_id_reg[13] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[13]),
        .Q(r1_id[13]),
        .R(1'b0));
  FDRE \r1_id_reg[14] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[14]),
        .Q(r1_id[14]),
        .R(1'b0));
  FDRE \r1_id_reg[15] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[15]),
        .Q(r1_id[15]),
        .R(1'b0));
  FDRE \r1_id_reg[16] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[16]),
        .Q(r1_id[16]),
        .R(1'b0));
  FDRE \r1_id_reg[17] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[17]),
        .Q(r1_id[17]),
        .R(1'b0));
  FDRE \r1_id_reg[18] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[18]),
        .Q(r1_id[18]),
        .R(1'b0));
  FDRE \r1_id_reg[19] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[19]),
        .Q(r1_id[19]),
        .R(1'b0));
  FDRE \r1_id_reg[1] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[1]),
        .Q(r1_id[1]),
        .R(1'b0));
  FDRE \r1_id_reg[20] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[20]),
        .Q(r1_id[20]),
        .R(1'b0));
  FDRE \r1_id_reg[21] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[21]),
        .Q(r1_id[21]),
        .R(1'b0));
  FDRE \r1_id_reg[22] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[22]),
        .Q(r1_id[22]),
        .R(1'b0));
  FDRE \r1_id_reg[23] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[23]),
        .Q(r1_id[23]),
        .R(1'b0));
  FDRE \r1_id_reg[24] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[24]),
        .Q(r1_id[24]),
        .R(1'b0));
  FDRE \r1_id_reg[25] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[25]),
        .Q(r1_id[25]),
        .R(1'b0));
  FDRE \r1_id_reg[26] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[26]),
        .Q(r1_id[26]),
        .R(1'b0));
  FDRE \r1_id_reg[27] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[27]),
        .Q(r1_id[27]),
        .R(1'b0));
  FDRE \r1_id_reg[28] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[28]),
        .Q(r1_id[28]),
        .R(1'b0));
  FDRE \r1_id_reg[29] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[29]),
        .Q(r1_id[29]),
        .R(1'b0));
  FDRE \r1_id_reg[2] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[2]),
        .Q(r1_id[2]),
        .R(1'b0));
  FDRE \r1_id_reg[30] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[30]),
        .Q(r1_id[30]),
        .R(1'b0));
  FDRE \r1_id_reg[31] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[31]),
        .Q(r1_id[31]),
        .R(1'b0));
  FDRE \r1_id_reg[3] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[3]),
        .Q(r1_id[3]),
        .R(1'b0));
  FDRE \r1_id_reg[4] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[4]),
        .Q(r1_id[4]),
        .R(1'b0));
  FDRE \r1_id_reg[5] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[5]),
        .Q(r1_id[5]),
        .R(1'b0));
  FDRE \r1_id_reg[6] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[6]),
        .Q(r1_id[6]),
        .R(1'b0));
  FDRE \r1_id_reg[7] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[7]),
        .Q(r1_id[7]),
        .R(1'b0));
  FDRE \r1_id_reg[8] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[8]),
        .Q(r1_id[8]),
        .R(1'b0));
  FDRE \r1_id_reg[9] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_id[9]),
        .Q(r1_id[9]),
        .R(1'b0));
  FDRE \r1_keep_reg[0] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_keep[2]),
        .Q(r1_keep[0]),
        .R(1'b0));
  FDRE \r1_keep_reg[1] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_keep[3]),
        .Q(r1_keep[1]),
        .R(1'b0));
  FDRE r1_last_reg
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_last_reg_n_0),
        .Q(r1_last_reg_n_0),
        .R(1'b0));
  FDRE \r1_user_reg[0] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_user[6]),
        .Q(r1_user[0]),
        .R(1'b0));
  FDRE \r1_user_reg[1] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_user[7]),
        .Q(r1_user[1]),
        .R(1'b0));
  FDRE \r1_user_reg[2] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_user[8]),
        .Q(r1_user[2]),
        .R(1'b0));
  FDRE \r1_user_reg[3] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_user[9]),
        .Q(r1_user[3]),
        .R(1'b0));
  FDRE \r1_user_reg[4] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_user[10]),
        .Q(r1_user[4]),
        .R(1'b0));
  FDRE \r1_user_reg[5] 
       (.C(aclk),
        .CE(\r1_data[15]_i_1_n_0 ),
        .D(r0_user[11]),
        .Q(r1_user[5]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFF550FCF)) 
    \state[0]_i_1 
       (.I0(s_axis_tvalid),
        .I1(m_axis_tready),
        .I2(Q[1]),
        .I3(\state_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(state[0]));
  LUT6 #(
    .INIT(64'h0000AFAAFF003F00)) 
    \state[1]_i_1 
       (.I0(s_axis_tvalid),
        .I1(r0_is_end),
        .I2(m_axis_tready),
        .I3(Q[1]),
        .I4(\state_reg_n_0_[2] ),
        .I5(Q[0]),
        .O(state[1]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h000008C0)) 
    \state[2]_i_1 
       (.I0(s_axis_tvalid),
        .I1(Q[1]),
        .I2(\state_reg_n_0_[2] ),
        .I3(Q[0]),
        .I4(m_axis_tready),
        .O(state[2]));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \state_reg[0] 
       (.C(aclk),
        .CE(1'b1),
        .D(state[0]),
        .Q(Q[0]),
        .R(SR));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \state_reg[1] 
       (.C(aclk),
        .CE(1'b1),
        .D(state[1]),
        .Q(Q[1]),
        .R(SR));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \state_reg[2] 
       (.C(aclk),
        .CE(1'b1),
        .D(state[2]),
        .Q(\state_reg_n_0_[2] ),
        .R(SR));
endmodule

(* AXIS_TDATA_WIDTH = "32" *) (* AXIS_TDEST_WIDTH = "2" *) (* AXIS_TUSER_WIDTH = "96" *) 
(* DowngradeIPIdentifiedWarnings = "yes" *) (* VFB_4PXL_W = "32" *) (* VFB_BYPASS_WC = "0" *) 
(* VFB_DATA_TYPE = "42" *) (* VFB_DCONV_OWIDTH = "16" *) (* VFB_FIFO_DEPTH = "128" *) 
(* VFB_FIFO_WIDTH = "32" *) (* VFB_FILTER_VC = "0" *) (* VFB_OP_DWIDTH = "16" *) 
(* VFB_OP_PIXELS = "2" *) (* VFB_PXL_W = "8" *) (* VFB_PXL_W_BB = "8" *) 
(* VFB_REQ_BUFFER = "0" *) (* VFB_REQ_REORDER = "0" *) (* VFB_TU_WIDTH = "1" *) 
(* VFB_VC = "0" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (s_axis_aclk,
    s_axis_aresetn,
    s_axis_tready,
    s_axis_tvalid,
    s_axis_tlast,
    s_axis_tdata,
    s_axis_tkeep,
    s_axis_tuser,
    s_axis_tdest,
    mdt_tv,
    mdt_tr,
    sdt_tv,
    sdt_tr,
    vfb_tv,
    vfb_tr,
    vfb_clk,
    vfb_ready,
    vfb_valid,
    vfb_eol,
    vfb_sof,
    vfb_vcdt,
    vfb_data);
  input s_axis_aclk;
  input s_axis_aresetn;
  output s_axis_tready;
  input s_axis_tvalid;
  input s_axis_tlast;
  input [31:0]s_axis_tdata;
  input [3:0]s_axis_tkeep;
  input [95:0]s_axis_tuser;
  input [1:0]s_axis_tdest;
  output mdt_tv;
  output mdt_tr;
  output sdt_tv;
  output sdt_tr;
  output vfb_tv;
  output vfb_tr;
  input vfb_clk;
  input vfb_ready;
  output vfb_valid;
  output vfb_eol;
  output [0:0]vfb_sof;
  output [7:0]vfb_vcdt;
  output [15:0]vfb_data;

  wire mdt_tr;
  wire mdt_tv;
  wire s_axis_aclk;
  wire s_axis_aresetn;
  wire [31:0]s_axis_tdata;
  wire [1:0]s_axis_tdest;
  wire [3:0]s_axis_tkeep;
  wire s_axis_tlast;
  wire s_axis_tready;
  wire [95:0]s_axis_tuser;
  wire s_axis_tvalid;
  wire sdt_tr;
  wire sdt_tv;
  wire vfb_clk;
  wire [15:0]vfb_data;
  wire vfb_eol;
  wire vfb_ready;
  wire [0:0]vfb_sof;
  wire vfb_tr;
  wire vfb_tv;
  wire vfb_valid;
  wire [7:0]vfb_vcdt;

  (* AXIS_TDATA_WIDTH = "32" *) 
  (* AXIS_TDEST_WIDTH = "2" *) 
  (* AXIS_TUSER_WIDTH = "96" *) 
  (* DowngradeIPIdentifiedWarnings = "yes" *) 
  (* VFB_4PXL_W = "32" *) 
  (* VFB_BYPASS_WC = "0" *) 
  (* VFB_DATA_TYPE = "42" *) 
  (* VFB_DCONV_OWIDTH = "16" *) 
  (* VFB_DCONV_TUW = "12" *) 
  (* VFB_FIFO_DEPTH = "128" *) 
  (* VFB_FIFO_WIDTH = "32" *) 
  (* VFB_FILTER_VC = "0" *) 
  (* VFB_OP_DWIDTH = "16" *) 
  (* VFB_OP_PIXELS = "2" *) 
  (* VFB_PXL_W = "8" *) 
  (* VFB_PXL_W_BB = "8" *) 
  (* VFB_REQ_BUFFER = "0" *) 
  (* VFB_REQ_REORDER = "0" *) 
  (* VFB_TSB0_WIDTH = "32" *) 
  (* VFB_TSB1_WIDTH = "0" *) 
  (* VFB_TSB2_WIDTH = "3" *) 
  (* VFB_TU_WIDTH = "1" *) 
  (* VFB_VC = "0" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_bd_d10d_vfb_0_0_core inst
       (.mdt_tr(mdt_tr),
        .mdt_tv(mdt_tv),
        .s_axis_aclk(s_axis_aclk),
        .s_axis_aresetn(s_axis_aresetn),
        .s_axis_tdata(s_axis_tdata),
        .s_axis_tdest(s_axis_tdest),
        .s_axis_tkeep(s_axis_tkeep),
        .s_axis_tlast(s_axis_tlast),
        .s_axis_tready(s_axis_tready),
        .s_axis_tuser(s_axis_tuser),
        .s_axis_tvalid(s_axis_tvalid),
        .sdt_tr(sdt_tr),
        .sdt_tv(sdt_tv),
        .vfb_clk(vfb_clk),
        .vfb_data(vfb_data),
        .vfb_eol(vfb_eol),
        .vfb_ready(vfb_ready),
        .vfb_sof(vfb_sof),
        .vfb_tr(vfb_tr),
        .vfb_tv(vfb_tv),
        .vfb_valid(vfb_valid),
        .vfb_vcdt(vfb_vcdt));
endmodule

(* CHECK_LICENSE_TYPE = "bd_d10d_vfb_0_0_axis_converter,axis_dwidth_converter_v1_1_16_axis_dwidth_converter,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "axis_dwidth_converter_v1_1_16_axis_dwidth_converter,Vivado 2018.2.1" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_bd_d10d_vfb_0_0_axis_converter
   (aclk,
    aresetn,
    s_axis_tvalid,
    s_axis_tready,
    s_axis_tdata,
    s_axis_tkeep,
    s_axis_tlast,
    s_axis_tid,
    s_axis_tuser,
    m_axis_tvalid,
    m_axis_tready,
    m_axis_tdata,
    m_axis_tkeep,
    m_axis_tlast,
    m_axis_tid,
    m_axis_tuser);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLKIF CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLKIF, FREQ_HZ 10000000, PHASE 0.000" *) input aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 RSTIF RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME RSTIF, POLARITY ACTIVE_LOW" *) input aresetn;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S_AXIS TVALID" *) input s_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S_AXIS TREADY" *) output s_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S_AXIS TDATA" *) input [31:0]s_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S_AXIS TKEEP" *) input [3:0]s_axis_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S_AXIS TLAST" *) input s_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S_AXIS TID" *) input [31:0]s_axis_tid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S_AXIS TUSER" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S_AXIS, TDATA_NUM_BYTES 1, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, LAYERED_METADATA undef" *) input [11:0]s_axis_tuser;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M_AXIS TVALID" *) output m_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M_AXIS TREADY" *) input m_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M_AXIS TDATA" *) output [15:0]m_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M_AXIS TKEEP" *) output [1:0]m_axis_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M_AXIS TLAST" *) output m_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M_AXIS TID" *) output [31:0]m_axis_tid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M_AXIS TUSER" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M_AXIS, TDATA_NUM_BYTES 1, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, LAYERED_METADATA undef" *) output [5:0]m_axis_tuser;

  wire aclk;
  wire aresetn;
  wire [15:0]m_axis_tdata;
  wire [31:0]m_axis_tid;
  wire [1:0]m_axis_tkeep;
  wire m_axis_tlast;
  wire m_axis_tready;
  wire [5:0]m_axis_tuser;
  wire m_axis_tvalid;
  wire [31:0]s_axis_tdata;
  wire [31:0]s_axis_tid;
  wire [3:0]s_axis_tkeep;
  wire s_axis_tlast;
  wire s_axis_tready;
  wire [11:0]s_axis_tuser;
  wire s_axis_tvalid;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_dwidth_converter_v1_1_16_axis_dwidth_converter inst
       (.Q({m_axis_tvalid,s_axis_tready}),
        .aclk(aclk),
        .aresetn(aresetn),
        .m_axis_tdata(m_axis_tdata),
        .m_axis_tid(m_axis_tid),
        .m_axis_tkeep(m_axis_tkeep),
        .m_axis_tlast(m_axis_tlast),
        .m_axis_tready(m_axis_tready),
        .m_axis_tuser(m_axis_tuser),
        .s_axis_tdata(s_axis_tdata),
        .s_axis_tid(s_axis_tid),
        .s_axis_tkeep(s_axis_tkeep),
        .s_axis_tlast(s_axis_tlast),
        .s_axis_tuser(s_axis_tuser),
        .s_axis_tvalid(s_axis_tvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_bd_d10d_vfb_0_0_axis_dconverter
   (m_axis_tvalid,
    m_axis_tlast,
    \vfb_sof_reg[0] ,
    D,
    \vfb_data_reg[15] ,
    \vfb_data_reg[14] ,
    \vfb_data_reg[13] ,
    \vfb_data_reg[12] ,
    \vfb_data_reg[11] ,
    \vfb_data_reg[10] ,
    \vfb_data_reg[9] ,
    \vfb_data_reg[8] ,
    \vfb_data_reg[7] ,
    \vfb_data_reg[6] ,
    \vfb_data_reg[5] ,
    \vfb_data_reg[4] ,
    \vfb_data_reg[3] ,
    \vfb_data_reg[2] ,
    \vfb_data_reg[1] ,
    \vfb_data_reg[0] ,
    vfb_clk,
    s_axis_aresetn,
    mdt_tr,
    sband_tu_r,
    sband_tact0,
    Q,
    vfb_eol_reg,
    \sband_ts_r_reg[7] ,
    \sband_td_r_reg[15] ,
    sband_tact_reg);
  output m_axis_tvalid;
  output m_axis_tlast;
  output \vfb_sof_reg[0] ;
  output [7:0]D;
  output \vfb_data_reg[15] ;
  output \vfb_data_reg[14] ;
  output \vfb_data_reg[13] ;
  output \vfb_data_reg[12] ;
  output \vfb_data_reg[11] ;
  output \vfb_data_reg[10] ;
  output \vfb_data_reg[9] ;
  output \vfb_data_reg[8] ;
  output \vfb_data_reg[7] ;
  output \vfb_data_reg[6] ;
  output \vfb_data_reg[5] ;
  output \vfb_data_reg[4] ;
  output \vfb_data_reg[3] ;
  output \vfb_data_reg[2] ;
  output \vfb_data_reg[1] ;
  output \vfb_data_reg[0] ;
  input vfb_clk;
  input s_axis_aresetn;
  input mdt_tr;
  input sband_tu_r;
  input sband_tact0;
  input [23:0]Q;
  input vfb_eol_reg;
  input [7:0]\sband_ts_r_reg[7] ;
  input [15:0]\sband_td_r_reg[15] ;
  input sband_tact_reg;

  wire [7:0]D;
  wire [23:0]Q;
  wire [15:0]m_axis_tdata;
  wire [31:0]m_axis_tid;
  wire [1:0]m_axis_tkeep;
  wire m_axis_tlast;
  wire [2:0]m_axis_tuser;
  wire m_axis_tvalid;
  wire mdt_tr;
  wire s_axis_aresetn;
  wire s_fifo_tr;
  wire sband_tact0;
  wire sband_tact_reg;
  wire [15:0]\sband_td_r_reg[15] ;
  wire [7:0]\sband_ts_r_reg[7] ;
  wire sband_tu_r;
  wire vfb_clk;
  wire \vfb_data_reg[0] ;
  wire \vfb_data_reg[10] ;
  wire \vfb_data_reg[11] ;
  wire \vfb_data_reg[12] ;
  wire \vfb_data_reg[13] ;
  wire \vfb_data_reg[14] ;
  wire \vfb_data_reg[15] ;
  wire \vfb_data_reg[1] ;
  wire \vfb_data_reg[2] ;
  wire \vfb_data_reg[3] ;
  wire \vfb_data_reg[4] ;
  wire \vfb_data_reg[5] ;
  wire \vfb_data_reg[6] ;
  wire \vfb_data_reg[7] ;
  wire \vfb_data_reg[8] ;
  wire \vfb_data_reg[9] ;
  wire vfb_eol_reg;
  wire \vfb_sof_reg[0] ;
  wire [5:3]NLW_axis_conv_inst_m_axis_tuser_UNCONNECTED;

  (* CHECK_LICENSE_TYPE = "bd_d10d_vfb_0_0_axis_converter,axis_dwidth_converter_v1_1_16_axis_dwidth_converter,{}" *) 
  (* DowngradeIPIdentifiedWarnings = "yes" *) 
  (* X_CORE_INFO = "axis_dwidth_converter_v1_1_16_axis_dwidth_converter,Vivado 2018.2.1" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_bd_d10d_vfb_0_0_axis_converter axis_conv_inst
       (.aclk(vfb_clk),
        .aresetn(s_axis_aresetn),
        .m_axis_tdata(m_axis_tdata),
        .m_axis_tid(m_axis_tid),
        .m_axis_tkeep(m_axis_tkeep),
        .m_axis_tlast(m_axis_tlast),
        .m_axis_tready(mdt_tr),
        .m_axis_tuser({NLW_axis_conv_inst_m_axis_tuser_UNCONNECTED[5:3],m_axis_tuser}),
        .m_axis_tvalid(m_axis_tvalid),
        .s_axis_tdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axis_tid({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axis_tkeep({1'b0,1'b0,1'b0,1'b0}),
        .s_axis_tlast(1'b0),
        .s_axis_tready(s_fifo_tr),
        .s_axis_tuser({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axis_tvalid(1'b0));
  LUT6 #(
    .INIT(64'hB8BBB888B888B888)) 
    \vfb_data[0]_i_1 
       (.I0(m_axis_tdata[0]),
        .I1(m_axis_tvalid),
        .I2(Q[8]),
        .I3(sband_tact0),
        .I4(\sband_td_r_reg[15] [0]),
        .I5(sband_tact_reg),
        .O(\vfb_data_reg[0] ));
  LUT6 #(
    .INIT(64'hB8BBB888B888B888)) 
    \vfb_data[10]_i_1 
       (.I0(m_axis_tdata[10]),
        .I1(m_axis_tvalid),
        .I2(Q[18]),
        .I3(sband_tact0),
        .I4(\sband_td_r_reg[15] [10]),
        .I5(sband_tact_reg),
        .O(\vfb_data_reg[10] ));
  LUT6 #(
    .INIT(64'hB8BBB888B888B888)) 
    \vfb_data[11]_i_1 
       (.I0(m_axis_tdata[11]),
        .I1(m_axis_tvalid),
        .I2(Q[19]),
        .I3(sband_tact0),
        .I4(\sband_td_r_reg[15] [11]),
        .I5(sband_tact_reg),
        .O(\vfb_data_reg[11] ));
  LUT6 #(
    .INIT(64'hB8BBB888B888B888)) 
    \vfb_data[12]_i_1 
       (.I0(m_axis_tdata[12]),
        .I1(m_axis_tvalid),
        .I2(Q[20]),
        .I3(sband_tact0),
        .I4(\sband_td_r_reg[15] [12]),
        .I5(sband_tact_reg),
        .O(\vfb_data_reg[12] ));
  LUT6 #(
    .INIT(64'hB8BBB888B888B888)) 
    \vfb_data[13]_i_1 
       (.I0(m_axis_tdata[13]),
        .I1(m_axis_tvalid),
        .I2(Q[21]),
        .I3(sband_tact0),
        .I4(\sband_td_r_reg[15] [13]),
        .I5(sband_tact_reg),
        .O(\vfb_data_reg[13] ));
  LUT6 #(
    .INIT(64'hB8BBB888B888B888)) 
    \vfb_data[14]_i_1 
       (.I0(m_axis_tdata[14]),
        .I1(m_axis_tvalid),
        .I2(Q[22]),
        .I3(sband_tact0),
        .I4(\sband_td_r_reg[15] [14]),
        .I5(sband_tact_reg),
        .O(\vfb_data_reg[14] ));
  LUT6 #(
    .INIT(64'hB8BBB888B888B888)) 
    \vfb_data[15]_i_2 
       (.I0(m_axis_tdata[15]),
        .I1(m_axis_tvalid),
        .I2(Q[23]),
        .I3(sband_tact0),
        .I4(\sband_td_r_reg[15] [15]),
        .I5(sband_tact_reg),
        .O(\vfb_data_reg[15] ));
  LUT6 #(
    .INIT(64'hB8BBB888B888B888)) 
    \vfb_data[1]_i_1 
       (.I0(m_axis_tdata[1]),
        .I1(m_axis_tvalid),
        .I2(Q[9]),
        .I3(sband_tact0),
        .I4(\sband_td_r_reg[15] [1]),
        .I5(sband_tact_reg),
        .O(\vfb_data_reg[1] ));
  LUT6 #(
    .INIT(64'hB8BBB888B888B888)) 
    \vfb_data[2]_i_1 
       (.I0(m_axis_tdata[2]),
        .I1(m_axis_tvalid),
        .I2(Q[10]),
        .I3(sband_tact0),
        .I4(\sband_td_r_reg[15] [2]),
        .I5(sband_tact_reg),
        .O(\vfb_data_reg[2] ));
  LUT6 #(
    .INIT(64'hB8BBB888B888B888)) 
    \vfb_data[3]_i_1 
       (.I0(m_axis_tdata[3]),
        .I1(m_axis_tvalid),
        .I2(Q[11]),
        .I3(sband_tact0),
        .I4(\sband_td_r_reg[15] [3]),
        .I5(sband_tact_reg),
        .O(\vfb_data_reg[3] ));
  LUT6 #(
    .INIT(64'hB8BBB888B888B888)) 
    \vfb_data[4]_i_1 
       (.I0(m_axis_tdata[4]),
        .I1(m_axis_tvalid),
        .I2(Q[12]),
        .I3(sband_tact0),
        .I4(\sband_td_r_reg[15] [4]),
        .I5(sband_tact_reg),
        .O(\vfb_data_reg[4] ));
  LUT6 #(
    .INIT(64'hB8BBB888B888B888)) 
    \vfb_data[5]_i_1 
       (.I0(m_axis_tdata[5]),
        .I1(m_axis_tvalid),
        .I2(Q[13]),
        .I3(sband_tact0),
        .I4(\sband_td_r_reg[15] [5]),
        .I5(sband_tact_reg),
        .O(\vfb_data_reg[5] ));
  LUT6 #(
    .INIT(64'hB8BBB888B888B888)) 
    \vfb_data[6]_i_1 
       (.I0(m_axis_tdata[6]),
        .I1(m_axis_tvalid),
        .I2(Q[14]),
        .I3(sband_tact0),
        .I4(\sband_td_r_reg[15] [6]),
        .I5(sband_tact_reg),
        .O(\vfb_data_reg[6] ));
  LUT6 #(
    .INIT(64'hB8BBB888B888B888)) 
    \vfb_data[7]_i_1 
       (.I0(m_axis_tdata[7]),
        .I1(m_axis_tvalid),
        .I2(Q[15]),
        .I3(sband_tact0),
        .I4(\sband_td_r_reg[15] [7]),
        .I5(sband_tact_reg),
        .O(\vfb_data_reg[7] ));
  LUT6 #(
    .INIT(64'hB8BBB888B888B888)) 
    \vfb_data[8]_i_1 
       (.I0(m_axis_tdata[8]),
        .I1(m_axis_tvalid),
        .I2(Q[16]),
        .I3(sband_tact0),
        .I4(\sband_td_r_reg[15] [8]),
        .I5(sband_tact_reg),
        .O(\vfb_data_reg[8] ));
  LUT6 #(
    .INIT(64'hB8BBB888B888B888)) 
    \vfb_data[9]_i_1 
       (.I0(m_axis_tdata[9]),
        .I1(m_axis_tvalid),
        .I2(Q[17]),
        .I3(sband_tact0),
        .I4(\sband_td_r_reg[15] [9]),
        .I5(sband_tact_reg),
        .O(\vfb_data_reg[9] ));
  LUT6 #(
    .INIT(64'h88888888BBB888B8)) 
    \vfb_sof[0]_i_2 
       (.I0(m_axis_tuser[0]),
        .I1(m_axis_tvalid),
        .I2(sband_tu_r),
        .I3(sband_tact0),
        .I4(Q[2]),
        .I5(vfb_eol_reg),
        .O(\vfb_sof_reg[0] ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \vfb_vcdt[0]_i_1 
       (.I0(m_axis_tid[0]),
        .I1(m_axis_tvalid),
        .I2(Q[0]),
        .I3(sband_tact0),
        .I4(\sband_ts_r_reg[7] [0]),
        .O(D[0]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \vfb_vcdt[1]_i_1 
       (.I0(m_axis_tid[1]),
        .I1(m_axis_tvalid),
        .I2(Q[1]),
        .I3(sband_tact0),
        .I4(\sband_ts_r_reg[7] [1]),
        .O(D[1]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \vfb_vcdt[2]_i_1 
       (.I0(m_axis_tid[2]),
        .I1(m_axis_tvalid),
        .I2(Q[3]),
        .I3(sband_tact0),
        .I4(\sband_ts_r_reg[7] [2]),
        .O(D[2]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \vfb_vcdt[3]_i_1 
       (.I0(m_axis_tid[3]),
        .I1(m_axis_tvalid),
        .I2(Q[4]),
        .I3(sband_tact0),
        .I4(\sband_ts_r_reg[7] [3]),
        .O(D[3]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \vfb_vcdt[4]_i_1 
       (.I0(m_axis_tid[4]),
        .I1(m_axis_tvalid),
        .I2(Q[5]),
        .I3(sband_tact0),
        .I4(\sband_ts_r_reg[7] [4]),
        .O(D[4]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \vfb_vcdt[5]_i_1 
       (.I0(m_axis_tid[5]),
        .I1(m_axis_tvalid),
        .I2(Q[6]),
        .I3(sband_tact0),
        .I4(\sband_ts_r_reg[7] [5]),
        .O(D[5]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \vfb_vcdt[6]_i_1 
       (.I0(m_axis_tid[6]),
        .I1(m_axis_tvalid),
        .I2(Q[7]),
        .I3(sband_tact0),
        .I4(\sband_ts_r_reg[7] [6]),
        .O(D[6]));
  LUT4 #(
    .INIT(16'hBBB8)) 
    \vfb_vcdt[7]_i_2 
       (.I0(m_axis_tid[7]),
        .I1(m_axis_tvalid),
        .I2(\sband_ts_r_reg[7] [7]),
        .I3(sband_tact0),
        .O(D[7]));
endmodule

(* AXIS_TDATA_WIDTH = "32" *) (* AXIS_TDEST_WIDTH = "2" *) (* AXIS_TUSER_WIDTH = "96" *) 
(* DowngradeIPIdentifiedWarnings = "yes" *) (* VFB_4PXL_W = "32" *) (* VFB_BYPASS_WC = "0" *) 
(* VFB_DATA_TYPE = "42" *) (* VFB_DCONV_OWIDTH = "16" *) (* VFB_DCONV_TUW = "12" *) 
(* VFB_FIFO_DEPTH = "128" *) (* VFB_FIFO_WIDTH = "32" *) (* VFB_FILTER_VC = "0" *) 
(* VFB_OP_DWIDTH = "16" *) (* VFB_OP_PIXELS = "2" *) (* VFB_PXL_W = "8" *) 
(* VFB_PXL_W_BB = "8" *) (* VFB_REQ_BUFFER = "0" *) (* VFB_REQ_REORDER = "0" *) 
(* VFB_TSB0_WIDTH = "32" *) (* VFB_TSB1_WIDTH = "0" *) (* VFB_TSB2_WIDTH = "3" *) 
(* VFB_TU_WIDTH = "1" *) (* VFB_VC = "0" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_bd_d10d_vfb_0_0_core
   (s_axis_aclk,
    s_axis_aresetn,
    s_axis_tready,
    s_axis_tvalid,
    s_axis_tlast,
    s_axis_tdata,
    s_axis_tkeep,
    s_axis_tuser,
    s_axis_tdest,
    mdt_tv,
    mdt_tr,
    sdt_tv,
    sdt_tr,
    vfb_tv,
    vfb_tr,
    vfb_clk,
    vfb_ready,
    vfb_valid,
    vfb_eol,
    vfb_sof,
    vfb_vcdt,
    vfb_data);
  input s_axis_aclk;
  input s_axis_aresetn;
  output s_axis_tready;
  input s_axis_tvalid;
  input s_axis_tlast;
  input [31:0]s_axis_tdata;
  input [3:0]s_axis_tkeep;
  input [95:0]s_axis_tuser;
  input [1:0]s_axis_tdest;
  output mdt_tv;
  output mdt_tr;
  output sdt_tv;
  output sdt_tr;
  output vfb_tv;
  output vfb_tr;
  input vfb_clk;
  input vfb_ready;
  output vfb_valid;
  output vfb_eol;
  output [0:0]vfb_sof;
  output [7:0]vfb_vcdt;
  output [15:0]vfb_data;

  wire axis_dconverter_n_11;
  wire axis_dconverter_n_12;
  wire axis_dconverter_n_13;
  wire axis_dconverter_n_14;
  wire axis_dconverter_n_15;
  wire axis_dconverter_n_16;
  wire axis_dconverter_n_17;
  wire axis_dconverter_n_18;
  wire axis_dconverter_n_19;
  wire axis_dconverter_n_2;
  wire axis_dconverter_n_20;
  wire axis_dconverter_n_21;
  wire axis_dconverter_n_22;
  wire axis_dconverter_n_23;
  wire axis_dconverter_n_24;
  wire axis_dconverter_n_25;
  wire axis_dconverter_n_26;
  wire cur_dtype_udef;
  wire m_axis_tlast;
  wire mdt_tr;
  wire mdt_tv;
  wire n_0_76;
  wire op_inf_n_10;
  wire op_inf_n_27;
  wire op_inf_n_28;
  wire op_inf_n_29;
  wire op_inf_n_30;
  wire op_inf_n_31;
  wire op_inf_n_32;
  wire op_inf_n_33;
  wire op_inf_n_34;
  wire op_inf_n_35;
  wire op_inf_n_36;
  wire op_inf_n_37;
  wire op_inf_n_38;
  wire op_inf_n_39;
  wire op_inf_n_40;
  wire op_inf_n_41;
  wire op_inf_n_42;
  wire op_inf_n_6;
  wire op_inf_n_7;
  wire op_inf_n_8;
  wire op_inf_n_9;
  wire [7:0]p_1_in;
  wire reorder_n_1;
  wire reorder_n_3;
  wire reorder_n_44;
  wire reorder_n_48;
  wire reorder_n_49;
  wire s_axis_aclk;
  wire s_axis_aresetn;
  wire [31:0]s_axis_tdata;
  wire [15:0]s_axis_tdata__0;
  wire [1:0]s_axis_tdest;
  wire [3:0]s_axis_tkeep;
  wire s_axis_tlast;
  wire s_axis_tready;
  wire [95:0]s_axis_tuser;
  wire s_axis_tvalid;
  wire sband_tact0;
  wire [15:0]sband_td_r;
  wire sband_tl;
  wire [7:2]sband_ts;
  wire [1:0]sband_ts__0;
  wire [7:0]sband_ts_r;
  wire sband_tu;
  wire sband_tu_r;
  wire sdt_tr;
  wire sdt_tv;
  wire vfb_clk;
  wire [15:0]vfb_data;
  wire vfb_eol;
  wire vfb_ready;
  wire [0:0]vfb_sof;
  wire vfb_valid;
  wire [7:0]vfb_vcdt;

  assign vfb_tr = vfb_ready;
  assign vfb_tv = vfb_valid;
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_bd_d10d_vfb_0_0_axis_dconverter axis_dconverter
       (.D(p_1_in),
        .Q({s_axis_tdata__0,sband_ts[6:2],reorder_n_44,sband_ts__0}),
        .m_axis_tlast(m_axis_tlast),
        .m_axis_tvalid(mdt_tv),
        .mdt_tr(mdt_tr),
        .s_axis_aresetn(s_axis_aresetn),
        .sband_tact0(sband_tact0),
        .sband_tact_reg(op_inf_n_8),
        .\sband_td_r_reg[15] ({op_inf_n_27,op_inf_n_28,op_inf_n_29,op_inf_n_30,op_inf_n_31,op_inf_n_32,op_inf_n_33,op_inf_n_34,op_inf_n_35,op_inf_n_36,op_inf_n_37,op_inf_n_38,op_inf_n_39,op_inf_n_40,op_inf_n_41,op_inf_n_42}),
        .\sband_ts_r_reg[7] (sband_ts_r),
        .sband_tu_r(sband_tu_r),
        .vfb_clk(vfb_clk),
        .\vfb_data_reg[0] (axis_dconverter_n_26),
        .\vfb_data_reg[10] (axis_dconverter_n_16),
        .\vfb_data_reg[11] (axis_dconverter_n_15),
        .\vfb_data_reg[12] (axis_dconverter_n_14),
        .\vfb_data_reg[13] (axis_dconverter_n_13),
        .\vfb_data_reg[14] (axis_dconverter_n_12),
        .\vfb_data_reg[15] (axis_dconverter_n_11),
        .\vfb_data_reg[1] (axis_dconverter_n_25),
        .\vfb_data_reg[2] (axis_dconverter_n_24),
        .\vfb_data_reg[3] (axis_dconverter_n_23),
        .\vfb_data_reg[4] (axis_dconverter_n_22),
        .\vfb_data_reg[5] (axis_dconverter_n_21),
        .\vfb_data_reg[6] (axis_dconverter_n_20),
        .\vfb_data_reg[7] (axis_dconverter_n_19),
        .\vfb_data_reg[8] (axis_dconverter_n_18),
        .\vfb_data_reg[9] (axis_dconverter_n_17),
        .vfb_eol_reg(op_inf_n_7),
        .\vfb_sof_reg[0] (axis_dconverter_n_2));
  LUT1 #(
    .INIT(2'h1)) 
    i_76
       (.I0(s_axis_aresetn),
        .O(n_0_76));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vfb_v1_0_11_op_inf op_inf
       (.D(p_1_in),
        .E(reorder_n_49),
        .Q(sband_ts_r),
        .\buf_data_reg[0][100] (reorder_n_3),
        .\buf_data_reg[0][133] (sband_td_r),
        .\buf_data_reg[0][71] ({sband_ts,sband_ts__0}),
        .\buf_valid_reg[0] (op_inf_n_9),
        .cur_dtype_sink_reg(reorder_n_1),
        .cur_dtype_udef(cur_dtype_udef),
        .m_axis_tvalid(mdt_tv),
        .mdt_tr(mdt_tr),
        .s_axis_aresetn(s_axis_aresetn),
        .s_axis_aresetn_0(reorder_n_48),
        .sband_tact0(sband_tact0),
        .sband_tl(sband_tl),
        .sband_tl_r_reg_0(op_inf_n_10),
        .sband_tu(sband_tu),
        .sband_tu_r(sband_tu_r),
        .sdt_tr(sdt_tr),
        .\state_reg[1] (axis_dconverter_n_2),
        .\state_reg[1]_0 (axis_dconverter_n_11),
        .\state_reg[1]_1 (axis_dconverter_n_12),
        .\state_reg[1]_10 (axis_dconverter_n_21),
        .\state_reg[1]_11 (axis_dconverter_n_22),
        .\state_reg[1]_12 (axis_dconverter_n_23),
        .\state_reg[1]_13 (axis_dconverter_n_24),
        .\state_reg[1]_14 (axis_dconverter_n_25),
        .\state_reg[1]_15 (axis_dconverter_n_26),
        .\state_reg[1]_2 (axis_dconverter_n_13),
        .\state_reg[1]_3 (axis_dconverter_n_14),
        .\state_reg[1]_4 (axis_dconverter_n_15),
        .\state_reg[1]_5 (axis_dconverter_n_16),
        .\state_reg[1]_6 (axis_dconverter_n_17),
        .\state_reg[1]_7 (axis_dconverter_n_18),
        .\state_reg[1]_8 (axis_dconverter_n_19),
        .\state_reg[1]_9 (axis_dconverter_n_20),
        .vfb_clk(vfb_clk),
        .vfb_data(vfb_data),
        .\vfb_data_reg[15]_0 ({op_inf_n_27,op_inf_n_28,op_inf_n_29,op_inf_n_30,op_inf_n_31,op_inf_n_32,op_inf_n_33,op_inf_n_34,op_inf_n_35,op_inf_n_36,op_inf_n_37,op_inf_n_38,op_inf_n_39,op_inf_n_40,op_inf_n_41,op_inf_n_42}),
        .vfb_eol(vfb_eol),
        .vfb_eol_reg_0(op_inf_n_6),
        .vfb_eol_reg_1(op_inf_n_7),
        .vfb_ready(vfb_ready),
        .vfb_sof(vfb_sof),
        .\vfb_sof_reg[0]_0 (op_inf_n_8),
        .vfb_valid(vfb_valid),
        .vfb_vcdt(vfb_vcdt));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vfb_v1_0_11_reorder reorder
       (.D({s_axis_tlast,s_axis_tdata,s_axis_tkeep[2],s_axis_tuser[69:64],s_axis_tuser[0],s_axis_tdest}),
        .E(reorder_n_49),
        .Q({s_axis_tdata__0,sband_ts,reorder_n_44,sband_ts__0}),
        .\buf_data_reg[1][134]_0 (reorder_n_48),
        .cur_dtype_sink_reg_0(reorder_n_1),
        .cur_dtype_sink_reg_1(op_inf_n_9),
        .cur_dtype_udef(cur_dtype_udef),
        .m_axis_tlast(m_axis_tlast),
        .m_axis_tvalid(mdt_tv),
        .s_axis_aclk(s_axis_aclk),
        .s_axis_aresetn(s_axis_aresetn),
        .s_axis_tready(s_axis_tready),
        .s_axis_tvalid(s_axis_tvalid),
        .sband_tact0(sband_tact0),
        .\sband_td_r_reg[15] (sband_td_r),
        .sband_tl(sband_tl),
        .sband_tu(sband_tu),
        .sdt_tv(sdt_tv),
        .\state_reg[1] (op_inf_n_6),
        .vfb_eol(vfb_eol),
        .vfb_eol_reg(reorder_n_3),
        .vfb_ready(vfb_ready),
        .vfb_valid(vfb_valid),
        .vfb_valid_reg(op_inf_n_10));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vfb_v1_0_11_op_inf
   (vfb_eol,
    mdt_tr,
    vfb_valid,
    sband_tu_r,
    vfb_sof,
    sdt_tr,
    vfb_eol_reg_0,
    vfb_eol_reg_1,
    \vfb_sof_reg[0]_0 ,
    \buf_valid_reg[0] ,
    sband_tl_r_reg_0,
    Q,
    vfb_vcdt,
    \vfb_data_reg[15]_0 ,
    vfb_data,
    s_axis_aresetn_0,
    sband_tact0,
    sband_tl,
    vfb_clk,
    \buf_data_reg[0][100] ,
    sband_tu,
    m_axis_tvalid,
    vfb_ready,
    cur_dtype_sink_reg,
    cur_dtype_udef,
    \state_reg[1] ,
    s_axis_aresetn,
    \buf_data_reg[0][71] ,
    D,
    E,
    \buf_data_reg[0][133] ,
    \state_reg[1]_0 ,
    \state_reg[1]_1 ,
    \state_reg[1]_2 ,
    \state_reg[1]_3 ,
    \state_reg[1]_4 ,
    \state_reg[1]_5 ,
    \state_reg[1]_6 ,
    \state_reg[1]_7 ,
    \state_reg[1]_8 ,
    \state_reg[1]_9 ,
    \state_reg[1]_10 ,
    \state_reg[1]_11 ,
    \state_reg[1]_12 ,
    \state_reg[1]_13 ,
    \state_reg[1]_14 ,
    \state_reg[1]_15 );
  output vfb_eol;
  output mdt_tr;
  output vfb_valid;
  output sband_tu_r;
  output [0:0]vfb_sof;
  output sdt_tr;
  output vfb_eol_reg_0;
  output vfb_eol_reg_1;
  output \vfb_sof_reg[0]_0 ;
  output \buf_valid_reg[0] ;
  output sband_tl_r_reg_0;
  output [7:0]Q;
  output [7:0]vfb_vcdt;
  output [15:0]\vfb_data_reg[15]_0 ;
  output [15:0]vfb_data;
  input s_axis_aresetn_0;
  input sband_tact0;
  input sband_tl;
  input vfb_clk;
  input \buf_data_reg[0][100] ;
  input sband_tu;
  input m_axis_tvalid;
  input vfb_ready;
  input cur_dtype_sink_reg;
  input cur_dtype_udef;
  input \state_reg[1] ;
  input s_axis_aresetn;
  input [7:0]\buf_data_reg[0][71] ;
  input [7:0]D;
  input [0:0]E;
  input [15:0]\buf_data_reg[0][133] ;
  input \state_reg[1]_0 ;
  input \state_reg[1]_1 ;
  input \state_reg[1]_2 ;
  input \state_reg[1]_3 ;
  input \state_reg[1]_4 ;
  input \state_reg[1]_5 ;
  input \state_reg[1]_6 ;
  input \state_reg[1]_7 ;
  input \state_reg[1]_8 ;
  input \state_reg[1]_9 ;
  input \state_reg[1]_10 ;
  input \state_reg[1]_11 ;
  input \state_reg[1]_12 ;
  input \state_reg[1]_13 ;
  input \state_reg[1]_14 ;
  input \state_reg[1]_15 ;

  wire [7:0]D;
  wire [0:0]E;
  wire [7:0]Q;
  wire \buf_data_reg[0][100] ;
  wire [15:0]\buf_data_reg[0][133] ;
  wire [7:0]\buf_data_reg[0][71] ;
  wire \buf_valid_reg[0] ;
  wire [3:1]cnt_done0;
  wire cur_dtype_sink_reg;
  wire cur_dtype_udef;
  wire m_axis_tvalid;
  wire mdt_tr;
  wire s_axis_aresetn;
  wire s_axis_aresetn_0;
  wire sband_tact;
  wire sband_tact0;
  wire sband_tact_i_1_n_0;
  wire sband_tact_i_2_n_0;
  wire sband_tl;
  wire sband_tl_r;
  wire sband_tl_r_reg_0;
  wire sband_tr2;
  wire sband_tu;
  wire sband_tu_r;
  wire sdt_tr;
  wire sdt_tr_INST_0_i_1_n_0;
  wire \state_reg[1] ;
  wire \state_reg[1]_0 ;
  wire \state_reg[1]_1 ;
  wire \state_reg[1]_10 ;
  wire \state_reg[1]_11 ;
  wire \state_reg[1]_12 ;
  wire \state_reg[1]_13 ;
  wire \state_reg[1]_14 ;
  wire \state_reg[1]_15 ;
  wire \state_reg[1]_2 ;
  wire \state_reg[1]_3 ;
  wire \state_reg[1]_4 ;
  wire \state_reg[1]_5 ;
  wire \state_reg[1]_6 ;
  wire \state_reg[1]_7 ;
  wire \state_reg[1]_8 ;
  wire \state_reg[1]_9 ;
  wire vfb_clk;
  wire \vfb_cnt[3]_i_1_n_0 ;
  wire \vfb_cnt[3]_i_4_n_0 ;
  wire [3:1]vfb_cnt_reg__0;
  wire [15:0]vfb_data;
  wire \vfb_data[15]_i_1_n_0 ;
  wire [15:0]\vfb_data_reg[15]_0 ;
  wire vfb_eol;
  wire vfb_eol_reg_0;
  wire vfb_eol_reg_1;
  wire vfb_ready;
  wire [0:0]vfb_sof;
  wire \vfb_sof[0]_i_1_n_0 ;
  wire \vfb_sof[0]_i_3_n_0 ;
  wire \vfb_sof_reg[0]_0 ;
  wire vfb_valid;
  wire vfb_valid_i_2_n_0;
  wire [7:0]vfb_vcdt;
  wire \vfb_vcdt[7]_i_1_n_0 ;

  LUT6 #(
    .INIT(64'h5555551055555555)) 
    \buf_valid[1]_i_2 
       (.I0(cur_dtype_sink_reg),
        .I1(sdt_tr_INST_0_i_1_n_0),
        .I2(vfb_valid),
        .I3(vfb_eol),
        .I4(m_axis_tvalid),
        .I5(cur_dtype_udef),
        .O(\buf_valid_reg[0] ));
  LUT2 #(
    .INIT(4'hB)) 
    mdt_tr_INST_0
       (.I0(vfb_ready),
        .I1(vfb_valid),
        .O(mdt_tr));
  LUT3 #(
    .INIT(8'hBA)) 
    sband_tact_i_1
       (.I0(sband_tact0),
        .I1(sband_tact_i_2_n_0),
        .I2(sband_tact),
        .O(sband_tact_i_1_n_0));
  LUT6 #(
    .INIT(64'hFF00000004000000)) 
    sband_tact_i_2
       (.I0(vfb_cnt_reg__0[2]),
        .I1(vfb_cnt_reg__0[1]),
        .I2(vfb_cnt_reg__0[3]),
        .I3(vfb_valid),
        .I4(vfb_ready),
        .I5(vfb_eol),
        .O(sband_tact_i_2_n_0));
  FDRE sband_tact_reg
       (.C(vfb_clk),
        .CE(1'b1),
        .D(sband_tact_i_1_n_0),
        .Q(sband_tact),
        .R(s_axis_aresetn_0));
  FDRE \sband_td_r_reg[0] 
       (.C(vfb_clk),
        .CE(E),
        .D(\buf_data_reg[0][133] [0]),
        .Q(\vfb_data_reg[15]_0 [0]),
        .R(s_axis_aresetn_0));
  FDRE \sband_td_r_reg[10] 
       (.C(vfb_clk),
        .CE(E),
        .D(\buf_data_reg[0][133] [10]),
        .Q(\vfb_data_reg[15]_0 [10]),
        .R(s_axis_aresetn_0));
  FDRE \sband_td_r_reg[11] 
       (.C(vfb_clk),
        .CE(E),
        .D(\buf_data_reg[0][133] [11]),
        .Q(\vfb_data_reg[15]_0 [11]),
        .R(s_axis_aresetn_0));
  FDRE \sband_td_r_reg[12] 
       (.C(vfb_clk),
        .CE(E),
        .D(\buf_data_reg[0][133] [12]),
        .Q(\vfb_data_reg[15]_0 [12]),
        .R(s_axis_aresetn_0));
  FDRE \sband_td_r_reg[13] 
       (.C(vfb_clk),
        .CE(E),
        .D(\buf_data_reg[0][133] [13]),
        .Q(\vfb_data_reg[15]_0 [13]),
        .R(s_axis_aresetn_0));
  FDRE \sband_td_r_reg[14] 
       (.C(vfb_clk),
        .CE(E),
        .D(\buf_data_reg[0][133] [14]),
        .Q(\vfb_data_reg[15]_0 [14]),
        .R(s_axis_aresetn_0));
  FDRE \sband_td_r_reg[15] 
       (.C(vfb_clk),
        .CE(E),
        .D(\buf_data_reg[0][133] [15]),
        .Q(\vfb_data_reg[15]_0 [15]),
        .R(s_axis_aresetn_0));
  FDRE \sband_td_r_reg[1] 
       (.C(vfb_clk),
        .CE(E),
        .D(\buf_data_reg[0][133] [1]),
        .Q(\vfb_data_reg[15]_0 [1]),
        .R(s_axis_aresetn_0));
  FDRE \sband_td_r_reg[2] 
       (.C(vfb_clk),
        .CE(E),
        .D(\buf_data_reg[0][133] [2]),
        .Q(\vfb_data_reg[15]_0 [2]),
        .R(s_axis_aresetn_0));
  FDRE \sband_td_r_reg[3] 
       (.C(vfb_clk),
        .CE(E),
        .D(\buf_data_reg[0][133] [3]),
        .Q(\vfb_data_reg[15]_0 [3]),
        .R(s_axis_aresetn_0));
  FDRE \sband_td_r_reg[4] 
       (.C(vfb_clk),
        .CE(E),
        .D(\buf_data_reg[0][133] [4]),
        .Q(\vfb_data_reg[15]_0 [4]),
        .R(s_axis_aresetn_0));
  FDRE \sband_td_r_reg[5] 
       (.C(vfb_clk),
        .CE(E),
        .D(\buf_data_reg[0][133] [5]),
        .Q(\vfb_data_reg[15]_0 [5]),
        .R(s_axis_aresetn_0));
  FDRE \sband_td_r_reg[6] 
       (.C(vfb_clk),
        .CE(E),
        .D(\buf_data_reg[0][133] [6]),
        .Q(\vfb_data_reg[15]_0 [6]),
        .R(s_axis_aresetn_0));
  FDRE \sband_td_r_reg[7] 
       (.C(vfb_clk),
        .CE(E),
        .D(\buf_data_reg[0][133] [7]),
        .Q(\vfb_data_reg[15]_0 [7]),
        .R(s_axis_aresetn_0));
  FDRE \sband_td_r_reg[8] 
       (.C(vfb_clk),
        .CE(E),
        .D(\buf_data_reg[0][133] [8]),
        .Q(\vfb_data_reg[15]_0 [8]),
        .R(s_axis_aresetn_0));
  FDRE \sband_td_r_reg[9] 
       (.C(vfb_clk),
        .CE(E),
        .D(\buf_data_reg[0][133] [9]),
        .Q(\vfb_data_reg[15]_0 [9]),
        .R(s_axis_aresetn_0));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'hAAAAA2AA)) 
    sband_tl_r_i_3
       (.I0(vfb_valid),
        .I1(vfb_ready),
        .I2(vfb_cnt_reg__0[2]),
        .I3(vfb_cnt_reg__0[1]),
        .I4(vfb_cnt_reg__0[3]),
        .O(sband_tl_r_reg_0));
  FDRE sband_tl_r_reg
       (.C(vfb_clk),
        .CE(sband_tact0),
        .D(sband_tl),
        .Q(sband_tl_r),
        .R(s_axis_aresetn_0));
  FDRE \sband_ts_r_reg[0] 
       (.C(vfb_clk),
        .CE(sband_tact0),
        .D(\buf_data_reg[0][71] [0]),
        .Q(Q[0]),
        .R(s_axis_aresetn_0));
  FDRE \sband_ts_r_reg[1] 
       (.C(vfb_clk),
        .CE(sband_tact0),
        .D(\buf_data_reg[0][71] [1]),
        .Q(Q[1]),
        .R(s_axis_aresetn_0));
  FDRE \sband_ts_r_reg[2] 
       (.C(vfb_clk),
        .CE(sband_tact0),
        .D(\buf_data_reg[0][71] [2]),
        .Q(Q[2]),
        .R(s_axis_aresetn_0));
  FDRE \sband_ts_r_reg[3] 
       (.C(vfb_clk),
        .CE(sband_tact0),
        .D(\buf_data_reg[0][71] [3]),
        .Q(Q[3]),
        .R(s_axis_aresetn_0));
  FDRE \sband_ts_r_reg[4] 
       (.C(vfb_clk),
        .CE(sband_tact0),
        .D(\buf_data_reg[0][71] [4]),
        .Q(Q[4]),
        .R(s_axis_aresetn_0));
  FDRE \sband_ts_r_reg[5] 
       (.C(vfb_clk),
        .CE(sband_tact0),
        .D(\buf_data_reg[0][71] [5]),
        .Q(Q[5]),
        .R(s_axis_aresetn_0));
  FDRE \sband_ts_r_reg[6] 
       (.C(vfb_clk),
        .CE(sband_tact0),
        .D(\buf_data_reg[0][71] [6]),
        .Q(Q[6]),
        .R(s_axis_aresetn_0));
  FDRE \sband_ts_r_reg[7] 
       (.C(vfb_clk),
        .CE(sband_tact0),
        .D(\buf_data_reg[0][71] [7]),
        .Q(Q[7]),
        .R(s_axis_aresetn_0));
  FDRE \sband_tu_r_reg[0] 
       (.C(vfb_clk),
        .CE(sband_tact0),
        .D(sband_tu),
        .Q(sband_tu_r),
        .R(s_axis_aresetn_0));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h1101)) 
    sdt_tr_INST_0
       (.I0(m_axis_tvalid),
        .I1(vfb_eol),
        .I2(vfb_valid),
        .I3(sdt_tr_INST_0_i_1_n_0),
        .O(sdt_tr));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'h04000000)) 
    sdt_tr_INST_0_i_1
       (.I0(vfb_cnt_reg__0[3]),
        .I1(vfb_cnt_reg__0[1]),
        .I2(vfb_cnt_reg__0[2]),
        .I3(vfb_ready),
        .I4(vfb_valid),
        .O(sdt_tr_INST_0_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \vfb_cnt[1]_i_1 
       (.I0(vfb_cnt_reg__0[1]),
        .O(cnt_done0[1]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \vfb_cnt[2]_i_1 
       (.I0(vfb_cnt_reg__0[2]),
        .I1(vfb_cnt_reg__0[1]),
        .O(cnt_done0[2]));
  LUT6 #(
    .INIT(64'h22222322FFFFFFFF)) 
    \vfb_cnt[3]_i_1 
       (.I0(vfb_eol),
        .I1(\vfb_cnt[3]_i_4_n_0 ),
        .I2(vfb_cnt_reg__0[3]),
        .I3(vfb_cnt_reg__0[1]),
        .I4(vfb_cnt_reg__0[2]),
        .I5(s_axis_aresetn),
        .O(\vfb_cnt[3]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \vfb_cnt[3]_i_2 
       (.I0(vfb_ready),
        .I1(vfb_valid),
        .O(sband_tr2));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \vfb_cnt[3]_i_3 
       (.I0(vfb_cnt_reg__0[3]),
        .I1(vfb_cnt_reg__0[1]),
        .I2(vfb_cnt_reg__0[2]),
        .O(cnt_done0[3]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \vfb_cnt[3]_i_4 
       (.I0(vfb_valid),
        .I1(vfb_ready),
        .O(\vfb_cnt[3]_i_4_n_0 ));
  FDRE \vfb_cnt_reg[1] 
       (.C(vfb_clk),
        .CE(sband_tr2),
        .D(cnt_done0[1]),
        .Q(vfb_cnt_reg__0[1]),
        .R(\vfb_cnt[3]_i_1_n_0 ));
  FDRE \vfb_cnt_reg[2] 
       (.C(vfb_clk),
        .CE(sband_tr2),
        .D(cnt_done0[2]),
        .Q(vfb_cnt_reg__0[2]),
        .R(\vfb_cnt[3]_i_1_n_0 ));
  FDRE \vfb_cnt_reg[3] 
       (.C(vfb_clk),
        .CE(sband_tr2),
        .D(cnt_done0[3]),
        .Q(vfb_cnt_reg__0[3]),
        .R(\vfb_cnt[3]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hDD5D)) 
    \vfb_data[15]_i_1 
       (.I0(vfb_valid),
        .I1(vfb_ready),
        .I2(vfb_eol),
        .I3(m_axis_tvalid),
        .O(\vfb_data[15]_i_1_n_0 ));
  FDRE \vfb_data_reg[0] 
       (.C(vfb_clk),
        .CE(\vfb_data[15]_i_1_n_0 ),
        .D(\state_reg[1]_15 ),
        .Q(vfb_data[0]),
        .R(s_axis_aresetn_0));
  FDRE \vfb_data_reg[10] 
       (.C(vfb_clk),
        .CE(\vfb_data[15]_i_1_n_0 ),
        .D(\state_reg[1]_5 ),
        .Q(vfb_data[10]),
        .R(s_axis_aresetn_0));
  FDRE \vfb_data_reg[11] 
       (.C(vfb_clk),
        .CE(\vfb_data[15]_i_1_n_0 ),
        .D(\state_reg[1]_4 ),
        .Q(vfb_data[11]),
        .R(s_axis_aresetn_0));
  FDRE \vfb_data_reg[12] 
       (.C(vfb_clk),
        .CE(\vfb_data[15]_i_1_n_0 ),
        .D(\state_reg[1]_3 ),
        .Q(vfb_data[12]),
        .R(s_axis_aresetn_0));
  FDRE \vfb_data_reg[13] 
       (.C(vfb_clk),
        .CE(\vfb_data[15]_i_1_n_0 ),
        .D(\state_reg[1]_2 ),
        .Q(vfb_data[13]),
        .R(s_axis_aresetn_0));
  FDRE \vfb_data_reg[14] 
       (.C(vfb_clk),
        .CE(\vfb_data[15]_i_1_n_0 ),
        .D(\state_reg[1]_1 ),
        .Q(vfb_data[14]),
        .R(s_axis_aresetn_0));
  FDRE \vfb_data_reg[15] 
       (.C(vfb_clk),
        .CE(\vfb_data[15]_i_1_n_0 ),
        .D(\state_reg[1]_0 ),
        .Q(vfb_data[15]),
        .R(s_axis_aresetn_0));
  FDRE \vfb_data_reg[1] 
       (.C(vfb_clk),
        .CE(\vfb_data[15]_i_1_n_0 ),
        .D(\state_reg[1]_14 ),
        .Q(vfb_data[1]),
        .R(s_axis_aresetn_0));
  FDRE \vfb_data_reg[2] 
       (.C(vfb_clk),
        .CE(\vfb_data[15]_i_1_n_0 ),
        .D(\state_reg[1]_13 ),
        .Q(vfb_data[2]),
        .R(s_axis_aresetn_0));
  FDRE \vfb_data_reg[3] 
       (.C(vfb_clk),
        .CE(\vfb_data[15]_i_1_n_0 ),
        .D(\state_reg[1]_12 ),
        .Q(vfb_data[3]),
        .R(s_axis_aresetn_0));
  FDRE \vfb_data_reg[4] 
       (.C(vfb_clk),
        .CE(\vfb_data[15]_i_1_n_0 ),
        .D(\state_reg[1]_11 ),
        .Q(vfb_data[4]),
        .R(s_axis_aresetn_0));
  FDRE \vfb_data_reg[5] 
       (.C(vfb_clk),
        .CE(\vfb_data[15]_i_1_n_0 ),
        .D(\state_reg[1]_10 ),
        .Q(vfb_data[5]),
        .R(s_axis_aresetn_0));
  FDRE \vfb_data_reg[6] 
       (.C(vfb_clk),
        .CE(\vfb_data[15]_i_1_n_0 ),
        .D(\state_reg[1]_9 ),
        .Q(vfb_data[6]),
        .R(s_axis_aresetn_0));
  FDRE \vfb_data_reg[7] 
       (.C(vfb_clk),
        .CE(\vfb_data[15]_i_1_n_0 ),
        .D(\state_reg[1]_8 ),
        .Q(vfb_data[7]),
        .R(s_axis_aresetn_0));
  FDRE \vfb_data_reg[8] 
       (.C(vfb_clk),
        .CE(\vfb_data[15]_i_1_n_0 ),
        .D(\state_reg[1]_7 ),
        .Q(vfb_data[8]),
        .R(s_axis_aresetn_0));
  FDRE \vfb_data_reg[9] 
       (.C(vfb_clk),
        .CE(\vfb_data[15]_i_1_n_0 ),
        .D(\state_reg[1]_6 ),
        .Q(vfb_data[9]),
        .R(s_axis_aresetn_0));
  LUT5 #(
    .INIT(32'hEEEEEFFF)) 
    vfb_eol_i_2
       (.I0(vfb_eol_reg_1),
        .I1(m_axis_tvalid),
        .I2(sband_tl_r),
        .I3(\vfb_sof_reg[0]_0 ),
        .I4(sband_tact0),
        .O(vfb_eol_reg_0));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h80)) 
    vfb_eol_i_3
       (.I0(vfb_eol),
        .I1(vfb_ready),
        .I2(vfb_valid),
        .O(vfb_eol_reg_1));
  FDRE vfb_eol_reg
       (.C(vfb_clk),
        .CE(mdt_tr),
        .D(\buf_data_reg[0][100] ),
        .Q(vfb_eol),
        .R(s_axis_aresetn_0));
  LUT6 #(
    .INIT(64'hBBBFBBBB88808888)) 
    \vfb_sof[0]_i_1 
       (.I0(\state_reg[1] ),
        .I1(mdt_tr),
        .I2(sband_tact0),
        .I3(\vfb_sof_reg[0]_0 ),
        .I4(\vfb_sof[0]_i_3_n_0 ),
        .I5(vfb_sof),
        .O(\vfb_sof[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h1555)) 
    \vfb_sof[0]_i_3 
       (.I0(m_axis_tvalid),
        .I1(vfb_valid),
        .I2(vfb_ready),
        .I3(vfb_eol),
        .O(\vfb_sof[0]_i_3_n_0 ));
  FDRE \vfb_sof_reg[0] 
       (.C(vfb_clk),
        .CE(1'b1),
        .D(\vfb_sof[0]_i_1_n_0 ),
        .Q(vfb_sof),
        .R(s_axis_aresetn_0));
  LUT6 #(
    .INIT(64'hBFFFBFFFBFFFAAAA)) 
    vfb_valid_i_2
       (.I0(m_axis_tvalid),
        .I1(vfb_eol),
        .I2(vfb_ready),
        .I3(vfb_valid),
        .I4(sband_tact0),
        .I5(\vfb_sof_reg[0]_0 ),
        .O(vfb_valid_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'hAA8A)) 
    vfb_valid_i_3
       (.I0(sband_tact),
        .I1(vfb_cnt_reg__0[3]),
        .I2(vfb_cnt_reg__0[1]),
        .I3(vfb_cnt_reg__0[2]),
        .O(\vfb_sof_reg[0]_0 ));
  FDRE vfb_valid_reg
       (.C(vfb_clk),
        .CE(mdt_tr),
        .D(vfb_valid_i_2_n_0),
        .Q(vfb_valid),
        .R(s_axis_aresetn_0));
  LUT5 #(
    .INIT(32'hDDDDDDD0)) 
    \vfb_vcdt[7]_i_1 
       (.I0(vfb_valid),
        .I1(vfb_ready),
        .I2(sband_tact0),
        .I3(sband_tact),
        .I4(m_axis_tvalid),
        .O(\vfb_vcdt[7]_i_1_n_0 ));
  FDRE \vfb_vcdt_reg[0] 
       (.C(vfb_clk),
        .CE(\vfb_vcdt[7]_i_1_n_0 ),
        .D(D[0]),
        .Q(vfb_vcdt[0]),
        .R(s_axis_aresetn_0));
  FDRE \vfb_vcdt_reg[1] 
       (.C(vfb_clk),
        .CE(\vfb_vcdt[7]_i_1_n_0 ),
        .D(D[1]),
        .Q(vfb_vcdt[1]),
        .R(s_axis_aresetn_0));
  FDRE \vfb_vcdt_reg[2] 
       (.C(vfb_clk),
        .CE(\vfb_vcdt[7]_i_1_n_0 ),
        .D(D[2]),
        .Q(vfb_vcdt[2]),
        .R(s_axis_aresetn_0));
  FDRE \vfb_vcdt_reg[3] 
       (.C(vfb_clk),
        .CE(\vfb_vcdt[7]_i_1_n_0 ),
        .D(D[3]),
        .Q(vfb_vcdt[3]),
        .R(s_axis_aresetn_0));
  FDRE \vfb_vcdt_reg[4] 
       (.C(vfb_clk),
        .CE(\vfb_vcdt[7]_i_1_n_0 ),
        .D(D[4]),
        .Q(vfb_vcdt[4]),
        .R(s_axis_aresetn_0));
  FDRE \vfb_vcdt_reg[5] 
       (.C(vfb_clk),
        .CE(\vfb_vcdt[7]_i_1_n_0 ),
        .D(D[5]),
        .Q(vfb_vcdt[5]),
        .R(s_axis_aresetn_0));
  FDRE \vfb_vcdt_reg[6] 
       (.C(vfb_clk),
        .CE(\vfb_vcdt[7]_i_1_n_0 ),
        .D(D[6]),
        .Q(vfb_vcdt[6]),
        .R(s_axis_aresetn_0));
  FDRE \vfb_vcdt_reg[7] 
       (.C(vfb_clk),
        .CE(\vfb_vcdt[7]_i_1_n_0 ),
        .D(D[7]),
        .Q(vfb_vcdt[7]),
        .R(s_axis_aresetn_0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vfb_v1_0_11_reorder
   (cur_dtype_udef,
    cur_dtype_sink_reg_0,
    s_axis_tready,
    vfb_eol_reg,
    sband_tl,
    sband_tact0,
    \sband_td_r_reg[15] ,
    Q,
    sdt_tv,
    \buf_data_reg[1][134]_0 ,
    E,
    sband_tu,
    s_axis_aclk,
    \state_reg[1] ,
    m_axis_tlast,
    m_axis_tvalid,
    vfb_valid_reg,
    vfb_eol,
    s_axis_tvalid,
    cur_dtype_sink_reg_1,
    s_axis_aresetn,
    vfb_ready,
    vfb_valid,
    D);
  output cur_dtype_udef;
  output cur_dtype_sink_reg_0;
  output s_axis_tready;
  output vfb_eol_reg;
  output sband_tl;
  output sband_tact0;
  output [15:0]\sband_td_r_reg[15] ;
  output [24:0]Q;
  output sdt_tv;
  output \buf_data_reg[1][134]_0 ;
  output [0:0]E;
  output sband_tu;
  input s_axis_aclk;
  input \state_reg[1] ;
  input m_axis_tlast;
  input m_axis_tvalid;
  input vfb_valid_reg;
  input vfb_eol;
  input s_axis_tvalid;
  input cur_dtype_sink_reg_1;
  input s_axis_aresetn;
  input vfb_ready;
  input vfb_valid;
  input [42:0]D;

  wire [42:0]D;
  wire [0:0]E;
  wire [24:0]Q;
  wire \buf_data[0][134]_i_1_n_0 ;
  wire \buf_data[1][134]_i_1_n_0 ;
  wire [134:0]\buf_data_reg[1] ;
  wire \buf_data_reg[1][134]_0 ;
  wire \buf_valid[0]_i_1_n_0 ;
  wire \buf_valid[1]_i_1_n_0 ;
  wire \buf_valid_reg_n_0_[0] ;
  wire cur_dtype_sink_i_1_n_0;
  wire cur_dtype_sink_reg_0;
  wire cur_dtype_sink_reg_1;
  wire cur_dtype_udef;
  wire cur_dtype_udef_i_1_n_0;
  wire cur_dtype_udef_i_2_n_0;
  wire m_axis_tlast;
  wire m_axis_tvalid;
  wire p_0_in;
  wire [134:0]p_2_in;
  wire s_axis_aclk;
  wire s_axis_aresetn;
  wire [31:16]s_axis_tdata;
  wire s_axis_tlast;
  wire s_axis_tready;
  wire s_axis_tvalid;
  wire sband_tact0;
  wire [15:0]\sband_td_r_reg[15] ;
  wire [2:2]sband_tk;
  wire sband_tl;
  wire sband_tu;
  wire sdt_tv;
  wire sdt_tv_INST_0_i_1_n_0;
  wire \state_reg[1] ;
  wire vfb_eol;
  wire vfb_eol_reg;
  wire vfb_ready;
  wire vfb_valid;
  wire vfb_valid_reg;

  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][0]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [0]),
        .I4(D[0]),
        .O(p_2_in[0]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][100]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [100]),
        .I4(D[9]),
        .O(p_2_in[100]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][102]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [102]),
        .I4(D[10]),
        .O(p_2_in[102]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][103]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [103]),
        .I4(D[11]),
        .O(p_2_in[103]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][104]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [104]),
        .I4(D[12]),
        .O(p_2_in[104]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][105]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [105]),
        .I4(D[13]),
        .O(p_2_in[105]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][106]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [106]),
        .I4(D[14]),
        .O(p_2_in[106]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][107]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [107]),
        .I4(D[15]),
        .O(p_2_in[107]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][108]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [108]),
        .I4(D[16]),
        .O(p_2_in[108]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][109]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [109]),
        .I4(D[17]),
        .O(p_2_in[109]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][110]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [110]),
        .I4(D[18]),
        .O(p_2_in[110]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][111]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [111]),
        .I4(D[19]),
        .O(p_2_in[111]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][112]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [112]),
        .I4(D[20]),
        .O(p_2_in[112]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][113]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [113]),
        .I4(D[21]),
        .O(p_2_in[113]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][114]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [114]),
        .I4(D[22]),
        .O(p_2_in[114]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][115]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [115]),
        .I4(D[23]),
        .O(p_2_in[115]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][116]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [116]),
        .I4(D[24]),
        .O(p_2_in[116]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][117]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [117]),
        .I4(D[25]),
        .O(p_2_in[117]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][118]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [118]),
        .I4(D[26]),
        .O(p_2_in[118]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][119]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [119]),
        .I4(D[27]),
        .O(p_2_in[119]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][120]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [120]),
        .I4(D[28]),
        .O(p_2_in[120]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][121]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [121]),
        .I4(D[29]),
        .O(p_2_in[121]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][122]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [122]),
        .I4(D[30]),
        .O(p_2_in[122]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][123]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [123]),
        .I4(D[31]),
        .O(p_2_in[123]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][124]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [124]),
        .I4(D[32]),
        .O(p_2_in[124]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][125]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [125]),
        .I4(D[33]),
        .O(p_2_in[125]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][126]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [126]),
        .I4(D[34]),
        .O(p_2_in[126]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][127]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [127]),
        .I4(D[35]),
        .O(p_2_in[127]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][128]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [128]),
        .I4(D[36]),
        .O(p_2_in[128]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][129]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [129]),
        .I4(D[37]),
        .O(p_2_in[129]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][130]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [130]),
        .I4(D[38]),
        .O(p_2_in[130]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][131]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [131]),
        .I4(D[39]),
        .O(p_2_in[131]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][132]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [132]),
        .I4(D[40]),
        .O(p_2_in[132]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][133]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [133]),
        .I4(D[41]),
        .O(p_2_in[133]));
  LUT4 #(
    .INIT(16'h04E4)) 
    \buf_data[0][134]_i_1 
       (.I0(p_0_in),
        .I1(s_axis_tvalid),
        .I2(\buf_valid_reg_n_0_[0] ),
        .I3(cur_dtype_sink_reg_1),
        .O(\buf_data[0][134]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][134]_i_2 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [134]),
        .I4(D[42]),
        .O(p_2_in[134]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][1]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [1]),
        .I4(D[1]),
        .O(p_2_in[1]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][2]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [2]),
        .I4(D[2]),
        .O(p_2_in[2]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][66]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [66]),
        .I4(D[3]),
        .O(p_2_in[66]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][67]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [67]),
        .I4(D[4]),
        .O(p_2_in[67]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][68]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [68]),
        .I4(D[5]),
        .O(p_2_in[68]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][69]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [69]),
        .I4(D[6]),
        .O(p_2_in[69]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][70]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [70]),
        .I4(D[7]),
        .O(p_2_in[70]));
  LUT5 #(
    .INIT(32'hFFF70800)) 
    \buf_data[0][71]_i_1 
       (.I0(p_0_in),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .I3(\buf_data_reg[1] [71]),
        .I4(D[8]),
        .O(p_2_in[71]));
  LUT4 #(
    .INIT(16'h0800)) 
    \buf_data[1][134]_i_1 
       (.I0(cur_dtype_sink_reg_1),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(p_0_in),
        .I3(s_axis_tvalid),
        .O(\buf_data[1][134]_i_1_n_0 ));
  FDRE \buf_data_reg[0][0] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[0]),
        .Q(Q[0]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][100] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[100]),
        .Q(sband_tk),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][102] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[102]),
        .Q(Q[9]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][103] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[103]),
        .Q(Q[10]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][104] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[104]),
        .Q(Q[11]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][105] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[105]),
        .Q(Q[12]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][106] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[106]),
        .Q(Q[13]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][107] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[107]),
        .Q(Q[14]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][108] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[108]),
        .Q(Q[15]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][109] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[109]),
        .Q(Q[16]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][110] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[110]),
        .Q(Q[17]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][111] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[111]),
        .Q(Q[18]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][112] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[112]),
        .Q(Q[19]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][113] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[113]),
        .Q(Q[20]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][114] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[114]),
        .Q(Q[21]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][115] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[115]),
        .Q(Q[22]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][116] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[116]),
        .Q(Q[23]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][117] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[117]),
        .Q(Q[24]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][118] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[118]),
        .Q(s_axis_tdata[16]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][119] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[119]),
        .Q(s_axis_tdata[17]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][120] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[120]),
        .Q(s_axis_tdata[18]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][121] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[121]),
        .Q(s_axis_tdata[19]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][122] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[122]),
        .Q(s_axis_tdata[20]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][123] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[123]),
        .Q(s_axis_tdata[21]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][124] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[124]),
        .Q(s_axis_tdata[22]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][125] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[125]),
        .Q(s_axis_tdata[23]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][126] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[126]),
        .Q(s_axis_tdata[24]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][127] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[127]),
        .Q(s_axis_tdata[25]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][128] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[128]),
        .Q(s_axis_tdata[26]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][129] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[129]),
        .Q(s_axis_tdata[27]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][130] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[130]),
        .Q(s_axis_tdata[28]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][131] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[131]),
        .Q(s_axis_tdata[29]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][132] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[132]),
        .Q(s_axis_tdata[30]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][133] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[133]),
        .Q(s_axis_tdata[31]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][134] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[134]),
        .Q(s_axis_tlast),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][1] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[1]),
        .Q(Q[1]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][2] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[2]),
        .Q(Q[2]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][66] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[66]),
        .Q(Q[3]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][67] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[67]),
        .Q(Q[4]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][68] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[68]),
        .Q(Q[5]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][69] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[69]),
        .Q(Q[6]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][70] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[70]),
        .Q(Q[7]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[0][71] 
       (.C(s_axis_aclk),
        .CE(\buf_data[0][134]_i_1_n_0 ),
        .D(p_2_in[71]),
        .Q(Q[8]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][0] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[0]),
        .Q(\buf_data_reg[1] [0]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][100] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[9]),
        .Q(\buf_data_reg[1] [100]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][102] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[10]),
        .Q(\buf_data_reg[1] [102]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][103] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[11]),
        .Q(\buf_data_reg[1] [103]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][104] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[12]),
        .Q(\buf_data_reg[1] [104]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][105] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[13]),
        .Q(\buf_data_reg[1] [105]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][106] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[14]),
        .Q(\buf_data_reg[1] [106]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][107] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[15]),
        .Q(\buf_data_reg[1] [107]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][108] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[16]),
        .Q(\buf_data_reg[1] [108]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][109] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[17]),
        .Q(\buf_data_reg[1] [109]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][110] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[18]),
        .Q(\buf_data_reg[1] [110]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][111] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[19]),
        .Q(\buf_data_reg[1] [111]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][112] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[20]),
        .Q(\buf_data_reg[1] [112]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][113] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[21]),
        .Q(\buf_data_reg[1] [113]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][114] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[22]),
        .Q(\buf_data_reg[1] [114]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][115] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[23]),
        .Q(\buf_data_reg[1] [115]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][116] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[24]),
        .Q(\buf_data_reg[1] [116]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][117] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[25]),
        .Q(\buf_data_reg[1] [117]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][118] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[26]),
        .Q(\buf_data_reg[1] [118]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][119] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[27]),
        .Q(\buf_data_reg[1] [119]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][120] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[28]),
        .Q(\buf_data_reg[1] [120]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][121] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[29]),
        .Q(\buf_data_reg[1] [121]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][122] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[30]),
        .Q(\buf_data_reg[1] [122]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][123] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[31]),
        .Q(\buf_data_reg[1] [123]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][124] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[32]),
        .Q(\buf_data_reg[1] [124]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][125] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[33]),
        .Q(\buf_data_reg[1] [125]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][126] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[34]),
        .Q(\buf_data_reg[1] [126]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][127] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[35]),
        .Q(\buf_data_reg[1] [127]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][128] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[36]),
        .Q(\buf_data_reg[1] [128]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][129] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[37]),
        .Q(\buf_data_reg[1] [129]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][130] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[38]),
        .Q(\buf_data_reg[1] [130]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][131] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[39]),
        .Q(\buf_data_reg[1] [131]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][132] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[40]),
        .Q(\buf_data_reg[1] [132]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][133] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[41]),
        .Q(\buf_data_reg[1] [133]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][134] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[42]),
        .Q(\buf_data_reg[1] [134]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][1] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[1]),
        .Q(\buf_data_reg[1] [1]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][2] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[2]),
        .Q(\buf_data_reg[1] [2]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][66] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[3]),
        .Q(\buf_data_reg[1] [66]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][67] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[4]),
        .Q(\buf_data_reg[1] [67]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][68] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[5]),
        .Q(\buf_data_reg[1] [68]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][69] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[6]),
        .Q(\buf_data_reg[1] [69]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][70] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[7]),
        .Q(\buf_data_reg[1] [70]),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_data_reg[1][71] 
       (.C(s_axis_aclk),
        .CE(\buf_data[1][134]_i_1_n_0 ),
        .D(D[8]),
        .Q(\buf_data_reg[1] [71]),
        .R(\buf_data_reg[1][134]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT4 #(
    .INIT(16'hF4E4)) 
    \buf_valid[0]_i_1 
       (.I0(p_0_in),
        .I1(s_axis_tvalid),
        .I2(\buf_valid_reg_n_0_[0] ),
        .I3(cur_dtype_sink_reg_1),
        .O(\buf_valid[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT4 #(
    .INIT(16'hB8B0)) 
    \buf_valid[1]_i_1 
       (.I0(cur_dtype_sink_reg_1),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(p_0_in),
        .I3(s_axis_tvalid),
        .O(\buf_valid[1]_i_1_n_0 ));
  FDRE \buf_valid_reg[0] 
       (.C(s_axis_aclk),
        .CE(1'b1),
        .D(\buf_valid[0]_i_1_n_0 ),
        .Q(\buf_valid_reg_n_0_[0] ),
        .R(\buf_data_reg[1][134]_0 ));
  FDRE \buf_valid_reg[1] 
       (.C(s_axis_aclk),
        .CE(1'b1),
        .D(\buf_valid[1]_i_1_n_0 ),
        .Q(p_0_in),
        .R(\buf_data_reg[1][134]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    cur_dtype_sink_i_1
       (.I0(\buf_valid_reg_n_0_[0] ),
        .I1(sdt_tv_INST_0_i_1_n_0),
        .I2(cur_dtype_sink_reg_0),
        .O(cur_dtype_sink_i_1_n_0));
  FDRE cur_dtype_sink_reg
       (.C(s_axis_aclk),
        .CE(1'b1),
        .D(cur_dtype_sink_i_1_n_0),
        .Q(cur_dtype_sink_reg_0),
        .R(cur_dtype_udef_i_1_n_0));
  LUT4 #(
    .INIT(16'h40FF)) 
    cur_dtype_udef_i_1
       (.I0(cur_dtype_sink_reg_1),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(s_axis_tlast),
        .I3(s_axis_aresetn),
        .O(cur_dtype_udef_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    cur_dtype_udef_i_2
       (.I0(cur_dtype_udef),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(sdt_tv_INST_0_i_1_n_0),
        .O(cur_dtype_udef_i_2_n_0));
  FDRE cur_dtype_udef_reg
       (.C(s_axis_aclk),
        .CE(1'b1),
        .D(cur_dtype_udef_i_2_n_0),
        .Q(cur_dtype_udef),
        .R(cur_dtype_udef_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT1 #(
    .INIT(2'h1)) 
    s_axis_tready_INST_0
       (.I0(p_0_in),
        .O(s_axis_tready));
  LUT2 #(
    .INIT(4'h8)) 
    \sband_td_r[0]_i_1 
       (.I0(s_axis_tdata[16]),
        .I1(sband_tact0),
        .O(\sband_td_r_reg[15] [0]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sband_td_r[10]_i_1 
       (.I0(s_axis_tdata[26]),
        .I1(sband_tact0),
        .O(\sband_td_r_reg[15] [10]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sband_td_r[11]_i_1 
       (.I0(s_axis_tdata[27]),
        .I1(sband_tact0),
        .O(\sband_td_r_reg[15] [11]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sband_td_r[12]_i_1 
       (.I0(s_axis_tdata[28]),
        .I1(sband_tact0),
        .O(\sband_td_r_reg[15] [12]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sband_td_r[13]_i_1 
       (.I0(s_axis_tdata[29]),
        .I1(sband_tact0),
        .O(\sband_td_r_reg[15] [13]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sband_td_r[14]_i_1 
       (.I0(s_axis_tdata[30]),
        .I1(sband_tact0),
        .O(\sband_td_r_reg[15] [14]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hEA)) 
    \sband_td_r[15]_i_1 
       (.I0(sband_tact0),
        .I1(vfb_ready),
        .I2(vfb_valid),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sband_td_r[15]_i_2 
       (.I0(s_axis_tdata[31]),
        .I1(sband_tact0),
        .O(\sband_td_r_reg[15] [15]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sband_td_r[1]_i_1 
       (.I0(s_axis_tdata[17]),
        .I1(sband_tact0),
        .O(\sband_td_r_reg[15] [1]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sband_td_r[2]_i_1 
       (.I0(s_axis_tdata[18]),
        .I1(sband_tact0),
        .O(\sband_td_r_reg[15] [2]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sband_td_r[3]_i_1 
       (.I0(s_axis_tdata[19]),
        .I1(sband_tact0),
        .O(\sband_td_r_reg[15] [3]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sband_td_r[4]_i_1 
       (.I0(s_axis_tdata[20]),
        .I1(sband_tact0),
        .O(\sband_td_r_reg[15] [4]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sband_td_r[5]_i_1 
       (.I0(s_axis_tdata[21]),
        .I1(sband_tact0),
        .O(\sband_td_r_reg[15] [5]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sband_td_r[6]_i_1 
       (.I0(s_axis_tdata[22]),
        .I1(sband_tact0),
        .O(\sband_td_r_reg[15] [6]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sband_td_r[7]_i_1 
       (.I0(s_axis_tdata[23]),
        .I1(sband_tact0),
        .O(\sband_td_r_reg[15] [7]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sband_td_r[8]_i_1 
       (.I0(s_axis_tdata[24]),
        .I1(sband_tact0),
        .O(\sband_td_r_reg[15] [8]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sband_td_r[9]_i_1 
       (.I0(s_axis_tdata[25]),
        .I1(sband_tact0),
        .O(\sband_td_r_reg[15] [9]));
  LUT6 #(
    .INIT(64'h0000000001000000)) 
    sband_tl_r_i_1
       (.I0(vfb_valid_reg),
        .I1(vfb_eol),
        .I2(m_axis_tvalid),
        .I3(cur_dtype_udef),
        .I4(\buf_valid_reg_n_0_[0] ),
        .I5(sdt_tv_INST_0_i_1_n_0),
        .O(sband_tact0));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'h08)) 
    sband_tl_r_i_2
       (.I0(s_axis_tlast),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(cur_dtype_sink_reg_1),
        .O(sband_tl));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT4 #(
    .INIT(16'h0040)) 
    \sband_tu_r[0]_i_1 
       (.I0(sdt_tv_INST_0_i_1_n_0),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(Q[2]),
        .I3(cur_dtype_sink_reg_1),
        .O(sband_tu));
  LUT3 #(
    .INIT(8'h08)) 
    sdt_tv_INST_0
       (.I0(cur_dtype_udef),
        .I1(\buf_valid_reg_n_0_[0] ),
        .I2(sdt_tv_INST_0_i_1_n_0),
        .O(sdt_tv));
  LUT6 #(
    .INIT(64'hF7F7F7F7F7F7D7F7)) 
    sdt_tv_INST_0_i_1
       (.I0(Q[8]),
        .I1(Q[7]),
        .I2(Q[6]),
        .I3(Q[4]),
        .I4(Q[5]),
        .I5(Q[3]),
        .O(sdt_tv_INST_0_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFF105510551055)) 
    vfb_eol_i_1
       (.I0(\state_reg[1] ),
        .I1(sband_tk),
        .I2(sband_tl),
        .I3(sband_tact0),
        .I4(m_axis_tlast),
        .I5(m_axis_tvalid),
        .O(vfb_eol_reg));
  LUT1 #(
    .INIT(2'h1)) 
    vfb_valid_i_1
       (.I0(s_axis_aresetn),
        .O(\buf_data_reg[1][134]_0 ));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
