# This file is automatically generated.
# It contains project source information necessary for synthesis and implementation.

# IP: /home/tester2/Videos/ultra96-hw-simplified/ultra96-hw-simplified.srcs/sources_1/bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/bd_0/ip/ip_4/ip_0/bd_d10d_vfb_0_0_axis_converter.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_d10d_vfb_0_0_axis_converter || ORIG_REF_NAME==bd_d10d_vfb_0_0_axis_converter} -quiet] -quiet

# XDC: /home/tester2/Videos/ultra96-hw-simplified/ultra96-hw-simplified.srcs/sources_1/bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/bd_0/ip/ip_4/bd_d10d_vfb_0_0_ooc.xdc
# XDC: The top module name and the constraint reference have the same name: 'bd_d10d_vfb_0_0'. Do not add the DONT_TOUCH constraint.
set_property DONT_TOUCH TRUE [get_cells inst -quiet] -quiet

# IP: /home/tester2/Videos/ultra96-hw-simplified/ultra96-hw-simplified.srcs/sources_1/bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/bd_0/ip/ip_4/ip_0/bd_d10d_vfb_0_0_axis_converter.xci
#dup# set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_d10d_vfb_0_0_axis_converter || ORIG_REF_NAME==bd_d10d_vfb_0_0_axis_converter} -quiet] -quiet

# XDC: /home/tester2/Videos/ultra96-hw-simplified/ultra96-hw-simplified.srcs/sources_1/bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/bd_0/ip/ip_4/bd_d10d_vfb_0_0_ooc.xdc
# XDC: The top module name and the constraint reference have the same name: 'bd_d10d_vfb_0_0'. Do not add the DONT_TOUCH constraint.
#dup# set_property DONT_TOUCH TRUE [get_cells inst -quiet] -quiet
