-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
-- Date        : Sat Feb  1 12:31:55 2020
-- Host        : tester2-ThinkPad-X1-Extreme-2nd running 64-bit Ubuntu 18.04.3 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/tester2/Videos/ultra96-hw-simplified/ultra96-hw-simplified.srcs/sources_1/bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/bd_0/ip/ip_4/bd_d10d_vfb_0_0_sim_netlist.vhdl
-- Design      : bd_d10d_vfb_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xczu3eg-sbva484-1-i
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_d10d_vfb_0_0_axis_dwidth_converter_v1_1_16_axisc_downsizer is
  port (
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axis_tlast : out STD_LOGIC;
    m_axis_tid : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axis_tdata : out STD_LOGIC_VECTOR ( 15 downto 0 );
    m_axis_tkeep : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axis_tuser : out STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axis_tlast : in STD_LOGIC;
    aclk : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_tkeep : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axis_tvalid : in STD_LOGIC;
    m_axis_tready : in STD_LOGIC;
    s_axis_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_tid : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_tuser : in STD_LOGIC_VECTOR ( 11 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of bd_d10d_vfb_0_0_axis_dwidth_converter_v1_1_16_axisc_downsizer : entity is "axis_dwidth_converter_v1_1_16_axisc_downsizer";
end bd_d10d_vfb_0_0_axis_dwidth_converter_v1_1_16_axisc_downsizer;

architecture STRUCTURE of bd_d10d_vfb_0_0_axis_dwidth_converter_v1_1_16_axisc_downsizer is
  signal \^q\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal p_0_in : STD_LOGIC;
  signal r0_data : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal r0_id : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal r0_is_end : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \r0_is_null_r[1]_i_1_n_0\ : STD_LOGIC;
  signal r0_keep : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal r0_last_reg_n_0 : STD_LOGIC;
  signal r0_load : STD_LOGIC;
  signal \r0_out_sel_r[0]_i_1_n_0\ : STD_LOGIC;
  signal \r0_out_sel_r_reg_n_0_[0]\ : STD_LOGIC;
  signal r0_user : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal r1_data : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \r1_data[15]_i_1_n_0\ : STD_LOGIC;
  signal r1_id : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal r1_keep : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal r1_last_reg_n_0 : STD_LOGIC;
  signal r1_user : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal state : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \state_reg_n_0_[2]\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \m_axis_tdata[0]_INST_0\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \m_axis_tdata[10]_INST_0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \m_axis_tdata[11]_INST_0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \m_axis_tdata[12]_INST_0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \m_axis_tdata[13]_INST_0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \m_axis_tdata[14]_INST_0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \m_axis_tdata[15]_INST_0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \m_axis_tdata[1]_INST_0\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \m_axis_tdata[2]_INST_0\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \m_axis_tdata[3]_INST_0\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \m_axis_tdata[4]_INST_0\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \m_axis_tdata[5]_INST_0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \m_axis_tdata[6]_INST_0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \m_axis_tdata[7]_INST_0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \m_axis_tdata[8]_INST_0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \m_axis_tdata[9]_INST_0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \m_axis_tid[0]_INST_0\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \m_axis_tkeep[0]_INST_0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \m_axis_tkeep[1]_INST_0\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \m_axis_tuser[0]_INST_0\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \m_axis_tuser[1]_INST_0\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \m_axis_tuser[2]_INST_0\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \m_axis_tuser[3]_INST_0\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \m_axis_tuser[4]_INST_0\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \m_axis_tuser[5]_INST_0\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \r0_out_sel_r[0]_i_2\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \state[0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \state[2]_i_1\ : label is "soft_lutpair0";
  attribute FSM_ENCODING : string;
  attribute FSM_ENCODING of \state_reg[0]\ : label is "none";
  attribute FSM_ENCODING of \state_reg[1]\ : label is "none";
  attribute FSM_ENCODING of \state_reg[2]\ : label is "none";
begin
  Q(1 downto 0) <= \^q\(1 downto 0);
\m_axis_tdata[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r1_data(0),
      I1 => \r0_out_sel_r_reg_n_0_[0]\,
      I2 => r0_data(0),
      O => m_axis_tdata(0)
    );
\m_axis_tdata[10]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r1_data(10),
      I1 => \r0_out_sel_r_reg_n_0_[0]\,
      I2 => r0_data(10),
      O => m_axis_tdata(10)
    );
\m_axis_tdata[11]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r1_data(11),
      I1 => \r0_out_sel_r_reg_n_0_[0]\,
      I2 => r0_data(11),
      O => m_axis_tdata(11)
    );
\m_axis_tdata[12]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r1_data(12),
      I1 => \r0_out_sel_r_reg_n_0_[0]\,
      I2 => r0_data(12),
      O => m_axis_tdata(12)
    );
\m_axis_tdata[13]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r1_data(13),
      I1 => \r0_out_sel_r_reg_n_0_[0]\,
      I2 => r0_data(13),
      O => m_axis_tdata(13)
    );
\m_axis_tdata[14]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r1_data(14),
      I1 => \r0_out_sel_r_reg_n_0_[0]\,
      I2 => r0_data(14),
      O => m_axis_tdata(14)
    );
\m_axis_tdata[15]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r1_data(15),
      I1 => \r0_out_sel_r_reg_n_0_[0]\,
      I2 => r0_data(15),
      O => m_axis_tdata(15)
    );
\m_axis_tdata[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r1_data(1),
      I1 => \r0_out_sel_r_reg_n_0_[0]\,
      I2 => r0_data(1),
      O => m_axis_tdata(1)
    );
\m_axis_tdata[2]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r1_data(2),
      I1 => \r0_out_sel_r_reg_n_0_[0]\,
      I2 => r0_data(2),
      O => m_axis_tdata(2)
    );
\m_axis_tdata[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r1_data(3),
      I1 => \r0_out_sel_r_reg_n_0_[0]\,
      I2 => r0_data(3),
      O => m_axis_tdata(3)
    );
\m_axis_tdata[4]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r1_data(4),
      I1 => \r0_out_sel_r_reg_n_0_[0]\,
      I2 => r0_data(4),
      O => m_axis_tdata(4)
    );
\m_axis_tdata[5]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r1_data(5),
      I1 => \r0_out_sel_r_reg_n_0_[0]\,
      I2 => r0_data(5),
      O => m_axis_tdata(5)
    );
\m_axis_tdata[6]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r1_data(6),
      I1 => \r0_out_sel_r_reg_n_0_[0]\,
      I2 => r0_data(6),
      O => m_axis_tdata(6)
    );
\m_axis_tdata[7]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r1_data(7),
      I1 => \r0_out_sel_r_reg_n_0_[0]\,
      I2 => r0_data(7),
      O => m_axis_tdata(7)
    );
\m_axis_tdata[8]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r1_data(8),
      I1 => \r0_out_sel_r_reg_n_0_[0]\,
      I2 => r0_data(8),
      O => m_axis_tdata(8)
    );
\m_axis_tdata[9]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r1_data(9),
      I1 => \r0_out_sel_r_reg_n_0_[0]\,
      I2 => r0_data(9),
      O => m_axis_tdata(9)
    );
\m_axis_tid[0]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(0),
      I1 => r0_id(0),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(0)
    );
\m_axis_tid[10]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(10),
      I1 => r0_id(10),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(10)
    );
\m_axis_tid[11]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(11),
      I1 => r0_id(11),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(11)
    );
\m_axis_tid[12]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(12),
      I1 => r0_id(12),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(12)
    );
\m_axis_tid[13]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(13),
      I1 => r0_id(13),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(13)
    );
\m_axis_tid[14]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(14),
      I1 => r0_id(14),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(14)
    );
\m_axis_tid[15]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(15),
      I1 => r0_id(15),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(15)
    );
\m_axis_tid[16]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(16),
      I1 => r0_id(16),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(16)
    );
\m_axis_tid[17]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(17),
      I1 => r0_id(17),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(17)
    );
\m_axis_tid[18]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(18),
      I1 => r0_id(18),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(18)
    );
\m_axis_tid[19]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(19),
      I1 => r0_id(19),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(19)
    );
\m_axis_tid[1]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(1),
      I1 => r0_id(1),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(1)
    );
\m_axis_tid[20]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(20),
      I1 => r0_id(20),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(20)
    );
\m_axis_tid[21]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(21),
      I1 => r0_id(21),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(21)
    );
\m_axis_tid[22]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(22),
      I1 => r0_id(22),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(22)
    );
\m_axis_tid[23]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(23),
      I1 => r0_id(23),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(23)
    );
\m_axis_tid[24]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(24),
      I1 => r0_id(24),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(24)
    );
\m_axis_tid[25]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(25),
      I1 => r0_id(25),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(25)
    );
\m_axis_tid[26]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(26),
      I1 => r0_id(26),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(26)
    );
\m_axis_tid[27]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(27),
      I1 => r0_id(27),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(27)
    );
\m_axis_tid[28]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(28),
      I1 => r0_id(28),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(28)
    );
\m_axis_tid[29]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(29),
      I1 => r0_id(29),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(29)
    );
\m_axis_tid[2]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(2),
      I1 => r0_id(2),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(2)
    );
\m_axis_tid[30]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(30),
      I1 => r0_id(30),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(30)
    );
\m_axis_tid[31]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(31),
      I1 => r0_id(31),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(31)
    );
\m_axis_tid[3]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(3),
      I1 => r0_id(3),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(3)
    );
\m_axis_tid[4]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(4),
      I1 => r0_id(4),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(4)
    );
\m_axis_tid[5]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(5),
      I1 => r0_id(5),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(5)
    );
\m_axis_tid[6]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(6),
      I1 => r0_id(6),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(6)
    );
\m_axis_tid[7]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(7),
      I1 => r0_id(7),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(7)
    );
\m_axis_tid[8]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(8),
      I1 => r0_id(8),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(8)
    );
\m_axis_tid[9]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCACACCC"
    )
        port map (
      I0 => r1_id(9),
      I1 => r0_id(9),
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => m_axis_tid(9)
    );
\m_axis_tkeep[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r1_keep(0),
      I1 => \r0_out_sel_r_reg_n_0_[0]\,
      I2 => r0_keep(0),
      O => m_axis_tkeep(0)
    );
\m_axis_tkeep[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r1_keep(1),
      I1 => \r0_out_sel_r_reg_n_0_[0]\,
      I2 => r0_keep(1),
      O => m_axis_tkeep(1)
    );
m_axis_tlast_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FBBF088008800880"
    )
        port map (
      I0 => r1_last_reg_n_0,
      I1 => \^q\(1),
      I2 => \state_reg_n_0_[2]\,
      I3 => \^q\(0),
      I4 => r0_last_reg_n_0,
      I5 => r0_is_end(0),
      O => m_axis_tlast
    );
\m_axis_tuser[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r1_user(0),
      I1 => \r0_out_sel_r_reg_n_0_[0]\,
      I2 => r0_user(0),
      O => m_axis_tuser(0)
    );
\m_axis_tuser[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r1_user(1),
      I1 => \r0_out_sel_r_reg_n_0_[0]\,
      I2 => r0_user(1),
      O => m_axis_tuser(1)
    );
\m_axis_tuser[2]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r1_user(2),
      I1 => \r0_out_sel_r_reg_n_0_[0]\,
      I2 => r0_user(2),
      O => m_axis_tuser(2)
    );
\m_axis_tuser[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r1_user(3),
      I1 => \r0_out_sel_r_reg_n_0_[0]\,
      I2 => r0_user(3),
      O => m_axis_tuser(3)
    );
\m_axis_tuser[4]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r1_user(4),
      I1 => \r0_out_sel_r_reg_n_0_[0]\,
      I2 => r0_user(4),
      O => m_axis_tuser(4)
    );
\m_axis_tuser[5]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r1_user(5),
      I1 => \r0_out_sel_r_reg_n_0_[0]\,
      I2 => r0_user(5),
      O => m_axis_tuser(5)
    );
\r0_data[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \state_reg_n_0_[2]\,
      I1 => \^q\(0),
      O => r0_load
    );
\r0_data_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(0),
      Q => r0_data(0),
      R => '0'
    );
\r0_data_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(10),
      Q => r0_data(10),
      R => '0'
    );
\r0_data_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(11),
      Q => r0_data(11),
      R => '0'
    );
\r0_data_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(12),
      Q => r0_data(12),
      R => '0'
    );
\r0_data_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(13),
      Q => r0_data(13),
      R => '0'
    );
\r0_data_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(14),
      Q => r0_data(14),
      R => '0'
    );
\r0_data_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(15),
      Q => r0_data(15),
      R => '0'
    );
\r0_data_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(16),
      Q => r0_data(16),
      R => '0'
    );
\r0_data_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(17),
      Q => r0_data(17),
      R => '0'
    );
\r0_data_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(18),
      Q => r0_data(18),
      R => '0'
    );
\r0_data_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(19),
      Q => r0_data(19),
      R => '0'
    );
\r0_data_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(1),
      Q => r0_data(1),
      R => '0'
    );
\r0_data_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(20),
      Q => r0_data(20),
      R => '0'
    );
\r0_data_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(21),
      Q => r0_data(21),
      R => '0'
    );
\r0_data_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(22),
      Q => r0_data(22),
      R => '0'
    );
\r0_data_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(23),
      Q => r0_data(23),
      R => '0'
    );
\r0_data_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(24),
      Q => r0_data(24),
      R => '0'
    );
\r0_data_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(25),
      Q => r0_data(25),
      R => '0'
    );
\r0_data_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(26),
      Q => r0_data(26),
      R => '0'
    );
\r0_data_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(27),
      Q => r0_data(27),
      R => '0'
    );
\r0_data_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(28),
      Q => r0_data(28),
      R => '0'
    );
\r0_data_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(29),
      Q => r0_data(29),
      R => '0'
    );
\r0_data_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(2),
      Q => r0_data(2),
      R => '0'
    );
\r0_data_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(30),
      Q => r0_data(30),
      R => '0'
    );
\r0_data_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(31),
      Q => r0_data(31),
      R => '0'
    );
\r0_data_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(3),
      Q => r0_data(3),
      R => '0'
    );
\r0_data_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(4),
      Q => r0_data(4),
      R => '0'
    );
\r0_data_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(5),
      Q => r0_data(5),
      R => '0'
    );
\r0_data_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(6),
      Q => r0_data(6),
      R => '0'
    );
\r0_data_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(7),
      Q => r0_data(7),
      R => '0'
    );
\r0_data_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(8),
      Q => r0_data(8),
      R => '0'
    );
\r0_data_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tdata(9),
      Q => r0_data(9),
      R => '0'
    );
\r0_id_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(0),
      Q => r0_id(0),
      R => '0'
    );
\r0_id_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(10),
      Q => r0_id(10),
      R => '0'
    );
\r0_id_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(11),
      Q => r0_id(11),
      R => '0'
    );
\r0_id_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(12),
      Q => r0_id(12),
      R => '0'
    );
\r0_id_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(13),
      Q => r0_id(13),
      R => '0'
    );
\r0_id_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(14),
      Q => r0_id(14),
      R => '0'
    );
\r0_id_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(15),
      Q => r0_id(15),
      R => '0'
    );
\r0_id_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(16),
      Q => r0_id(16),
      R => '0'
    );
\r0_id_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(17),
      Q => r0_id(17),
      R => '0'
    );
\r0_id_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(18),
      Q => r0_id(18),
      R => '0'
    );
\r0_id_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(19),
      Q => r0_id(19),
      R => '0'
    );
\r0_id_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(1),
      Q => r0_id(1),
      R => '0'
    );
\r0_id_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(20),
      Q => r0_id(20),
      R => '0'
    );
\r0_id_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(21),
      Q => r0_id(21),
      R => '0'
    );
\r0_id_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(22),
      Q => r0_id(22),
      R => '0'
    );
\r0_id_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(23),
      Q => r0_id(23),
      R => '0'
    );
\r0_id_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(24),
      Q => r0_id(24),
      R => '0'
    );
\r0_id_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(25),
      Q => r0_id(25),
      R => '0'
    );
\r0_id_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(26),
      Q => r0_id(26),
      R => '0'
    );
\r0_id_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(27),
      Q => r0_id(27),
      R => '0'
    );
\r0_id_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(28),
      Q => r0_id(28),
      R => '0'
    );
\r0_id_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(29),
      Q => r0_id(29),
      R => '0'
    );
\r0_id_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(2),
      Q => r0_id(2),
      R => '0'
    );
\r0_id_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(30),
      Q => r0_id(30),
      R => '0'
    );
\r0_id_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(31),
      Q => r0_id(31),
      R => '0'
    );
\r0_id_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(3),
      Q => r0_id(3),
      R => '0'
    );
\r0_id_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(4),
      Q => r0_id(4),
      R => '0'
    );
\r0_id_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(5),
      Q => r0_id(5),
      R => '0'
    );
\r0_id_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(6),
      Q => r0_id(6),
      R => '0'
    );
\r0_id_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(7),
      Q => r0_id(7),
      R => '0'
    );
\r0_id_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(8),
      Q => r0_id(8),
      R => '0'
    );
\r0_id_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tid(9),
      Q => r0_id(9),
      R => '0'
    );
\r0_is_null_r[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBFFFFF00040000"
    )
        port map (
      I0 => \state_reg_n_0_[2]\,
      I1 => \^q\(0),
      I2 => s_axis_tkeep(2),
      I3 => s_axis_tkeep(3),
      I4 => s_axis_tvalid,
      I5 => r0_is_end(0),
      O => \r0_is_null_r[1]_i_1_n_0\
    );
\r0_is_null_r_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => aclk,
      CE => '1',
      D => \r0_is_null_r[1]_i_1_n_0\,
      Q => r0_is_end(0),
      R => SR(0)
    );
\r0_keep_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tkeep(0),
      Q => r0_keep(0),
      R => '0'
    );
\r0_keep_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tkeep(1),
      Q => r0_keep(1),
      R => '0'
    );
\r0_keep_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tkeep(2),
      Q => r0_keep(2),
      R => '0'
    );
\r0_keep_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tkeep(3),
      Q => r0_keep(3),
      R => '0'
    );
r0_last_reg: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tlast,
      Q => r0_last_reg_n_0,
      R => '0'
    );
\r0_out_sel_r[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000046"
    )
        port map (
      I0 => m_axis_tready,
      I1 => \r0_out_sel_r_reg_n_0_[0]\,
      I2 => r0_is_end(0),
      I3 => p_0_in,
      I4 => SR(0),
      O => \r0_out_sel_r[0]_i_1_n_0\
    );
\r0_out_sel_r[0]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \^q\(0),
      I1 => \state_reg_n_0_[2]\,
      I2 => \^q\(1),
      O => p_0_in
    );
\r0_out_sel_r_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => aclk,
      CE => '1',
      D => \r0_out_sel_r[0]_i_1_n_0\,
      Q => \r0_out_sel_r_reg_n_0_[0]\,
      R => '0'
    );
\r0_user_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tuser(0),
      Q => r0_user(0),
      R => '0'
    );
\r0_user_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tuser(10),
      Q => r0_user(10),
      R => '0'
    );
\r0_user_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tuser(11),
      Q => r0_user(11),
      R => '0'
    );
\r0_user_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tuser(1),
      Q => r0_user(1),
      R => '0'
    );
\r0_user_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tuser(2),
      Q => r0_user(2),
      R => '0'
    );
\r0_user_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tuser(3),
      Q => r0_user(3),
      R => '0'
    );
\r0_user_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tuser(4),
      Q => r0_user(4),
      R => '0'
    );
\r0_user_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tuser(5),
      Q => r0_user(5),
      R => '0'
    );
\r0_user_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tuser(6),
      Q => r0_user(6),
      R => '0'
    );
\r0_user_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tuser(7),
      Q => r0_user(7),
      R => '0'
    );
\r0_user_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tuser(8),
      Q => r0_user(8),
      R => '0'
    );
\r0_user_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => r0_load,
      D => s_axis_tuser(9),
      Q => r0_user(9),
      R => '0'
    );
\r1_data[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \state_reg_n_0_[2]\,
      I1 => \^q\(1),
      I2 => \^q\(0),
      O => \r1_data[15]_i_1_n_0\
    );
\r1_data_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_data(16),
      Q => r1_data(0),
      R => '0'
    );
\r1_data_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_data(26),
      Q => r1_data(10),
      R => '0'
    );
\r1_data_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_data(27),
      Q => r1_data(11),
      R => '0'
    );
\r1_data_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_data(28),
      Q => r1_data(12),
      R => '0'
    );
\r1_data_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_data(29),
      Q => r1_data(13),
      R => '0'
    );
\r1_data_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_data(30),
      Q => r1_data(14),
      R => '0'
    );
\r1_data_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_data(31),
      Q => r1_data(15),
      R => '0'
    );
\r1_data_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_data(17),
      Q => r1_data(1),
      R => '0'
    );
\r1_data_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_data(18),
      Q => r1_data(2),
      R => '0'
    );
\r1_data_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_data(19),
      Q => r1_data(3),
      R => '0'
    );
\r1_data_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_data(20),
      Q => r1_data(4),
      R => '0'
    );
\r1_data_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_data(21),
      Q => r1_data(5),
      R => '0'
    );
\r1_data_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_data(22),
      Q => r1_data(6),
      R => '0'
    );
\r1_data_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_data(23),
      Q => r1_data(7),
      R => '0'
    );
\r1_data_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_data(24),
      Q => r1_data(8),
      R => '0'
    );
\r1_data_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_data(25),
      Q => r1_data(9),
      R => '0'
    );
\r1_id_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(0),
      Q => r1_id(0),
      R => '0'
    );
\r1_id_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(10),
      Q => r1_id(10),
      R => '0'
    );
\r1_id_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(11),
      Q => r1_id(11),
      R => '0'
    );
\r1_id_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(12),
      Q => r1_id(12),
      R => '0'
    );
\r1_id_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(13),
      Q => r1_id(13),
      R => '0'
    );
\r1_id_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(14),
      Q => r1_id(14),
      R => '0'
    );
\r1_id_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(15),
      Q => r1_id(15),
      R => '0'
    );
\r1_id_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(16),
      Q => r1_id(16),
      R => '0'
    );
\r1_id_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(17),
      Q => r1_id(17),
      R => '0'
    );
\r1_id_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(18),
      Q => r1_id(18),
      R => '0'
    );
\r1_id_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(19),
      Q => r1_id(19),
      R => '0'
    );
\r1_id_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(1),
      Q => r1_id(1),
      R => '0'
    );
\r1_id_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(20),
      Q => r1_id(20),
      R => '0'
    );
\r1_id_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(21),
      Q => r1_id(21),
      R => '0'
    );
\r1_id_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(22),
      Q => r1_id(22),
      R => '0'
    );
\r1_id_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(23),
      Q => r1_id(23),
      R => '0'
    );
\r1_id_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(24),
      Q => r1_id(24),
      R => '0'
    );
\r1_id_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(25),
      Q => r1_id(25),
      R => '0'
    );
\r1_id_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(26),
      Q => r1_id(26),
      R => '0'
    );
\r1_id_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(27),
      Q => r1_id(27),
      R => '0'
    );
\r1_id_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(28),
      Q => r1_id(28),
      R => '0'
    );
\r1_id_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(29),
      Q => r1_id(29),
      R => '0'
    );
\r1_id_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(2),
      Q => r1_id(2),
      R => '0'
    );
\r1_id_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(30),
      Q => r1_id(30),
      R => '0'
    );
\r1_id_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(31),
      Q => r1_id(31),
      R => '0'
    );
\r1_id_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(3),
      Q => r1_id(3),
      R => '0'
    );
\r1_id_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(4),
      Q => r1_id(4),
      R => '0'
    );
\r1_id_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(5),
      Q => r1_id(5),
      R => '0'
    );
\r1_id_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(6),
      Q => r1_id(6),
      R => '0'
    );
\r1_id_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(7),
      Q => r1_id(7),
      R => '0'
    );
\r1_id_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(8),
      Q => r1_id(8),
      R => '0'
    );
\r1_id_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_id(9),
      Q => r1_id(9),
      R => '0'
    );
\r1_keep_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_keep(2),
      Q => r1_keep(0),
      R => '0'
    );
\r1_keep_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_keep(3),
      Q => r1_keep(1),
      R => '0'
    );
r1_last_reg: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_last_reg_n_0,
      Q => r1_last_reg_n_0,
      R => '0'
    );
\r1_user_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_user(6),
      Q => r1_user(0),
      R => '0'
    );
\r1_user_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_user(7),
      Q => r1_user(1),
      R => '0'
    );
\r1_user_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_user(8),
      Q => r1_user(2),
      R => '0'
    );
\r1_user_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_user(9),
      Q => r1_user(3),
      R => '0'
    );
\r1_user_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_user(10),
      Q => r1_user(4),
      R => '0'
    );
\r1_user_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => aclk,
      CE => \r1_data[15]_i_1_n_0\,
      D => r0_user(11),
      Q => r1_user(5),
      R => '0'
    );
\state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF550FCF"
    )
        port map (
      I0 => s_axis_tvalid,
      I1 => m_axis_tready,
      I2 => \^q\(1),
      I3 => \state_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => state(0)
    );
\state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000AFAAFF003F00"
    )
        port map (
      I0 => s_axis_tvalid,
      I1 => r0_is_end(0),
      I2 => m_axis_tready,
      I3 => \^q\(1),
      I4 => \state_reg_n_0_[2]\,
      I5 => \^q\(0),
      O => state(1)
    );
\state[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000008C0"
    )
        port map (
      I0 => s_axis_tvalid,
      I1 => \^q\(1),
      I2 => \state_reg_n_0_[2]\,
      I3 => \^q\(0),
      I4 => m_axis_tready,
      O => state(2)
    );
\state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => aclk,
      CE => '1',
      D => state(0),
      Q => \^q\(0),
      R => SR(0)
    );
\state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => aclk,
      CE => '1',
      D => state(1),
      Q => \^q\(1),
      R => SR(0)
    );
\state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => aclk,
      CE => '1',
      D => state(2),
      Q => \state_reg_n_0_[2]\,
      R => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_d10d_vfb_0_0_vfb_v1_0_11_op_inf is
  port (
    vfb_eol : out STD_LOGIC;
    mdt_tr : out STD_LOGIC;
    vfb_valid : out STD_LOGIC;
    sband_tu_r : out STD_LOGIC;
    vfb_sof : out STD_LOGIC_VECTOR ( 0 to 0 );
    sdt_tr : out STD_LOGIC;
    vfb_eol_reg_0 : out STD_LOGIC;
    vfb_eol_reg_1 : out STD_LOGIC;
    \vfb_sof_reg[0]_0\ : out STD_LOGIC;
    \buf_valid_reg[0]\ : out STD_LOGIC;
    sband_tl_r_reg_0 : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 7 downto 0 );
    vfb_vcdt : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \vfb_data_reg[15]_0\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    vfb_data : out STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axis_aresetn_0 : in STD_LOGIC;
    sband_tact0 : in STD_LOGIC;
    sband_tl : in STD_LOGIC;
    vfb_clk : in STD_LOGIC;
    \buf_data_reg[0][100]\ : in STD_LOGIC;
    sband_tu : in STD_LOGIC;
    m_axis_tvalid : in STD_LOGIC;
    vfb_ready : in STD_LOGIC;
    cur_dtype_sink_reg : in STD_LOGIC;
    cur_dtype_udef : in STD_LOGIC;
    \state_reg[1]\ : in STD_LOGIC;
    s_axis_aresetn : in STD_LOGIC;
    \buf_data_reg[0][71]\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    D : in STD_LOGIC_VECTOR ( 7 downto 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    \buf_data_reg[0][133]\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    \state_reg[1]_0\ : in STD_LOGIC;
    \state_reg[1]_1\ : in STD_LOGIC;
    \state_reg[1]_2\ : in STD_LOGIC;
    \state_reg[1]_3\ : in STD_LOGIC;
    \state_reg[1]_4\ : in STD_LOGIC;
    \state_reg[1]_5\ : in STD_LOGIC;
    \state_reg[1]_6\ : in STD_LOGIC;
    \state_reg[1]_7\ : in STD_LOGIC;
    \state_reg[1]_8\ : in STD_LOGIC;
    \state_reg[1]_9\ : in STD_LOGIC;
    \state_reg[1]_10\ : in STD_LOGIC;
    \state_reg[1]_11\ : in STD_LOGIC;
    \state_reg[1]_12\ : in STD_LOGIC;
    \state_reg[1]_13\ : in STD_LOGIC;
    \state_reg[1]_14\ : in STD_LOGIC;
    \state_reg[1]_15\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of bd_d10d_vfb_0_0_vfb_v1_0_11_op_inf : entity is "vfb_v1_0_11_op_inf";
end bd_d10d_vfb_0_0_vfb_v1_0_11_op_inf;

architecture STRUCTURE of bd_d10d_vfb_0_0_vfb_v1_0_11_op_inf is
  signal cnt_done0 : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \^mdt_tr\ : STD_LOGIC;
  signal sband_tact : STD_LOGIC;
  signal sband_tact_i_1_n_0 : STD_LOGIC;
  signal sband_tact_i_2_n_0 : STD_LOGIC;
  signal sband_tl_r : STD_LOGIC;
  signal sband_tr2 : STD_LOGIC;
  signal sdt_tr_INST_0_i_1_n_0 : STD_LOGIC;
  signal \vfb_cnt[3]_i_1_n_0\ : STD_LOGIC;
  signal \vfb_cnt[3]_i_4_n_0\ : STD_LOGIC;
  signal \vfb_cnt_reg__0\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \vfb_data[15]_i_1_n_0\ : STD_LOGIC;
  signal \^vfb_eol\ : STD_LOGIC;
  signal \^vfb_eol_reg_1\ : STD_LOGIC;
  signal \^vfb_sof\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \vfb_sof[0]_i_1_n_0\ : STD_LOGIC;
  signal \vfb_sof[0]_i_3_n_0\ : STD_LOGIC;
  signal \^vfb_sof_reg[0]_0\ : STD_LOGIC;
  signal \^vfb_valid\ : STD_LOGIC;
  signal vfb_valid_i_2_n_0 : STD_LOGIC;
  signal \vfb_vcdt[7]_i_1_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of sband_tl_r_i_3 : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of sdt_tr_INST_0 : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of sdt_tr_INST_0_i_1 : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \vfb_cnt[1]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \vfb_cnt[2]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \vfb_cnt[3]_i_3\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \vfb_cnt[3]_i_4\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of vfb_eol_i_3 : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \vfb_sof[0]_i_3\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of vfb_valid_i_3 : label is "soft_lutpair15";
begin
  mdt_tr <= \^mdt_tr\;
  vfb_eol <= \^vfb_eol\;
  vfb_eol_reg_1 <= \^vfb_eol_reg_1\;
  vfb_sof(0) <= \^vfb_sof\(0);
  \vfb_sof_reg[0]_0\ <= \^vfb_sof_reg[0]_0\;
  vfb_valid <= \^vfb_valid\;
\buf_valid[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5555551055555555"
    )
        port map (
      I0 => cur_dtype_sink_reg,
      I1 => sdt_tr_INST_0_i_1_n_0,
      I2 => \^vfb_valid\,
      I3 => \^vfb_eol\,
      I4 => m_axis_tvalid,
      I5 => cur_dtype_udef,
      O => \buf_valid_reg[0]\
    );
mdt_tr_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => vfb_ready,
      I1 => \^vfb_valid\,
      O => \^mdt_tr\
    );
sband_tact_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => sband_tact0,
      I1 => sband_tact_i_2_n_0,
      I2 => sband_tact,
      O => sband_tact_i_1_n_0
    );
sband_tact_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00000004000000"
    )
        port map (
      I0 => \vfb_cnt_reg__0\(2),
      I1 => \vfb_cnt_reg__0\(1),
      I2 => \vfb_cnt_reg__0\(3),
      I3 => \^vfb_valid\,
      I4 => vfb_ready,
      I5 => \^vfb_eol\,
      O => sband_tact_i_2_n_0
    );
sband_tact_reg: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => '1',
      D => sband_tact_i_1_n_0,
      Q => sband_tact,
      R => s_axis_aresetn_0
    );
\sband_td_r_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => E(0),
      D => \buf_data_reg[0][133]\(0),
      Q => \vfb_data_reg[15]_0\(0),
      R => s_axis_aresetn_0
    );
\sband_td_r_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => E(0),
      D => \buf_data_reg[0][133]\(10),
      Q => \vfb_data_reg[15]_0\(10),
      R => s_axis_aresetn_0
    );
\sband_td_r_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => E(0),
      D => \buf_data_reg[0][133]\(11),
      Q => \vfb_data_reg[15]_0\(11),
      R => s_axis_aresetn_0
    );
\sband_td_r_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => E(0),
      D => \buf_data_reg[0][133]\(12),
      Q => \vfb_data_reg[15]_0\(12),
      R => s_axis_aresetn_0
    );
\sband_td_r_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => E(0),
      D => \buf_data_reg[0][133]\(13),
      Q => \vfb_data_reg[15]_0\(13),
      R => s_axis_aresetn_0
    );
\sband_td_r_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => E(0),
      D => \buf_data_reg[0][133]\(14),
      Q => \vfb_data_reg[15]_0\(14),
      R => s_axis_aresetn_0
    );
\sband_td_r_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => E(0),
      D => \buf_data_reg[0][133]\(15),
      Q => \vfb_data_reg[15]_0\(15),
      R => s_axis_aresetn_0
    );
\sband_td_r_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => E(0),
      D => \buf_data_reg[0][133]\(1),
      Q => \vfb_data_reg[15]_0\(1),
      R => s_axis_aresetn_0
    );
\sband_td_r_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => E(0),
      D => \buf_data_reg[0][133]\(2),
      Q => \vfb_data_reg[15]_0\(2),
      R => s_axis_aresetn_0
    );
\sband_td_r_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => E(0),
      D => \buf_data_reg[0][133]\(3),
      Q => \vfb_data_reg[15]_0\(3),
      R => s_axis_aresetn_0
    );
\sband_td_r_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => E(0),
      D => \buf_data_reg[0][133]\(4),
      Q => \vfb_data_reg[15]_0\(4),
      R => s_axis_aresetn_0
    );
\sband_td_r_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => E(0),
      D => \buf_data_reg[0][133]\(5),
      Q => \vfb_data_reg[15]_0\(5),
      R => s_axis_aresetn_0
    );
\sband_td_r_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => E(0),
      D => \buf_data_reg[0][133]\(6),
      Q => \vfb_data_reg[15]_0\(6),
      R => s_axis_aresetn_0
    );
\sband_td_r_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => E(0),
      D => \buf_data_reg[0][133]\(7),
      Q => \vfb_data_reg[15]_0\(7),
      R => s_axis_aresetn_0
    );
\sband_td_r_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => E(0),
      D => \buf_data_reg[0][133]\(8),
      Q => \vfb_data_reg[15]_0\(8),
      R => s_axis_aresetn_0
    );
\sband_td_r_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => E(0),
      D => \buf_data_reg[0][133]\(9),
      Q => \vfb_data_reg[15]_0\(9),
      R => s_axis_aresetn_0
    );
sband_tl_r_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAA2AA"
    )
        port map (
      I0 => \^vfb_valid\,
      I1 => vfb_ready,
      I2 => \vfb_cnt_reg__0\(2),
      I3 => \vfb_cnt_reg__0\(1),
      I4 => \vfb_cnt_reg__0\(3),
      O => sband_tl_r_reg_0
    );
sband_tl_r_reg: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => sband_tact0,
      D => sband_tl,
      Q => sband_tl_r,
      R => s_axis_aresetn_0
    );
\sband_ts_r_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => sband_tact0,
      D => \buf_data_reg[0][71]\(0),
      Q => Q(0),
      R => s_axis_aresetn_0
    );
\sband_ts_r_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => sband_tact0,
      D => \buf_data_reg[0][71]\(1),
      Q => Q(1),
      R => s_axis_aresetn_0
    );
\sband_ts_r_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => sband_tact0,
      D => \buf_data_reg[0][71]\(2),
      Q => Q(2),
      R => s_axis_aresetn_0
    );
\sband_ts_r_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => sband_tact0,
      D => \buf_data_reg[0][71]\(3),
      Q => Q(3),
      R => s_axis_aresetn_0
    );
\sband_ts_r_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => sband_tact0,
      D => \buf_data_reg[0][71]\(4),
      Q => Q(4),
      R => s_axis_aresetn_0
    );
\sband_ts_r_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => sband_tact0,
      D => \buf_data_reg[0][71]\(5),
      Q => Q(5),
      R => s_axis_aresetn_0
    );
\sband_ts_r_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => sband_tact0,
      D => \buf_data_reg[0][71]\(6),
      Q => Q(6),
      R => s_axis_aresetn_0
    );
\sband_ts_r_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => sband_tact0,
      D => \buf_data_reg[0][71]\(7),
      Q => Q(7),
      R => s_axis_aresetn_0
    );
\sband_tu_r_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => sband_tact0,
      D => sband_tu,
      Q => sband_tu_r,
      R => s_axis_aresetn_0
    );
sdt_tr_INST_0: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1101"
    )
        port map (
      I0 => m_axis_tvalid,
      I1 => \^vfb_eol\,
      I2 => \^vfb_valid\,
      I3 => sdt_tr_INST_0_i_1_n_0,
      O => sdt_tr
    );
sdt_tr_INST_0_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04000000"
    )
        port map (
      I0 => \vfb_cnt_reg__0\(3),
      I1 => \vfb_cnt_reg__0\(1),
      I2 => \vfb_cnt_reg__0\(2),
      I3 => vfb_ready,
      I4 => \^vfb_valid\,
      O => sdt_tr_INST_0_i_1_n_0
    );
\vfb_cnt[1]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vfb_cnt_reg__0\(1),
      O => cnt_done0(1)
    );
\vfb_cnt[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \vfb_cnt_reg__0\(2),
      I1 => \vfb_cnt_reg__0\(1),
      O => cnt_done0(2)
    );
\vfb_cnt[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"22222322FFFFFFFF"
    )
        port map (
      I0 => \^vfb_eol\,
      I1 => \vfb_cnt[3]_i_4_n_0\,
      I2 => \vfb_cnt_reg__0\(3),
      I3 => \vfb_cnt_reg__0\(1),
      I4 => \vfb_cnt_reg__0\(2),
      I5 => s_axis_aresetn,
      O => \vfb_cnt[3]_i_1_n_0\
    );
\vfb_cnt[3]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => vfb_ready,
      I1 => \^vfb_valid\,
      O => sband_tr2
    );
\vfb_cnt[3]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \vfb_cnt_reg__0\(3),
      I1 => \vfb_cnt_reg__0\(1),
      I2 => \vfb_cnt_reg__0\(2),
      O => cnt_done0(3)
    );
\vfb_cnt[3]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \^vfb_valid\,
      I1 => vfb_ready,
      O => \vfb_cnt[3]_i_4_n_0\
    );
\vfb_cnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => sband_tr2,
      D => cnt_done0(1),
      Q => \vfb_cnt_reg__0\(1),
      R => \vfb_cnt[3]_i_1_n_0\
    );
\vfb_cnt_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => sband_tr2,
      D => cnt_done0(2),
      Q => \vfb_cnt_reg__0\(2),
      R => \vfb_cnt[3]_i_1_n_0\
    );
\vfb_cnt_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => sband_tr2,
      D => cnt_done0(3),
      Q => \vfb_cnt_reg__0\(3),
      R => \vfb_cnt[3]_i_1_n_0\
    );
\vfb_data[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DD5D"
    )
        port map (
      I0 => \^vfb_valid\,
      I1 => vfb_ready,
      I2 => \^vfb_eol\,
      I3 => m_axis_tvalid,
      O => \vfb_data[15]_i_1_n_0\
    );
\vfb_data_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => \vfb_data[15]_i_1_n_0\,
      D => \state_reg[1]_15\,
      Q => vfb_data(0),
      R => s_axis_aresetn_0
    );
\vfb_data_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => \vfb_data[15]_i_1_n_0\,
      D => \state_reg[1]_5\,
      Q => vfb_data(10),
      R => s_axis_aresetn_0
    );
\vfb_data_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => \vfb_data[15]_i_1_n_0\,
      D => \state_reg[1]_4\,
      Q => vfb_data(11),
      R => s_axis_aresetn_0
    );
\vfb_data_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => \vfb_data[15]_i_1_n_0\,
      D => \state_reg[1]_3\,
      Q => vfb_data(12),
      R => s_axis_aresetn_0
    );
\vfb_data_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => \vfb_data[15]_i_1_n_0\,
      D => \state_reg[1]_2\,
      Q => vfb_data(13),
      R => s_axis_aresetn_0
    );
\vfb_data_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => \vfb_data[15]_i_1_n_0\,
      D => \state_reg[1]_1\,
      Q => vfb_data(14),
      R => s_axis_aresetn_0
    );
\vfb_data_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => \vfb_data[15]_i_1_n_0\,
      D => \state_reg[1]_0\,
      Q => vfb_data(15),
      R => s_axis_aresetn_0
    );
\vfb_data_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => \vfb_data[15]_i_1_n_0\,
      D => \state_reg[1]_14\,
      Q => vfb_data(1),
      R => s_axis_aresetn_0
    );
\vfb_data_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => \vfb_data[15]_i_1_n_0\,
      D => \state_reg[1]_13\,
      Q => vfb_data(2),
      R => s_axis_aresetn_0
    );
\vfb_data_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => \vfb_data[15]_i_1_n_0\,
      D => \state_reg[1]_12\,
      Q => vfb_data(3),
      R => s_axis_aresetn_0
    );
\vfb_data_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => \vfb_data[15]_i_1_n_0\,
      D => \state_reg[1]_11\,
      Q => vfb_data(4),
      R => s_axis_aresetn_0
    );
\vfb_data_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => \vfb_data[15]_i_1_n_0\,
      D => \state_reg[1]_10\,
      Q => vfb_data(5),
      R => s_axis_aresetn_0
    );
\vfb_data_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => \vfb_data[15]_i_1_n_0\,
      D => \state_reg[1]_9\,
      Q => vfb_data(6),
      R => s_axis_aresetn_0
    );
\vfb_data_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => \vfb_data[15]_i_1_n_0\,
      D => \state_reg[1]_8\,
      Q => vfb_data(7),
      R => s_axis_aresetn_0
    );
\vfb_data_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => \vfb_data[15]_i_1_n_0\,
      D => \state_reg[1]_7\,
      Q => vfb_data(8),
      R => s_axis_aresetn_0
    );
\vfb_data_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => \vfb_data[15]_i_1_n_0\,
      D => \state_reg[1]_6\,
      Q => vfb_data(9),
      R => s_axis_aresetn_0
    );
vfb_eol_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEEEEFFF"
    )
        port map (
      I0 => \^vfb_eol_reg_1\,
      I1 => m_axis_tvalid,
      I2 => sband_tl_r,
      I3 => \^vfb_sof_reg[0]_0\,
      I4 => sband_tact0,
      O => vfb_eol_reg_0
    );
vfb_eol_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \^vfb_eol\,
      I1 => vfb_ready,
      I2 => \^vfb_valid\,
      O => \^vfb_eol_reg_1\
    );
vfb_eol_reg: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => \^mdt_tr\,
      D => \buf_data_reg[0][100]\,
      Q => \^vfb_eol\,
      R => s_axis_aresetn_0
    );
\vfb_sof[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBFBBBB88808888"
    )
        port map (
      I0 => \state_reg[1]\,
      I1 => \^mdt_tr\,
      I2 => sband_tact0,
      I3 => \^vfb_sof_reg[0]_0\,
      I4 => \vfb_sof[0]_i_3_n_0\,
      I5 => \^vfb_sof\(0),
      O => \vfb_sof[0]_i_1_n_0\
    );
\vfb_sof[0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1555"
    )
        port map (
      I0 => m_axis_tvalid,
      I1 => \^vfb_valid\,
      I2 => vfb_ready,
      I3 => \^vfb_eol\,
      O => \vfb_sof[0]_i_3_n_0\
    );
\vfb_sof_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => '1',
      D => \vfb_sof[0]_i_1_n_0\,
      Q => \^vfb_sof\(0),
      R => s_axis_aresetn_0
    );
vfb_valid_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFBFFFBFFFAAAA"
    )
        port map (
      I0 => m_axis_tvalid,
      I1 => \^vfb_eol\,
      I2 => vfb_ready,
      I3 => \^vfb_valid\,
      I4 => sband_tact0,
      I5 => \^vfb_sof_reg[0]_0\,
      O => vfb_valid_i_2_n_0
    );
vfb_valid_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AA8A"
    )
        port map (
      I0 => sband_tact,
      I1 => \vfb_cnt_reg__0\(3),
      I2 => \vfb_cnt_reg__0\(1),
      I3 => \vfb_cnt_reg__0\(2),
      O => \^vfb_sof_reg[0]_0\
    );
vfb_valid_reg: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => \^mdt_tr\,
      D => vfb_valid_i_2_n_0,
      Q => \^vfb_valid\,
      R => s_axis_aresetn_0
    );
\vfb_vcdt[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DDDDDDD0"
    )
        port map (
      I0 => \^vfb_valid\,
      I1 => vfb_ready,
      I2 => sband_tact0,
      I3 => sband_tact,
      I4 => m_axis_tvalid,
      O => \vfb_vcdt[7]_i_1_n_0\
    );
\vfb_vcdt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => \vfb_vcdt[7]_i_1_n_0\,
      D => D(0),
      Q => vfb_vcdt(0),
      R => s_axis_aresetn_0
    );
\vfb_vcdt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => \vfb_vcdt[7]_i_1_n_0\,
      D => D(1),
      Q => vfb_vcdt(1),
      R => s_axis_aresetn_0
    );
\vfb_vcdt_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => \vfb_vcdt[7]_i_1_n_0\,
      D => D(2),
      Q => vfb_vcdt(2),
      R => s_axis_aresetn_0
    );
\vfb_vcdt_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => \vfb_vcdt[7]_i_1_n_0\,
      D => D(3),
      Q => vfb_vcdt(3),
      R => s_axis_aresetn_0
    );
\vfb_vcdt_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => \vfb_vcdt[7]_i_1_n_0\,
      D => D(4),
      Q => vfb_vcdt(4),
      R => s_axis_aresetn_0
    );
\vfb_vcdt_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => \vfb_vcdt[7]_i_1_n_0\,
      D => D(5),
      Q => vfb_vcdt(5),
      R => s_axis_aresetn_0
    );
\vfb_vcdt_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => \vfb_vcdt[7]_i_1_n_0\,
      D => D(6),
      Q => vfb_vcdt(6),
      R => s_axis_aresetn_0
    );
\vfb_vcdt_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => vfb_clk,
      CE => \vfb_vcdt[7]_i_1_n_0\,
      D => D(7),
      Q => vfb_vcdt(7),
      R => s_axis_aresetn_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_d10d_vfb_0_0_vfb_v1_0_11_reorder is
  port (
    cur_dtype_udef : out STD_LOGIC;
    cur_dtype_sink_reg_0 : out STD_LOGIC;
    s_axis_tready : out STD_LOGIC;
    vfb_eol_reg : out STD_LOGIC;
    sband_tl : out STD_LOGIC;
    sband_tact0 : out STD_LOGIC;
    \sband_td_r_reg[15]\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 24 downto 0 );
    sdt_tv : out STD_LOGIC;
    \buf_data_reg[1][134]_0\ : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    sband_tu : out STD_LOGIC;
    s_axis_aclk : in STD_LOGIC;
    \state_reg[1]\ : in STD_LOGIC;
    m_axis_tlast : in STD_LOGIC;
    m_axis_tvalid : in STD_LOGIC;
    vfb_valid_reg : in STD_LOGIC;
    vfb_eol : in STD_LOGIC;
    s_axis_tvalid : in STD_LOGIC;
    cur_dtype_sink_reg_1 : in STD_LOGIC;
    s_axis_aresetn : in STD_LOGIC;
    vfb_ready : in STD_LOGIC;
    vfb_valid : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 42 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of bd_d10d_vfb_0_0_vfb_v1_0_11_reorder : entity is "vfb_v1_0_11_reorder";
end bd_d10d_vfb_0_0_vfb_v1_0_11_reorder;

architecture STRUCTURE of bd_d10d_vfb_0_0_vfb_v1_0_11_reorder is
  signal \^q\ : STD_LOGIC_VECTOR ( 24 downto 0 );
  signal \buf_data[0][134]_i_1_n_0\ : STD_LOGIC;
  signal \buf_data[1][134]_i_1_n_0\ : STD_LOGIC;
  signal \buf_data_reg[1]\ : STD_LOGIC_VECTOR ( 134 downto 0 );
  signal \^buf_data_reg[1][134]_0\ : STD_LOGIC;
  signal \buf_valid[0]_i_1_n_0\ : STD_LOGIC;
  signal \buf_valid[1]_i_1_n_0\ : STD_LOGIC;
  signal \buf_valid_reg_n_0_[0]\ : STD_LOGIC;
  signal cur_dtype_sink_i_1_n_0 : STD_LOGIC;
  signal \^cur_dtype_sink_reg_0\ : STD_LOGIC;
  signal \^cur_dtype_udef\ : STD_LOGIC;
  signal cur_dtype_udef_i_1_n_0 : STD_LOGIC;
  signal cur_dtype_udef_i_2_n_0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal p_2_in : STD_LOGIC_VECTOR ( 134 downto 0 );
  signal s_axis_tdata : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal s_axis_tlast : STD_LOGIC;
  signal \^sband_tact0\ : STD_LOGIC;
  signal sband_tk : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \^sband_tl\ : STD_LOGIC;
  signal sdt_tv_INST_0_i_1_n_0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \buf_data[0][121]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \buf_valid[0]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \buf_valid[1]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of cur_dtype_sink_i_1 : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of cur_dtype_udef_i_2 : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of s_axis_tready_INST_0 : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \sband_td_r[10]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \sband_td_r[11]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \sband_td_r[12]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \sband_td_r[13]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \sband_td_r[14]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \sband_td_r[15]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \sband_td_r[15]_i_2\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \sband_td_r[1]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \sband_td_r[2]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \sband_td_r[3]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \sband_td_r[4]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \sband_td_r[5]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \sband_td_r[6]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \sband_td_r[7]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \sband_td_r[8]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \sband_td_r[9]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of sband_tl_r_i_2 : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \sband_tu_r[0]_i_1\ : label is "soft_lutpair21";
begin
  Q(24 downto 0) <= \^q\(24 downto 0);
  \buf_data_reg[1][134]_0\ <= \^buf_data_reg[1][134]_0\;
  cur_dtype_sink_reg_0 <= \^cur_dtype_sink_reg_0\;
  cur_dtype_udef <= \^cur_dtype_udef\;
  sband_tact0 <= \^sband_tact0\;
  sband_tl <= \^sband_tl\;
\buf_data[0][0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(0),
      I4 => D(0),
      O => p_2_in(0)
    );
\buf_data[0][100]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(100),
      I4 => D(9),
      O => p_2_in(100)
    );
\buf_data[0][102]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(102),
      I4 => D(10),
      O => p_2_in(102)
    );
\buf_data[0][103]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(103),
      I4 => D(11),
      O => p_2_in(103)
    );
\buf_data[0][104]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(104),
      I4 => D(12),
      O => p_2_in(104)
    );
\buf_data[0][105]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(105),
      I4 => D(13),
      O => p_2_in(105)
    );
\buf_data[0][106]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(106),
      I4 => D(14),
      O => p_2_in(106)
    );
\buf_data[0][107]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(107),
      I4 => D(15),
      O => p_2_in(107)
    );
\buf_data[0][108]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(108),
      I4 => D(16),
      O => p_2_in(108)
    );
\buf_data[0][109]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(109),
      I4 => D(17),
      O => p_2_in(109)
    );
\buf_data[0][110]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(110),
      I4 => D(18),
      O => p_2_in(110)
    );
\buf_data[0][111]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(111),
      I4 => D(19),
      O => p_2_in(111)
    );
\buf_data[0][112]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(112),
      I4 => D(20),
      O => p_2_in(112)
    );
\buf_data[0][113]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(113),
      I4 => D(21),
      O => p_2_in(113)
    );
\buf_data[0][114]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(114),
      I4 => D(22),
      O => p_2_in(114)
    );
\buf_data[0][115]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(115),
      I4 => D(23),
      O => p_2_in(115)
    );
\buf_data[0][116]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(116),
      I4 => D(24),
      O => p_2_in(116)
    );
\buf_data[0][117]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(117),
      I4 => D(25),
      O => p_2_in(117)
    );
\buf_data[0][118]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(118),
      I4 => D(26),
      O => p_2_in(118)
    );
\buf_data[0][119]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(119),
      I4 => D(27),
      O => p_2_in(119)
    );
\buf_data[0][120]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(120),
      I4 => D(28),
      O => p_2_in(120)
    );
\buf_data[0][121]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(121),
      I4 => D(29),
      O => p_2_in(121)
    );
\buf_data[0][122]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(122),
      I4 => D(30),
      O => p_2_in(122)
    );
\buf_data[0][123]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(123),
      I4 => D(31),
      O => p_2_in(123)
    );
\buf_data[0][124]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(124),
      I4 => D(32),
      O => p_2_in(124)
    );
\buf_data[0][125]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(125),
      I4 => D(33),
      O => p_2_in(125)
    );
\buf_data[0][126]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(126),
      I4 => D(34),
      O => p_2_in(126)
    );
\buf_data[0][127]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(127),
      I4 => D(35),
      O => p_2_in(127)
    );
\buf_data[0][128]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(128),
      I4 => D(36),
      O => p_2_in(128)
    );
\buf_data[0][129]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(129),
      I4 => D(37),
      O => p_2_in(129)
    );
\buf_data[0][130]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(130),
      I4 => D(38),
      O => p_2_in(130)
    );
\buf_data[0][131]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(131),
      I4 => D(39),
      O => p_2_in(131)
    );
\buf_data[0][132]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(132),
      I4 => D(40),
      O => p_2_in(132)
    );
\buf_data[0][133]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(133),
      I4 => D(41),
      O => p_2_in(133)
    );
\buf_data[0][134]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"04E4"
    )
        port map (
      I0 => p_0_in,
      I1 => s_axis_tvalid,
      I2 => \buf_valid_reg_n_0_[0]\,
      I3 => cur_dtype_sink_reg_1,
      O => \buf_data[0][134]_i_1_n_0\
    );
\buf_data[0][134]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(134),
      I4 => D(42),
      O => p_2_in(134)
    );
\buf_data[0][1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(1),
      I4 => D(1),
      O => p_2_in(1)
    );
\buf_data[0][2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(2),
      I4 => D(2),
      O => p_2_in(2)
    );
\buf_data[0][66]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(66),
      I4 => D(3),
      O => p_2_in(66)
    );
\buf_data[0][67]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(67),
      I4 => D(4),
      O => p_2_in(67)
    );
\buf_data[0][68]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(68),
      I4 => D(5),
      O => p_2_in(68)
    );
\buf_data[0][69]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(69),
      I4 => D(6),
      O => p_2_in(69)
    );
\buf_data[0][70]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(70),
      I4 => D(7),
      O => p_2_in(70)
    );
\buf_data[0][71]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70800"
    )
        port map (
      I0 => p_0_in,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      I3 => \buf_data_reg[1]\(71),
      I4 => D(8),
      O => p_2_in(71)
    );
\buf_data[1][134]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => cur_dtype_sink_reg_1,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => p_0_in,
      I3 => s_axis_tvalid,
      O => \buf_data[1][134]_i_1_n_0\
    );
\buf_data_reg[0][0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(0),
      Q => \^q\(0),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][100]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(100),
      Q => sband_tk(2),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][102]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(102),
      Q => \^q\(9),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][103]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(103),
      Q => \^q\(10),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][104]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(104),
      Q => \^q\(11),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][105]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(105),
      Q => \^q\(12),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][106]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(106),
      Q => \^q\(13),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][107]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(107),
      Q => \^q\(14),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][108]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(108),
      Q => \^q\(15),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][109]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(109),
      Q => \^q\(16),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][110]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(110),
      Q => \^q\(17),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][111]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(111),
      Q => \^q\(18),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][112]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(112),
      Q => \^q\(19),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][113]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(113),
      Q => \^q\(20),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][114]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(114),
      Q => \^q\(21),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][115]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(115),
      Q => \^q\(22),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][116]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(116),
      Q => \^q\(23),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][117]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(117),
      Q => \^q\(24),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][118]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(118),
      Q => s_axis_tdata(16),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][119]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(119),
      Q => s_axis_tdata(17),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][120]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(120),
      Q => s_axis_tdata(18),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][121]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(121),
      Q => s_axis_tdata(19),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][122]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(122),
      Q => s_axis_tdata(20),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][123]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(123),
      Q => s_axis_tdata(21),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][124]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(124),
      Q => s_axis_tdata(22),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][125]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(125),
      Q => s_axis_tdata(23),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][126]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(126),
      Q => s_axis_tdata(24),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][127]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(127),
      Q => s_axis_tdata(25),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][128]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(128),
      Q => s_axis_tdata(26),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][129]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(129),
      Q => s_axis_tdata(27),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][130]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(130),
      Q => s_axis_tdata(28),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][131]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(131),
      Q => s_axis_tdata(29),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][132]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(132),
      Q => s_axis_tdata(30),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][133]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(133),
      Q => s_axis_tdata(31),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][134]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(134),
      Q => s_axis_tlast,
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(1),
      Q => \^q\(1),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(2),
      Q => \^q\(2),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][66]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(66),
      Q => \^q\(3),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][67]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(67),
      Q => \^q\(4),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][68]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(68),
      Q => \^q\(5),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][69]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(69),
      Q => \^q\(6),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][70]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(70),
      Q => \^q\(7),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[0][71]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[0][134]_i_1_n_0\,
      D => p_2_in(71),
      Q => \^q\(8),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(0),
      Q => \buf_data_reg[1]\(0),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][100]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(9),
      Q => \buf_data_reg[1]\(100),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][102]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(10),
      Q => \buf_data_reg[1]\(102),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][103]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(11),
      Q => \buf_data_reg[1]\(103),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][104]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(12),
      Q => \buf_data_reg[1]\(104),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][105]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(13),
      Q => \buf_data_reg[1]\(105),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][106]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(14),
      Q => \buf_data_reg[1]\(106),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][107]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(15),
      Q => \buf_data_reg[1]\(107),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][108]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(16),
      Q => \buf_data_reg[1]\(108),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][109]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(17),
      Q => \buf_data_reg[1]\(109),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][110]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(18),
      Q => \buf_data_reg[1]\(110),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][111]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(19),
      Q => \buf_data_reg[1]\(111),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][112]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(20),
      Q => \buf_data_reg[1]\(112),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][113]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(21),
      Q => \buf_data_reg[1]\(113),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][114]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(22),
      Q => \buf_data_reg[1]\(114),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][115]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(23),
      Q => \buf_data_reg[1]\(115),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][116]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(24),
      Q => \buf_data_reg[1]\(116),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][117]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(25),
      Q => \buf_data_reg[1]\(117),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][118]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(26),
      Q => \buf_data_reg[1]\(118),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][119]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(27),
      Q => \buf_data_reg[1]\(119),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][120]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(28),
      Q => \buf_data_reg[1]\(120),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][121]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(29),
      Q => \buf_data_reg[1]\(121),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][122]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(30),
      Q => \buf_data_reg[1]\(122),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][123]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(31),
      Q => \buf_data_reg[1]\(123),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][124]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(32),
      Q => \buf_data_reg[1]\(124),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][125]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(33),
      Q => \buf_data_reg[1]\(125),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][126]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(34),
      Q => \buf_data_reg[1]\(126),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][127]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(35),
      Q => \buf_data_reg[1]\(127),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][128]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(36),
      Q => \buf_data_reg[1]\(128),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][129]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(37),
      Q => \buf_data_reg[1]\(129),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][130]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(38),
      Q => \buf_data_reg[1]\(130),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][131]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(39),
      Q => \buf_data_reg[1]\(131),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][132]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(40),
      Q => \buf_data_reg[1]\(132),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][133]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(41),
      Q => \buf_data_reg[1]\(133),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][134]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(42),
      Q => \buf_data_reg[1]\(134),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(1),
      Q => \buf_data_reg[1]\(1),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(2),
      Q => \buf_data_reg[1]\(2),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][66]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(3),
      Q => \buf_data_reg[1]\(66),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][67]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(4),
      Q => \buf_data_reg[1]\(67),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][68]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(5),
      Q => \buf_data_reg[1]\(68),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][69]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(6),
      Q => \buf_data_reg[1]\(69),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][70]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(7),
      Q => \buf_data_reg[1]\(70),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_data_reg[1][71]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \buf_data[1][134]_i_1_n_0\,
      D => D(8),
      Q => \buf_data_reg[1]\(71),
      R => \^buf_data_reg[1][134]_0\
    );
\buf_valid[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F4E4"
    )
        port map (
      I0 => p_0_in,
      I1 => s_axis_tvalid,
      I2 => \buf_valid_reg_n_0_[0]\,
      I3 => cur_dtype_sink_reg_1,
      O => \buf_valid[0]_i_1_n_0\
    );
\buf_valid[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B8B0"
    )
        port map (
      I0 => cur_dtype_sink_reg_1,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => p_0_in,
      I3 => s_axis_tvalid,
      O => \buf_valid[1]_i_1_n_0\
    );
\buf_valid_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => '1',
      D => \buf_valid[0]_i_1_n_0\,
      Q => \buf_valid_reg_n_0_[0]\,
      R => \^buf_data_reg[1][134]_0\
    );
\buf_valid_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => '1',
      D => \buf_valid[1]_i_1_n_0\,
      Q => p_0_in,
      R => \^buf_data_reg[1][134]_0\
    );
cur_dtype_sink_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => \buf_valid_reg_n_0_[0]\,
      I1 => sdt_tv_INST_0_i_1_n_0,
      I2 => \^cur_dtype_sink_reg_0\,
      O => cur_dtype_sink_i_1_n_0
    );
cur_dtype_sink_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => '1',
      D => cur_dtype_sink_i_1_n_0,
      Q => \^cur_dtype_sink_reg_0\,
      R => cur_dtype_udef_i_1_n_0
    );
cur_dtype_udef_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40FF"
    )
        port map (
      I0 => cur_dtype_sink_reg_1,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => s_axis_tlast,
      I3 => s_axis_aresetn,
      O => cur_dtype_udef_i_1_n_0
    );
cur_dtype_udef_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => \^cur_dtype_udef\,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => sdt_tv_INST_0_i_1_n_0,
      O => cur_dtype_udef_i_2_n_0
    );
cur_dtype_udef_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => '1',
      D => cur_dtype_udef_i_2_n_0,
      Q => \^cur_dtype_udef\,
      R => cur_dtype_udef_i_1_n_0
    );
s_axis_tready_INST_0: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => p_0_in,
      O => s_axis_tready
    );
\sband_td_r[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s_axis_tdata(16),
      I1 => \^sband_tact0\,
      O => \sband_td_r_reg[15]\(0)
    );
\sband_td_r[10]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s_axis_tdata(26),
      I1 => \^sband_tact0\,
      O => \sband_td_r_reg[15]\(10)
    );
\sband_td_r[11]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s_axis_tdata(27),
      I1 => \^sband_tact0\,
      O => \sband_td_r_reg[15]\(11)
    );
\sband_td_r[12]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s_axis_tdata(28),
      I1 => \^sband_tact0\,
      O => \sband_td_r_reg[15]\(12)
    );
\sband_td_r[13]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s_axis_tdata(29),
      I1 => \^sband_tact0\,
      O => \sband_td_r_reg[15]\(13)
    );
\sband_td_r[14]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s_axis_tdata(30),
      I1 => \^sband_tact0\,
      O => \sband_td_r_reg[15]\(14)
    );
\sband_td_r[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => \^sband_tact0\,
      I1 => vfb_ready,
      I2 => vfb_valid,
      O => E(0)
    );
\sband_td_r[15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s_axis_tdata(31),
      I1 => \^sband_tact0\,
      O => \sband_td_r_reg[15]\(15)
    );
\sband_td_r[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s_axis_tdata(17),
      I1 => \^sband_tact0\,
      O => \sband_td_r_reg[15]\(1)
    );
\sband_td_r[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s_axis_tdata(18),
      I1 => \^sband_tact0\,
      O => \sband_td_r_reg[15]\(2)
    );
\sband_td_r[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s_axis_tdata(19),
      I1 => \^sband_tact0\,
      O => \sband_td_r_reg[15]\(3)
    );
\sband_td_r[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s_axis_tdata(20),
      I1 => \^sband_tact0\,
      O => \sband_td_r_reg[15]\(4)
    );
\sband_td_r[5]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s_axis_tdata(21),
      I1 => \^sband_tact0\,
      O => \sband_td_r_reg[15]\(5)
    );
\sband_td_r[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s_axis_tdata(22),
      I1 => \^sband_tact0\,
      O => \sband_td_r_reg[15]\(6)
    );
\sband_td_r[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s_axis_tdata(23),
      I1 => \^sband_tact0\,
      O => \sband_td_r_reg[15]\(7)
    );
\sband_td_r[8]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s_axis_tdata(24),
      I1 => \^sband_tact0\,
      O => \sband_td_r_reg[15]\(8)
    );
\sband_td_r[9]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s_axis_tdata(25),
      I1 => \^sband_tact0\,
      O => \sband_td_r_reg[15]\(9)
    );
sband_tl_r_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000001000000"
    )
        port map (
      I0 => vfb_valid_reg,
      I1 => vfb_eol,
      I2 => m_axis_tvalid,
      I3 => \^cur_dtype_udef\,
      I4 => \buf_valid_reg_n_0_[0]\,
      I5 => sdt_tv_INST_0_i_1_n_0,
      O => \^sband_tact0\
    );
sband_tl_r_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => s_axis_tlast,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => cur_dtype_sink_reg_1,
      O => \^sband_tl\
    );
\sband_tu_r[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => sdt_tv_INST_0_i_1_n_0,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => \^q\(2),
      I3 => cur_dtype_sink_reg_1,
      O => sband_tu
    );
sdt_tv_INST_0: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^cur_dtype_udef\,
      I1 => \buf_valid_reg_n_0_[0]\,
      I2 => sdt_tv_INST_0_i_1_n_0,
      O => sdt_tv
    );
sdt_tv_INST_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F7F7F7F7F7F7D7F7"
    )
        port map (
      I0 => \^q\(8),
      I1 => \^q\(7),
      I2 => \^q\(6),
      I3 => \^q\(4),
      I4 => \^q\(5),
      I5 => \^q\(3),
      O => sdt_tv_INST_0_i_1_n_0
    );
vfb_eol_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF105510551055"
    )
        port map (
      I0 => \state_reg[1]\,
      I1 => sband_tk(2),
      I2 => \^sband_tl\,
      I3 => \^sband_tact0\,
      I4 => m_axis_tlast,
      I5 => m_axis_tvalid,
      O => vfb_eol_reg
    );
vfb_valid_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s_axis_aresetn,
      O => \^buf_data_reg[1][134]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_d10d_vfb_0_0_axis_dwidth_converter_v1_1_16_axis_dwidth_converter is
  port (
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axis_tlast : out STD_LOGIC;
    m_axis_tid : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axis_tdata : out STD_LOGIC_VECTOR ( 15 downto 0 );
    m_axis_tkeep : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axis_tuser : out STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axis_tkeep : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axis_tvalid : in STD_LOGIC;
    aclk : in STD_LOGIC;
    m_axis_tready : in STD_LOGIC;
    s_axis_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_tlast : in STD_LOGIC;
    s_axis_tid : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_tuser : in STD_LOGIC_VECTOR ( 11 downto 0 );
    aresetn : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of bd_d10d_vfb_0_0_axis_dwidth_converter_v1_1_16_axis_dwidth_converter : entity is "axis_dwidth_converter_v1_1_16_axis_dwidth_converter";
end bd_d10d_vfb_0_0_axis_dwidth_converter_v1_1_16_axis_dwidth_converter;

architecture STRUCTURE of bd_d10d_vfb_0_0_axis_dwidth_converter_v1_1_16_axis_dwidth_converter is
  signal areset_r : STD_LOGIC;
  signal areset_r_i_1_n_0 : STD_LOGIC;
begin
areset_r_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => aresetn,
      O => areset_r_i_1_n_0
    );
areset_r_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => aclk,
      CE => '1',
      D => areset_r_i_1_n_0,
      Q => areset_r,
      R => '0'
    );
\gen_downsizer_conversion.axisc_downsizer_0\: entity work.bd_d10d_vfb_0_0_axis_dwidth_converter_v1_1_16_axisc_downsizer
     port map (
      Q(1 downto 0) => Q(1 downto 0),
      SR(0) => areset_r,
      aclk => aclk,
      m_axis_tdata(15 downto 0) => m_axis_tdata(15 downto 0),
      m_axis_tid(31 downto 0) => m_axis_tid(31 downto 0),
      m_axis_tkeep(1 downto 0) => m_axis_tkeep(1 downto 0),
      m_axis_tlast => m_axis_tlast,
      m_axis_tready => m_axis_tready,
      m_axis_tuser(5 downto 0) => m_axis_tuser(5 downto 0),
      s_axis_tdata(31 downto 0) => s_axis_tdata(31 downto 0),
      s_axis_tid(31 downto 0) => s_axis_tid(31 downto 0),
      s_axis_tkeep(3 downto 0) => s_axis_tkeep(3 downto 0),
      s_axis_tlast => s_axis_tlast,
      s_axis_tuser(11 downto 0) => s_axis_tuser(11 downto 0),
      s_axis_tvalid => s_axis_tvalid
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_axis_converter is
  port (
    aclk : in STD_LOGIC;
    aresetn : in STD_LOGIC;
    s_axis_tvalid : in STD_LOGIC;
    s_axis_tready : out STD_LOGIC;
    s_axis_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_tkeep : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axis_tlast : in STD_LOGIC;
    s_axis_tid : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_tuser : in STD_LOGIC_VECTOR ( 11 downto 0 );
    m_axis_tvalid : out STD_LOGIC;
    m_axis_tready : in STD_LOGIC;
    m_axis_tdata : out STD_LOGIC_VECTOR ( 15 downto 0 );
    m_axis_tkeep : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axis_tlast : out STD_LOGIC;
    m_axis_tid : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axis_tuser : out STD_LOGIC_VECTOR ( 5 downto 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_axis_converter : entity is "bd_d10d_vfb_0_0_axis_converter,axis_dwidth_converter_v1_1_16_axis_dwidth_converter,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_axis_converter : entity is "yes";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_axis_converter : entity is "bd_d10d_vfb_0_0_axis_converter";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_axis_converter : entity is "axis_dwidth_converter_v1_1_16_axis_dwidth_converter,Vivado 2018.2.1";
end bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_axis_converter;

architecture STRUCTURE of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_axis_converter is
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of aclk : signal is "xilinx.com:signal:clock:1.0 CLKIF CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of aclk : signal is "XIL_INTERFACENAME CLKIF, FREQ_HZ 10000000, PHASE 0.000";
  attribute X_INTERFACE_INFO of aresetn : signal is "xilinx.com:signal:reset:1.0 RSTIF RST";
  attribute X_INTERFACE_PARAMETER of aresetn : signal is "XIL_INTERFACENAME RSTIF, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of m_axis_tlast : signal is "xilinx.com:interface:axis:1.0 M_AXIS TLAST";
  attribute X_INTERFACE_INFO of m_axis_tready : signal is "xilinx.com:interface:axis:1.0 M_AXIS TREADY";
  attribute X_INTERFACE_INFO of m_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 M_AXIS TVALID";
  attribute X_INTERFACE_INFO of s_axis_tlast : signal is "xilinx.com:interface:axis:1.0 S_AXIS TLAST";
  attribute X_INTERFACE_INFO of s_axis_tready : signal is "xilinx.com:interface:axis:1.0 S_AXIS TREADY";
  attribute X_INTERFACE_INFO of s_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 S_AXIS TVALID";
  attribute X_INTERFACE_INFO of m_axis_tdata : signal is "xilinx.com:interface:axis:1.0 M_AXIS TDATA";
  attribute X_INTERFACE_INFO of m_axis_tid : signal is "xilinx.com:interface:axis:1.0 M_AXIS TID";
  attribute X_INTERFACE_INFO of m_axis_tkeep : signal is "xilinx.com:interface:axis:1.0 M_AXIS TKEEP";
  attribute X_INTERFACE_INFO of m_axis_tuser : signal is "xilinx.com:interface:axis:1.0 M_AXIS TUSER";
  attribute X_INTERFACE_PARAMETER of m_axis_tuser : signal is "XIL_INTERFACENAME M_AXIS, TDATA_NUM_BYTES 1, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of s_axis_tdata : signal is "xilinx.com:interface:axis:1.0 S_AXIS TDATA";
  attribute X_INTERFACE_INFO of s_axis_tid : signal is "xilinx.com:interface:axis:1.0 S_AXIS TID";
  attribute X_INTERFACE_INFO of s_axis_tkeep : signal is "xilinx.com:interface:axis:1.0 S_AXIS TKEEP";
  attribute X_INTERFACE_INFO of s_axis_tuser : signal is "xilinx.com:interface:axis:1.0 S_AXIS TUSER";
  attribute X_INTERFACE_PARAMETER of s_axis_tuser : signal is "XIL_INTERFACENAME S_AXIS, TDATA_NUM_BYTES 1, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, LAYERED_METADATA undef";
begin
inst: entity work.bd_d10d_vfb_0_0_axis_dwidth_converter_v1_1_16_axis_dwidth_converter
     port map (
      Q(1) => m_axis_tvalid,
      Q(0) => s_axis_tready,
      aclk => aclk,
      aresetn => aresetn,
      m_axis_tdata(15 downto 0) => m_axis_tdata(15 downto 0),
      m_axis_tid(31 downto 0) => m_axis_tid(31 downto 0),
      m_axis_tkeep(1 downto 0) => m_axis_tkeep(1 downto 0),
      m_axis_tlast => m_axis_tlast,
      m_axis_tready => m_axis_tready,
      m_axis_tuser(5 downto 0) => m_axis_tuser(5 downto 0),
      s_axis_tdata(31 downto 0) => s_axis_tdata(31 downto 0),
      s_axis_tid(31 downto 0) => s_axis_tid(31 downto 0),
      s_axis_tkeep(3 downto 0) => s_axis_tkeep(3 downto 0),
      s_axis_tlast => s_axis_tlast,
      s_axis_tuser(11 downto 0) => s_axis_tuser(11 downto 0),
      s_axis_tvalid => s_axis_tvalid
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_axis_dconverter is
  port (
    m_axis_tvalid : out STD_LOGIC;
    m_axis_tlast : out STD_LOGIC;
    \vfb_sof_reg[0]\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \vfb_data_reg[15]\ : out STD_LOGIC;
    \vfb_data_reg[14]\ : out STD_LOGIC;
    \vfb_data_reg[13]\ : out STD_LOGIC;
    \vfb_data_reg[12]\ : out STD_LOGIC;
    \vfb_data_reg[11]\ : out STD_LOGIC;
    \vfb_data_reg[10]\ : out STD_LOGIC;
    \vfb_data_reg[9]\ : out STD_LOGIC;
    \vfb_data_reg[8]\ : out STD_LOGIC;
    \vfb_data_reg[7]\ : out STD_LOGIC;
    \vfb_data_reg[6]\ : out STD_LOGIC;
    \vfb_data_reg[5]\ : out STD_LOGIC;
    \vfb_data_reg[4]\ : out STD_LOGIC;
    \vfb_data_reg[3]\ : out STD_LOGIC;
    \vfb_data_reg[2]\ : out STD_LOGIC;
    \vfb_data_reg[1]\ : out STD_LOGIC;
    \vfb_data_reg[0]\ : out STD_LOGIC;
    vfb_clk : in STD_LOGIC;
    s_axis_aresetn : in STD_LOGIC;
    mdt_tr : in STD_LOGIC;
    sband_tu_r : in STD_LOGIC;
    sband_tact0 : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 23 downto 0 );
    vfb_eol_reg : in STD_LOGIC;
    \sband_ts_r_reg[7]\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \sband_td_r_reg[15]\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    sband_tact_reg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_axis_dconverter : entity is "bd_d10d_vfb_0_0_axis_dconverter";
end bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_axis_dconverter;

architecture STRUCTURE of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_axis_dconverter is
  signal m_axis_tdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal m_axis_tid : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m_axis_tkeep : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal m_axis_tuser : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^m_axis_tvalid\ : STD_LOGIC;
  signal s_fifo_tr : STD_LOGIC;
  signal NLW_axis_conv_inst_m_axis_tuser_UNCONNECTED : STD_LOGIC_VECTOR ( 5 downto 3 );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of axis_conv_inst : label is "bd_d10d_vfb_0_0_axis_converter,axis_dwidth_converter_v1_1_16_axis_dwidth_converter,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of axis_conv_inst : label is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of axis_conv_inst : label is "axis_dwidth_converter_v1_1_16_axis_dwidth_converter,Vivado 2018.2.1";
begin
  m_axis_tvalid <= \^m_axis_tvalid\;
axis_conv_inst: entity work.bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_axis_converter
     port map (
      aclk => vfb_clk,
      aresetn => s_axis_aresetn,
      m_axis_tdata(15 downto 0) => m_axis_tdata(15 downto 0),
      m_axis_tid(31 downto 0) => m_axis_tid(31 downto 0),
      m_axis_tkeep(1 downto 0) => m_axis_tkeep(1 downto 0),
      m_axis_tlast => m_axis_tlast,
      m_axis_tready => mdt_tr,
      m_axis_tuser(5 downto 3) => NLW_axis_conv_inst_m_axis_tuser_UNCONNECTED(5 downto 3),
      m_axis_tuser(2 downto 0) => m_axis_tuser(2 downto 0),
      m_axis_tvalid => \^m_axis_tvalid\,
      s_axis_tdata(31 downto 0) => B"00000000000000000000000000000000",
      s_axis_tid(31 downto 0) => B"00000000000000000000000000000000",
      s_axis_tkeep(3 downto 0) => B"0000",
      s_axis_tlast => '0',
      s_axis_tready => s_fifo_tr,
      s_axis_tuser(11 downto 0) => B"000000000000",
      s_axis_tvalid => '0'
    );
\vfb_data[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBB888B888B888"
    )
        port map (
      I0 => m_axis_tdata(0),
      I1 => \^m_axis_tvalid\,
      I2 => Q(8),
      I3 => sband_tact0,
      I4 => \sband_td_r_reg[15]\(0),
      I5 => sband_tact_reg,
      O => \vfb_data_reg[0]\
    );
\vfb_data[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBB888B888B888"
    )
        port map (
      I0 => m_axis_tdata(10),
      I1 => \^m_axis_tvalid\,
      I2 => Q(18),
      I3 => sband_tact0,
      I4 => \sband_td_r_reg[15]\(10),
      I5 => sband_tact_reg,
      O => \vfb_data_reg[10]\
    );
\vfb_data[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBB888B888B888"
    )
        port map (
      I0 => m_axis_tdata(11),
      I1 => \^m_axis_tvalid\,
      I2 => Q(19),
      I3 => sband_tact0,
      I4 => \sband_td_r_reg[15]\(11),
      I5 => sband_tact_reg,
      O => \vfb_data_reg[11]\
    );
\vfb_data[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBB888B888B888"
    )
        port map (
      I0 => m_axis_tdata(12),
      I1 => \^m_axis_tvalid\,
      I2 => Q(20),
      I3 => sband_tact0,
      I4 => \sband_td_r_reg[15]\(12),
      I5 => sband_tact_reg,
      O => \vfb_data_reg[12]\
    );
\vfb_data[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBB888B888B888"
    )
        port map (
      I0 => m_axis_tdata(13),
      I1 => \^m_axis_tvalid\,
      I2 => Q(21),
      I3 => sband_tact0,
      I4 => \sband_td_r_reg[15]\(13),
      I5 => sband_tact_reg,
      O => \vfb_data_reg[13]\
    );
\vfb_data[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBB888B888B888"
    )
        port map (
      I0 => m_axis_tdata(14),
      I1 => \^m_axis_tvalid\,
      I2 => Q(22),
      I3 => sband_tact0,
      I4 => \sband_td_r_reg[15]\(14),
      I5 => sband_tact_reg,
      O => \vfb_data_reg[14]\
    );
\vfb_data[15]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBB888B888B888"
    )
        port map (
      I0 => m_axis_tdata(15),
      I1 => \^m_axis_tvalid\,
      I2 => Q(23),
      I3 => sband_tact0,
      I4 => \sband_td_r_reg[15]\(15),
      I5 => sband_tact_reg,
      O => \vfb_data_reg[15]\
    );
\vfb_data[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBB888B888B888"
    )
        port map (
      I0 => m_axis_tdata(1),
      I1 => \^m_axis_tvalid\,
      I2 => Q(9),
      I3 => sband_tact0,
      I4 => \sband_td_r_reg[15]\(1),
      I5 => sband_tact_reg,
      O => \vfb_data_reg[1]\
    );
\vfb_data[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBB888B888B888"
    )
        port map (
      I0 => m_axis_tdata(2),
      I1 => \^m_axis_tvalid\,
      I2 => Q(10),
      I3 => sband_tact0,
      I4 => \sband_td_r_reg[15]\(2),
      I5 => sband_tact_reg,
      O => \vfb_data_reg[2]\
    );
\vfb_data[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBB888B888B888"
    )
        port map (
      I0 => m_axis_tdata(3),
      I1 => \^m_axis_tvalid\,
      I2 => Q(11),
      I3 => sband_tact0,
      I4 => \sband_td_r_reg[15]\(3),
      I5 => sband_tact_reg,
      O => \vfb_data_reg[3]\
    );
\vfb_data[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBB888B888B888"
    )
        port map (
      I0 => m_axis_tdata(4),
      I1 => \^m_axis_tvalid\,
      I2 => Q(12),
      I3 => sband_tact0,
      I4 => \sband_td_r_reg[15]\(4),
      I5 => sband_tact_reg,
      O => \vfb_data_reg[4]\
    );
\vfb_data[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBB888B888B888"
    )
        port map (
      I0 => m_axis_tdata(5),
      I1 => \^m_axis_tvalid\,
      I2 => Q(13),
      I3 => sband_tact0,
      I4 => \sband_td_r_reg[15]\(5),
      I5 => sband_tact_reg,
      O => \vfb_data_reg[5]\
    );
\vfb_data[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBB888B888B888"
    )
        port map (
      I0 => m_axis_tdata(6),
      I1 => \^m_axis_tvalid\,
      I2 => Q(14),
      I3 => sband_tact0,
      I4 => \sband_td_r_reg[15]\(6),
      I5 => sband_tact_reg,
      O => \vfb_data_reg[6]\
    );
\vfb_data[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBB888B888B888"
    )
        port map (
      I0 => m_axis_tdata(7),
      I1 => \^m_axis_tvalid\,
      I2 => Q(15),
      I3 => sband_tact0,
      I4 => \sband_td_r_reg[15]\(7),
      I5 => sband_tact_reg,
      O => \vfb_data_reg[7]\
    );
\vfb_data[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBB888B888B888"
    )
        port map (
      I0 => m_axis_tdata(8),
      I1 => \^m_axis_tvalid\,
      I2 => Q(16),
      I3 => sband_tact0,
      I4 => \sband_td_r_reg[15]\(8),
      I5 => sband_tact_reg,
      O => \vfb_data_reg[8]\
    );
\vfb_data[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBB888B888B888"
    )
        port map (
      I0 => m_axis_tdata(9),
      I1 => \^m_axis_tvalid\,
      I2 => Q(17),
      I3 => sband_tact0,
      I4 => \sband_td_r_reg[15]\(9),
      I5 => sband_tact_reg,
      O => \vfb_data_reg[9]\
    );
\vfb_sof[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88888888BBB888B8"
    )
        port map (
      I0 => m_axis_tuser(0),
      I1 => \^m_axis_tvalid\,
      I2 => sband_tu_r,
      I3 => sband_tact0,
      I4 => Q(2),
      I5 => vfb_eol_reg,
      O => \vfb_sof_reg[0]\
    );
\vfb_vcdt[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => m_axis_tid(0),
      I1 => \^m_axis_tvalid\,
      I2 => Q(0),
      I3 => sband_tact0,
      I4 => \sband_ts_r_reg[7]\(0),
      O => D(0)
    );
\vfb_vcdt[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => m_axis_tid(1),
      I1 => \^m_axis_tvalid\,
      I2 => Q(1),
      I3 => sband_tact0,
      I4 => \sband_ts_r_reg[7]\(1),
      O => D(1)
    );
\vfb_vcdt[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => m_axis_tid(2),
      I1 => \^m_axis_tvalid\,
      I2 => Q(3),
      I3 => sband_tact0,
      I4 => \sband_ts_r_reg[7]\(2),
      O => D(2)
    );
\vfb_vcdt[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => m_axis_tid(3),
      I1 => \^m_axis_tvalid\,
      I2 => Q(4),
      I3 => sband_tact0,
      I4 => \sband_ts_r_reg[7]\(3),
      O => D(3)
    );
\vfb_vcdt[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => m_axis_tid(4),
      I1 => \^m_axis_tvalid\,
      I2 => Q(5),
      I3 => sband_tact0,
      I4 => \sband_ts_r_reg[7]\(4),
      O => D(4)
    );
\vfb_vcdt[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => m_axis_tid(5),
      I1 => \^m_axis_tvalid\,
      I2 => Q(6),
      I3 => sband_tact0,
      I4 => \sband_ts_r_reg[7]\(5),
      O => D(5)
    );
\vfb_vcdt[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => m_axis_tid(6),
      I1 => \^m_axis_tvalid\,
      I2 => Q(7),
      I3 => sband_tact0,
      I4 => \sband_ts_r_reg[7]\(6),
      O => D(6)
    );
\vfb_vcdt[7]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BBB8"
    )
        port map (
      I0 => m_axis_tid(7),
      I1 => \^m_axis_tvalid\,
      I2 => \sband_ts_r_reg[7]\(7),
      I3 => sband_tact0,
      O => D(7)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core is
  port (
    s_axis_aclk : in STD_LOGIC;
    s_axis_aresetn : in STD_LOGIC;
    s_axis_tready : out STD_LOGIC;
    s_axis_tvalid : in STD_LOGIC;
    s_axis_tlast : in STD_LOGIC;
    s_axis_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_tkeep : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axis_tuser : in STD_LOGIC_VECTOR ( 95 downto 0 );
    s_axis_tdest : in STD_LOGIC_VECTOR ( 1 downto 0 );
    mdt_tv : out STD_LOGIC;
    mdt_tr : out STD_LOGIC;
    sdt_tv : out STD_LOGIC;
    sdt_tr : out STD_LOGIC;
    vfb_tv : out STD_LOGIC;
    vfb_tr : out STD_LOGIC;
    vfb_clk : in STD_LOGIC;
    vfb_ready : in STD_LOGIC;
    vfb_valid : out STD_LOGIC;
    vfb_eol : out STD_LOGIC;
    vfb_sof : out STD_LOGIC_VECTOR ( 0 to 0 );
    vfb_vcdt : out STD_LOGIC_VECTOR ( 7 downto 0 );
    vfb_data : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute AXIS_TDATA_WIDTH : integer;
  attribute AXIS_TDATA_WIDTH of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core : entity is 32;
  attribute AXIS_TDEST_WIDTH : integer;
  attribute AXIS_TDEST_WIDTH of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core : entity is 2;
  attribute AXIS_TUSER_WIDTH : integer;
  attribute AXIS_TUSER_WIDTH of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core : entity is 96;
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core : entity is "yes";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core : entity is "bd_d10d_vfb_0_0_core";
  attribute VFB_4PXL_W : integer;
  attribute VFB_4PXL_W of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core : entity is 32;
  attribute VFB_BYPASS_WC : integer;
  attribute VFB_BYPASS_WC of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core : entity is 0;
  attribute VFB_DATA_TYPE : integer;
  attribute VFB_DATA_TYPE of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core : entity is 42;
  attribute VFB_DCONV_OWIDTH : integer;
  attribute VFB_DCONV_OWIDTH of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core : entity is 16;
  attribute VFB_DCONV_TUW : integer;
  attribute VFB_DCONV_TUW of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core : entity is 12;
  attribute VFB_FIFO_DEPTH : integer;
  attribute VFB_FIFO_DEPTH of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core : entity is 128;
  attribute VFB_FIFO_WIDTH : integer;
  attribute VFB_FIFO_WIDTH of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core : entity is 32;
  attribute VFB_FILTER_VC : integer;
  attribute VFB_FILTER_VC of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core : entity is 0;
  attribute VFB_OP_DWIDTH : integer;
  attribute VFB_OP_DWIDTH of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core : entity is 16;
  attribute VFB_OP_PIXELS : integer;
  attribute VFB_OP_PIXELS of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core : entity is 2;
  attribute VFB_PXL_W : integer;
  attribute VFB_PXL_W of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core : entity is 8;
  attribute VFB_PXL_W_BB : integer;
  attribute VFB_PXL_W_BB of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core : entity is 8;
  attribute VFB_REQ_BUFFER : integer;
  attribute VFB_REQ_BUFFER of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core : entity is 0;
  attribute VFB_REQ_REORDER : integer;
  attribute VFB_REQ_REORDER of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core : entity is 0;
  attribute VFB_TSB0_WIDTH : integer;
  attribute VFB_TSB0_WIDTH of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core : entity is 32;
  attribute VFB_TSB1_WIDTH : integer;
  attribute VFB_TSB1_WIDTH of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core : entity is 0;
  attribute VFB_TSB2_WIDTH : integer;
  attribute VFB_TSB2_WIDTH of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core : entity is 3;
  attribute VFB_TU_WIDTH : integer;
  attribute VFB_TU_WIDTH of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core : entity is 1;
  attribute VFB_VC : integer;
  attribute VFB_VC of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core : entity is 0;
end bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core;

architecture STRUCTURE of bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core is
  signal axis_dconverter_n_11 : STD_LOGIC;
  signal axis_dconverter_n_12 : STD_LOGIC;
  signal axis_dconverter_n_13 : STD_LOGIC;
  signal axis_dconverter_n_14 : STD_LOGIC;
  signal axis_dconverter_n_15 : STD_LOGIC;
  signal axis_dconverter_n_16 : STD_LOGIC;
  signal axis_dconverter_n_17 : STD_LOGIC;
  signal axis_dconverter_n_18 : STD_LOGIC;
  signal axis_dconverter_n_19 : STD_LOGIC;
  signal axis_dconverter_n_2 : STD_LOGIC;
  signal axis_dconverter_n_20 : STD_LOGIC;
  signal axis_dconverter_n_21 : STD_LOGIC;
  signal axis_dconverter_n_22 : STD_LOGIC;
  signal axis_dconverter_n_23 : STD_LOGIC;
  signal axis_dconverter_n_24 : STD_LOGIC;
  signal axis_dconverter_n_25 : STD_LOGIC;
  signal axis_dconverter_n_26 : STD_LOGIC;
  signal cur_dtype_udef : STD_LOGIC;
  signal m_axis_tlast : STD_LOGIC;
  signal \^mdt_tr\ : STD_LOGIC;
  signal \^mdt_tv\ : STD_LOGIC;
  signal n_0_76 : STD_LOGIC;
  signal op_inf_n_10 : STD_LOGIC;
  signal op_inf_n_27 : STD_LOGIC;
  signal op_inf_n_28 : STD_LOGIC;
  signal op_inf_n_29 : STD_LOGIC;
  signal op_inf_n_30 : STD_LOGIC;
  signal op_inf_n_31 : STD_LOGIC;
  signal op_inf_n_32 : STD_LOGIC;
  signal op_inf_n_33 : STD_LOGIC;
  signal op_inf_n_34 : STD_LOGIC;
  signal op_inf_n_35 : STD_LOGIC;
  signal op_inf_n_36 : STD_LOGIC;
  signal op_inf_n_37 : STD_LOGIC;
  signal op_inf_n_38 : STD_LOGIC;
  signal op_inf_n_39 : STD_LOGIC;
  signal op_inf_n_40 : STD_LOGIC;
  signal op_inf_n_41 : STD_LOGIC;
  signal op_inf_n_42 : STD_LOGIC;
  signal op_inf_n_6 : STD_LOGIC;
  signal op_inf_n_7 : STD_LOGIC;
  signal op_inf_n_8 : STD_LOGIC;
  signal op_inf_n_9 : STD_LOGIC;
  signal p_1_in : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal reorder_n_1 : STD_LOGIC;
  signal reorder_n_3 : STD_LOGIC;
  signal reorder_n_44 : STD_LOGIC;
  signal reorder_n_48 : STD_LOGIC;
  signal reorder_n_49 : STD_LOGIC;
  signal \s_axis_tdata__0\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal sband_tact0 : STD_LOGIC;
  signal sband_td_r : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal sband_tl : STD_LOGIC;
  signal sband_ts : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal \sband_ts__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal sband_ts_r : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal sband_tu : STD_LOGIC;
  signal sband_tu_r : STD_LOGIC;
  signal \^vfb_eol\ : STD_LOGIC;
  signal \^vfb_ready\ : STD_LOGIC;
  signal \^vfb_valid\ : STD_LOGIC;
begin
  \^vfb_ready\ <= vfb_ready;
  mdt_tr <= \^mdt_tr\;
  mdt_tv <= \^mdt_tv\;
  vfb_eol <= \^vfb_eol\;
  vfb_tr <= \^vfb_ready\;
  vfb_tv <= \^vfb_valid\;
  vfb_valid <= \^vfb_valid\;
axis_dconverter: entity work.bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_axis_dconverter
     port map (
      D(7 downto 0) => p_1_in(7 downto 0),
      Q(23 downto 8) => \s_axis_tdata__0\(15 downto 0),
      Q(7 downto 3) => sband_ts(6 downto 2),
      Q(2) => reorder_n_44,
      Q(1 downto 0) => \sband_ts__0\(1 downto 0),
      m_axis_tlast => m_axis_tlast,
      m_axis_tvalid => \^mdt_tv\,
      mdt_tr => \^mdt_tr\,
      s_axis_aresetn => s_axis_aresetn,
      sband_tact0 => sband_tact0,
      sband_tact_reg => op_inf_n_8,
      \sband_td_r_reg[15]\(15) => op_inf_n_27,
      \sband_td_r_reg[15]\(14) => op_inf_n_28,
      \sband_td_r_reg[15]\(13) => op_inf_n_29,
      \sband_td_r_reg[15]\(12) => op_inf_n_30,
      \sband_td_r_reg[15]\(11) => op_inf_n_31,
      \sband_td_r_reg[15]\(10) => op_inf_n_32,
      \sband_td_r_reg[15]\(9) => op_inf_n_33,
      \sband_td_r_reg[15]\(8) => op_inf_n_34,
      \sband_td_r_reg[15]\(7) => op_inf_n_35,
      \sband_td_r_reg[15]\(6) => op_inf_n_36,
      \sband_td_r_reg[15]\(5) => op_inf_n_37,
      \sband_td_r_reg[15]\(4) => op_inf_n_38,
      \sband_td_r_reg[15]\(3) => op_inf_n_39,
      \sband_td_r_reg[15]\(2) => op_inf_n_40,
      \sband_td_r_reg[15]\(1) => op_inf_n_41,
      \sband_td_r_reg[15]\(0) => op_inf_n_42,
      \sband_ts_r_reg[7]\(7 downto 0) => sband_ts_r(7 downto 0),
      sband_tu_r => sband_tu_r,
      vfb_clk => vfb_clk,
      \vfb_data_reg[0]\ => axis_dconverter_n_26,
      \vfb_data_reg[10]\ => axis_dconverter_n_16,
      \vfb_data_reg[11]\ => axis_dconverter_n_15,
      \vfb_data_reg[12]\ => axis_dconverter_n_14,
      \vfb_data_reg[13]\ => axis_dconverter_n_13,
      \vfb_data_reg[14]\ => axis_dconverter_n_12,
      \vfb_data_reg[15]\ => axis_dconverter_n_11,
      \vfb_data_reg[1]\ => axis_dconverter_n_25,
      \vfb_data_reg[2]\ => axis_dconverter_n_24,
      \vfb_data_reg[3]\ => axis_dconverter_n_23,
      \vfb_data_reg[4]\ => axis_dconverter_n_22,
      \vfb_data_reg[5]\ => axis_dconverter_n_21,
      \vfb_data_reg[6]\ => axis_dconverter_n_20,
      \vfb_data_reg[7]\ => axis_dconverter_n_19,
      \vfb_data_reg[8]\ => axis_dconverter_n_18,
      \vfb_data_reg[9]\ => axis_dconverter_n_17,
      vfb_eol_reg => op_inf_n_7,
      \vfb_sof_reg[0]\ => axis_dconverter_n_2
    );
i_76: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s_axis_aresetn,
      O => n_0_76
    );
op_inf: entity work.bd_d10d_vfb_0_0_vfb_v1_0_11_op_inf
     port map (
      D(7 downto 0) => p_1_in(7 downto 0),
      E(0) => reorder_n_49,
      Q(7 downto 0) => sband_ts_r(7 downto 0),
      \buf_data_reg[0][100]\ => reorder_n_3,
      \buf_data_reg[0][133]\(15 downto 0) => sband_td_r(15 downto 0),
      \buf_data_reg[0][71]\(7 downto 2) => sband_ts(7 downto 2),
      \buf_data_reg[0][71]\(1 downto 0) => \sband_ts__0\(1 downto 0),
      \buf_valid_reg[0]\ => op_inf_n_9,
      cur_dtype_sink_reg => reorder_n_1,
      cur_dtype_udef => cur_dtype_udef,
      m_axis_tvalid => \^mdt_tv\,
      mdt_tr => \^mdt_tr\,
      s_axis_aresetn => s_axis_aresetn,
      s_axis_aresetn_0 => reorder_n_48,
      sband_tact0 => sband_tact0,
      sband_tl => sband_tl,
      sband_tl_r_reg_0 => op_inf_n_10,
      sband_tu => sband_tu,
      sband_tu_r => sband_tu_r,
      sdt_tr => sdt_tr,
      \state_reg[1]\ => axis_dconverter_n_2,
      \state_reg[1]_0\ => axis_dconverter_n_11,
      \state_reg[1]_1\ => axis_dconverter_n_12,
      \state_reg[1]_10\ => axis_dconverter_n_21,
      \state_reg[1]_11\ => axis_dconverter_n_22,
      \state_reg[1]_12\ => axis_dconverter_n_23,
      \state_reg[1]_13\ => axis_dconverter_n_24,
      \state_reg[1]_14\ => axis_dconverter_n_25,
      \state_reg[1]_15\ => axis_dconverter_n_26,
      \state_reg[1]_2\ => axis_dconverter_n_13,
      \state_reg[1]_3\ => axis_dconverter_n_14,
      \state_reg[1]_4\ => axis_dconverter_n_15,
      \state_reg[1]_5\ => axis_dconverter_n_16,
      \state_reg[1]_6\ => axis_dconverter_n_17,
      \state_reg[1]_7\ => axis_dconverter_n_18,
      \state_reg[1]_8\ => axis_dconverter_n_19,
      \state_reg[1]_9\ => axis_dconverter_n_20,
      vfb_clk => vfb_clk,
      vfb_data(15 downto 0) => vfb_data(15 downto 0),
      \vfb_data_reg[15]_0\(15) => op_inf_n_27,
      \vfb_data_reg[15]_0\(14) => op_inf_n_28,
      \vfb_data_reg[15]_0\(13) => op_inf_n_29,
      \vfb_data_reg[15]_0\(12) => op_inf_n_30,
      \vfb_data_reg[15]_0\(11) => op_inf_n_31,
      \vfb_data_reg[15]_0\(10) => op_inf_n_32,
      \vfb_data_reg[15]_0\(9) => op_inf_n_33,
      \vfb_data_reg[15]_0\(8) => op_inf_n_34,
      \vfb_data_reg[15]_0\(7) => op_inf_n_35,
      \vfb_data_reg[15]_0\(6) => op_inf_n_36,
      \vfb_data_reg[15]_0\(5) => op_inf_n_37,
      \vfb_data_reg[15]_0\(4) => op_inf_n_38,
      \vfb_data_reg[15]_0\(3) => op_inf_n_39,
      \vfb_data_reg[15]_0\(2) => op_inf_n_40,
      \vfb_data_reg[15]_0\(1) => op_inf_n_41,
      \vfb_data_reg[15]_0\(0) => op_inf_n_42,
      vfb_eol => \^vfb_eol\,
      vfb_eol_reg_0 => op_inf_n_6,
      vfb_eol_reg_1 => op_inf_n_7,
      vfb_ready => \^vfb_ready\,
      vfb_sof(0) => vfb_sof(0),
      \vfb_sof_reg[0]_0\ => op_inf_n_8,
      vfb_valid => \^vfb_valid\,
      vfb_vcdt(7 downto 0) => vfb_vcdt(7 downto 0)
    );
reorder: entity work.bd_d10d_vfb_0_0_vfb_v1_0_11_reorder
     port map (
      D(42) => s_axis_tlast,
      D(41 downto 10) => s_axis_tdata(31 downto 0),
      D(9) => s_axis_tkeep(2),
      D(8 downto 3) => s_axis_tuser(69 downto 64),
      D(2) => s_axis_tuser(0),
      D(1 downto 0) => s_axis_tdest(1 downto 0),
      E(0) => reorder_n_49,
      Q(24 downto 9) => \s_axis_tdata__0\(15 downto 0),
      Q(8 downto 3) => sband_ts(7 downto 2),
      Q(2) => reorder_n_44,
      Q(1 downto 0) => \sband_ts__0\(1 downto 0),
      \buf_data_reg[1][134]_0\ => reorder_n_48,
      cur_dtype_sink_reg_0 => reorder_n_1,
      cur_dtype_sink_reg_1 => op_inf_n_9,
      cur_dtype_udef => cur_dtype_udef,
      m_axis_tlast => m_axis_tlast,
      m_axis_tvalid => \^mdt_tv\,
      s_axis_aclk => s_axis_aclk,
      s_axis_aresetn => s_axis_aresetn,
      s_axis_tready => s_axis_tready,
      s_axis_tvalid => s_axis_tvalid,
      sband_tact0 => sband_tact0,
      \sband_td_r_reg[15]\(15 downto 0) => sband_td_r(15 downto 0),
      sband_tl => sband_tl,
      sband_tu => sband_tu,
      sdt_tv => sdt_tv,
      \state_reg[1]\ => op_inf_n_6,
      vfb_eol => \^vfb_eol\,
      vfb_eol_reg => reorder_n_3,
      vfb_ready => \^vfb_ready\,
      vfb_valid => \^vfb_valid\,
      vfb_valid_reg => op_inf_n_10
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_d10d_vfb_0_0 is
  port (
    s_axis_aclk : in STD_LOGIC;
    s_axis_aresetn : in STD_LOGIC;
    s_axis_tready : out STD_LOGIC;
    s_axis_tvalid : in STD_LOGIC;
    s_axis_tlast : in STD_LOGIC;
    s_axis_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_tkeep : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axis_tuser : in STD_LOGIC_VECTOR ( 95 downto 0 );
    s_axis_tdest : in STD_LOGIC_VECTOR ( 1 downto 0 );
    mdt_tv : out STD_LOGIC;
    mdt_tr : out STD_LOGIC;
    sdt_tv : out STD_LOGIC;
    sdt_tr : out STD_LOGIC;
    vfb_tv : out STD_LOGIC;
    vfb_tr : out STD_LOGIC;
    vfb_clk : in STD_LOGIC;
    vfb_ready : in STD_LOGIC;
    vfb_valid : out STD_LOGIC;
    vfb_eol : out STD_LOGIC;
    vfb_sof : out STD_LOGIC_VECTOR ( 0 to 0 );
    vfb_vcdt : out STD_LOGIC_VECTOR ( 7 downto 0 );
    vfb_data : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of bd_d10d_vfb_0_0 : entity is true;
  attribute AXIS_TDATA_WIDTH : integer;
  attribute AXIS_TDATA_WIDTH of bd_d10d_vfb_0_0 : entity is 32;
  attribute AXIS_TDEST_WIDTH : integer;
  attribute AXIS_TDEST_WIDTH of bd_d10d_vfb_0_0 : entity is 2;
  attribute AXIS_TUSER_WIDTH : integer;
  attribute AXIS_TUSER_WIDTH of bd_d10d_vfb_0_0 : entity is 96;
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of bd_d10d_vfb_0_0 : entity is "yes";
  attribute VFB_4PXL_W : integer;
  attribute VFB_4PXL_W of bd_d10d_vfb_0_0 : entity is 32;
  attribute VFB_BYPASS_WC : integer;
  attribute VFB_BYPASS_WC of bd_d10d_vfb_0_0 : entity is 0;
  attribute VFB_DATA_TYPE : integer;
  attribute VFB_DATA_TYPE of bd_d10d_vfb_0_0 : entity is 42;
  attribute VFB_DCONV_OWIDTH : integer;
  attribute VFB_DCONV_OWIDTH of bd_d10d_vfb_0_0 : entity is 16;
  attribute VFB_FIFO_DEPTH : integer;
  attribute VFB_FIFO_DEPTH of bd_d10d_vfb_0_0 : entity is 128;
  attribute VFB_FIFO_WIDTH : integer;
  attribute VFB_FIFO_WIDTH of bd_d10d_vfb_0_0 : entity is 32;
  attribute VFB_FILTER_VC : integer;
  attribute VFB_FILTER_VC of bd_d10d_vfb_0_0 : entity is 0;
  attribute VFB_OP_DWIDTH : integer;
  attribute VFB_OP_DWIDTH of bd_d10d_vfb_0_0 : entity is 16;
  attribute VFB_OP_PIXELS : integer;
  attribute VFB_OP_PIXELS of bd_d10d_vfb_0_0 : entity is 2;
  attribute VFB_PXL_W : integer;
  attribute VFB_PXL_W of bd_d10d_vfb_0_0 : entity is 8;
  attribute VFB_PXL_W_BB : integer;
  attribute VFB_PXL_W_BB of bd_d10d_vfb_0_0 : entity is 8;
  attribute VFB_REQ_BUFFER : integer;
  attribute VFB_REQ_BUFFER of bd_d10d_vfb_0_0 : entity is 0;
  attribute VFB_REQ_REORDER : integer;
  attribute VFB_REQ_REORDER of bd_d10d_vfb_0_0 : entity is 0;
  attribute VFB_TU_WIDTH : integer;
  attribute VFB_TU_WIDTH of bd_d10d_vfb_0_0 : entity is 1;
  attribute VFB_VC : integer;
  attribute VFB_VC of bd_d10d_vfb_0_0 : entity is 0;
end bd_d10d_vfb_0_0;

architecture STRUCTURE of bd_d10d_vfb_0_0 is
  attribute AXIS_TDATA_WIDTH of inst : label is 32;
  attribute AXIS_TDEST_WIDTH of inst : label is 2;
  attribute AXIS_TUSER_WIDTH of inst : label is 96;
  attribute DowngradeIPIdentifiedWarnings of inst : label is "yes";
  attribute VFB_4PXL_W of inst : label is 32;
  attribute VFB_BYPASS_WC of inst : label is 0;
  attribute VFB_DATA_TYPE of inst : label is 42;
  attribute VFB_DCONV_OWIDTH of inst : label is 16;
  attribute VFB_DCONV_TUW : integer;
  attribute VFB_DCONV_TUW of inst : label is 12;
  attribute VFB_FIFO_DEPTH of inst : label is 128;
  attribute VFB_FIFO_WIDTH of inst : label is 32;
  attribute VFB_FILTER_VC of inst : label is 0;
  attribute VFB_OP_DWIDTH of inst : label is 16;
  attribute VFB_OP_PIXELS of inst : label is 2;
  attribute VFB_PXL_W of inst : label is 8;
  attribute VFB_PXL_W_BB of inst : label is 8;
  attribute VFB_REQ_BUFFER of inst : label is 0;
  attribute VFB_REQ_REORDER of inst : label is 0;
  attribute VFB_TSB0_WIDTH : integer;
  attribute VFB_TSB0_WIDTH of inst : label is 32;
  attribute VFB_TSB1_WIDTH : integer;
  attribute VFB_TSB1_WIDTH of inst : label is 0;
  attribute VFB_TSB2_WIDTH : integer;
  attribute VFB_TSB2_WIDTH of inst : label is 3;
  attribute VFB_TU_WIDTH of inst : label is 1;
  attribute VFB_VC of inst : label is 0;
begin
inst: entity work.bd_d10d_vfb_0_0_bd_d10d_vfb_0_0_core
     port map (
      mdt_tr => mdt_tr,
      mdt_tv => mdt_tv,
      s_axis_aclk => s_axis_aclk,
      s_axis_aresetn => s_axis_aresetn,
      s_axis_tdata(31 downto 0) => s_axis_tdata(31 downto 0),
      s_axis_tdest(1 downto 0) => s_axis_tdest(1 downto 0),
      s_axis_tkeep(3 downto 0) => s_axis_tkeep(3 downto 0),
      s_axis_tlast => s_axis_tlast,
      s_axis_tready => s_axis_tready,
      s_axis_tuser(95 downto 0) => s_axis_tuser(95 downto 0),
      s_axis_tvalid => s_axis_tvalid,
      sdt_tr => sdt_tr,
      sdt_tv => sdt_tv,
      vfb_clk => vfb_clk,
      vfb_data(15 downto 0) => vfb_data(15 downto 0),
      vfb_eol => vfb_eol,
      vfb_ready => vfb_ready,
      vfb_sof(0) => vfb_sof(0),
      vfb_tr => vfb_tr,
      vfb_tv => vfb_tv,
      vfb_valid => vfb_valid,
      vfb_vcdt(7 downto 0) => vfb_vcdt(7 downto 0)
    );
end STRUCTURE;
