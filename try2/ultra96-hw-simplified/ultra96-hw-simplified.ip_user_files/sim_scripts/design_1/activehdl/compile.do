vlib work
vlib activehdl

vlib activehdl/xilinx_vip
vlib activehdl/xil_defaultlib
vlib activehdl/xpm
vlib activehdl/axi_infrastructure_v1_1_0
vlib activehdl/smartconnect_v1_0
vlib activehdl/axi_protocol_checker_v2_0_3
vlib activehdl/axi_vip_v1_1_3
vlib activehdl/zynq_ultra_ps_e_vip_v1_0_3
vlib activehdl/lib_cdc_v1_0_2
vlib activehdl/proc_sys_reset_v5_0_12
vlib activehdl/generic_baseblocks_v2_1_0
vlib activehdl/axi_register_slice_v2_1_17
vlib activehdl/fifo_generator_v13_2_2
vlib activehdl/axi_data_fifo_v2_1_16
vlib activehdl/axi_crossbar_v2_1_18
vlib activehdl/mipi_csi2_rx_ctrl_v1_0_8
vlib activehdl/high_speed_selectio_wiz_v3_3_1
vlib activehdl/mipi_dphy_v4_1_1
vlib activehdl/axis_infrastructure_v1_1_0
vlib activehdl/axis_register_slice_v1_1_17
vlib activehdl/axis_dwidth_converter_v1_1_16
vlib activehdl/vfb_v1_0_11
vlib activehdl/v_demosaic_v1_0_3
vlib activehdl/v_gamma_lut_v1_0_3
vlib activehdl/v_csc_v1_0_11
vlib activehdl/v_vscaler_v1_0_11
vlib activehdl/v_hscaler_v1_0_11
vlib activehdl/axis_subset_converter_v1_1_17
vlib activehdl/axi_lite_ipif_v3_0_4
vlib activehdl/interrupt_control_v3_1_4
vlib activehdl/axi_gpio_v2_0_19
vlib activehdl/axis_data_fifo_v1_1_18
vlib activehdl/xlslice_v1_0_1
vlib activehdl/axi_clock_converter_v2_1_16
vlib activehdl/v_frmbuf_wr_v2_1_0
vlib activehdl/axi_intc_v4_1_11
vlib activehdl/xlconcat_v2_1_1
vlib activehdl/axi_protocol_converter_v2_1_17
vlib activehdl/blk_mem_gen_v8_4_1
vlib activehdl/axi_dwidth_converter_v2_1_17

vmap xilinx_vip activehdl/xilinx_vip
vmap xil_defaultlib activehdl/xil_defaultlib
vmap xpm activehdl/xpm
vmap axi_infrastructure_v1_1_0 activehdl/axi_infrastructure_v1_1_0
vmap smartconnect_v1_0 activehdl/smartconnect_v1_0
vmap axi_protocol_checker_v2_0_3 activehdl/axi_protocol_checker_v2_0_3
vmap axi_vip_v1_1_3 activehdl/axi_vip_v1_1_3
vmap zynq_ultra_ps_e_vip_v1_0_3 activehdl/zynq_ultra_ps_e_vip_v1_0_3
vmap lib_cdc_v1_0_2 activehdl/lib_cdc_v1_0_2
vmap proc_sys_reset_v5_0_12 activehdl/proc_sys_reset_v5_0_12
vmap generic_baseblocks_v2_1_0 activehdl/generic_baseblocks_v2_1_0
vmap axi_register_slice_v2_1_17 activehdl/axi_register_slice_v2_1_17
vmap fifo_generator_v13_2_2 activehdl/fifo_generator_v13_2_2
vmap axi_data_fifo_v2_1_16 activehdl/axi_data_fifo_v2_1_16
vmap axi_crossbar_v2_1_18 activehdl/axi_crossbar_v2_1_18
vmap mipi_csi2_rx_ctrl_v1_0_8 activehdl/mipi_csi2_rx_ctrl_v1_0_8
vmap high_speed_selectio_wiz_v3_3_1 activehdl/high_speed_selectio_wiz_v3_3_1
vmap mipi_dphy_v4_1_1 activehdl/mipi_dphy_v4_1_1
vmap axis_infrastructure_v1_1_0 activehdl/axis_infrastructure_v1_1_0
vmap axis_register_slice_v1_1_17 activehdl/axis_register_slice_v1_1_17
vmap axis_dwidth_converter_v1_1_16 activehdl/axis_dwidth_converter_v1_1_16
vmap vfb_v1_0_11 activehdl/vfb_v1_0_11
vmap v_demosaic_v1_0_3 activehdl/v_demosaic_v1_0_3
vmap v_gamma_lut_v1_0_3 activehdl/v_gamma_lut_v1_0_3
vmap v_csc_v1_0_11 activehdl/v_csc_v1_0_11
vmap v_vscaler_v1_0_11 activehdl/v_vscaler_v1_0_11
vmap v_hscaler_v1_0_11 activehdl/v_hscaler_v1_0_11
vmap axis_subset_converter_v1_1_17 activehdl/axis_subset_converter_v1_1_17
vmap axi_lite_ipif_v3_0_4 activehdl/axi_lite_ipif_v3_0_4
vmap interrupt_control_v3_1_4 activehdl/interrupt_control_v3_1_4
vmap axi_gpio_v2_0_19 activehdl/axi_gpio_v2_0_19
vmap axis_data_fifo_v1_1_18 activehdl/axis_data_fifo_v1_1_18
vmap xlslice_v1_0_1 activehdl/xlslice_v1_0_1
vmap axi_clock_converter_v2_1_16 activehdl/axi_clock_converter_v2_1_16
vmap v_frmbuf_wr_v2_1_0 activehdl/v_frmbuf_wr_v2_1_0
vmap axi_intc_v4_1_11 activehdl/axi_intc_v4_1_11
vmap xlconcat_v2_1_1 activehdl/xlconcat_v2_1_1
vmap axi_protocol_converter_v2_1_17 activehdl/axi_protocol_converter_v2_1_17
vmap blk_mem_gen_v8_4_1 activehdl/blk_mem_gen_v8_4_1
vmap axi_dwidth_converter_v2_1_17 activehdl/axi_dwidth_converter_v2_1_17

vlog -work xilinx_vip  -sv2k12 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/hdl/axi4stream_vip_axi4streampc.sv" \
"/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/hdl/axi_vip_axi4pc.sv" \
"/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/hdl/xil_common_vip_pkg.sv" \
"/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/hdl/axi4stream_vip_pkg.sv" \
"/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/hdl/axi_vip_pkg.sv" \
"/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/hdl/axi4stream_vip_if.sv" \
"/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/hdl/axi_vip_if.sv" \
"/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/hdl/clk_vip_if.sv" \
"/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/hdl/rst_vip_if.sv" \

vlog -work xil_defaultlib  -sv2k12 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"/tools/Xilinx/Vivado/2018.2/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"/tools/Xilinx/Vivado/2018.2/data/ip/xpm/xpm_fifo/hdl/xpm_fifo.sv" \
"/tools/Xilinx/Vivado/2018.2/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -93 \
"/tools/Xilinx/Vivado/2018.2/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work axi_infrastructure_v1_1_0  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl/axi_infrastructure_v1_1_vl_rfs.v" \

vlog -work smartconnect_v1_0  -sv2k12 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/sc_util_v1_0_vl_rfs.sv" \

vlog -work axi_protocol_checker_v2_0_3  -sv2k12 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/03a9/hdl/axi_protocol_checker_v2_0_vl_rfs.sv" \

vlog -work axi_vip_v1_1_3  -sv2k12 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b9a8/hdl/axi_vip_v1_1_vl_rfs.sv" \

vlog -work zynq_ultra_ps_e_vip_v1_0_3  -sv2k12 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl/zynq_ultra_ps_e_vip_v1_0_vl_rfs.sv" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_zynq_ultra_ps_e_0_0/sim/design_1_zynq_ultra_ps_e_0_0_vip_wrapper.v" \

vcom -work lib_cdc_v1_0_2 -93 \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ef1e/hdl/lib_cdc_v1_0_rfs.vhd" \

vcom -work proc_sys_reset_v5_0_12 -93 \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/f86a/hdl/proc_sys_reset_v5_0_vh_rfs.vhd" \

vcom -work xil_defaultlib -93 \
"../../../bd/design_1/ip/design_1_proc_sys_reset_0_0/sim/design_1_proc_sys_reset_0_0.vhd" \

vlog -work generic_baseblocks_v2_1_0  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b752/hdl/generic_baseblocks_v2_1_vl_rfs.v" \

vlog -work axi_register_slice_v2_1_17  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/6020/hdl/axi_register_slice_v2_1_vl_rfs.v" \

vlog -work fifo_generator_v13_2_2  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/7aff/simulation/fifo_generator_vlog_beh.v" \

vcom -work fifo_generator_v13_2_2 -93 \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/7aff/hdl/fifo_generator_v13_2_rfs.vhd" \

vlog -work fifo_generator_v13_2_2  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/7aff/hdl/fifo_generator_v13_2_rfs.v" \

vlog -work axi_data_fifo_v2_1_16  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/247d/hdl/axi_data_fifo_v2_1_vl_rfs.v" \

vlog -work axi_crossbar_v2_1_18  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/15a3/hdl/axi_crossbar_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/bd_0/ip/ip_0/sim/bd_d10d_xbar_0.v" \

vcom -work xil_defaultlib -93 \
"../../../bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/bd_0/ip/ip_1/sim/bd_d10d_r_sync_0.vhd" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/bd_0/ip/ip_2/ip_0/sim/mipi_csi2_rx_ctrl_v1_0_8_fifo1.v" \
"../../../bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/bd_0/ip/ip_2/ip_1/sim/mipi_csi2_rx_ctrl_v1_0_8_fc_324096.v" \
"../../../bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/bd_0/ip/ip_2/ip_2/sim/mipi_csi2_rx_ctrl_v1_0_8_fifo0.v" \
"../../../bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/bd_0/ip/ip_2/ip_3/sim/mipi_csi2_rx_ctrl_v1_0_8_fifo2.v" \
"../../../bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/bd_0/ip/ip_2/mipi_csi2_rx_ctrl_v1_0_8_fc5.v" \

vlog -work mipi_csi2_rx_ctrl_v1_0_8  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl/mipi_csi2_rx_ctrl_v1_0_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/bd_0/ip/ip_2/sim/bd_d10d_rx_0.v" \

vlog -work high_speed_selectio_wiz_v3_3_1  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5912/hdl/high_speed_selectio_wiz_v3_3_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/bd_0/ip/ip_3/ip_0/hdl/bd_d10d_phy_0_hssio_rx_mipi_iobuf_rx.v" \
"../../../bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/bd_0/ip/ip_3/ip_0/bd_d10d_phy_0_hssio_rx_hssio_wiz_top.v" \
"../../../bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/bd_0/ip/ip_3/ip_0/bd_d10d_phy_0_hssio_rx_high_speed_selectio_wiz_v3_3_1.v" \
"../../../bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/bd_0/ip/ip_3/ip_0/sim/bd_d10d_phy_0_hssio_rx.v" \

vlog -work mipi_dphy_v4_1_1  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/27d0/hdl/mipi_dphy_v4_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/bd_0/ip/ip_3/bd_d10d_phy_0/support/bd_d10d_phy_0_support.v" \
"../../../bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/bd_0/ip/ip_3/bd_d10d_phy_0/support/bd_d10d_phy_0_clock_module.v" \
"../../../bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/bd_0/ip/ip_3/bd_d10d_phy_0_core.v" \
"../../../bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/bd_0/ip/ip_3/bd_d10d_phy_0.v" \

vlog -work axis_infrastructure_v1_1_0  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl/axis_infrastructure_v1_1_vl_rfs.v" \

vlog -work axis_register_slice_v1_1_17  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/15d7/hdl/axis_register_slice_v1_1_vl_rfs.v" \

vlog -work axis_dwidth_converter_v1_1_16  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/64f6/hdl/axis_dwidth_converter_v1_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/bd_0/ip/ip_4/ip_0/sim/bd_d10d_vfb_0_0_axis_converter.v" \
"../../../bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/bd_0/ip/ip_4/bd_d10d_vfb_0_0/src/verilog/bd_d10d_vfb_0_0_axis_dconverter.v" \

vlog -work vfb_v1_0_11  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/d4a9/hdl/vfb_v1_0_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/bd_0/ip/ip_4/bd_d10d_vfb_0_0_core.v" \
"../../../bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/bd_0/ip/ip_4/bd_d10d_vfb_0_0.v" \
"../../../bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/bd_0/sim/bd_d10d.v" \
"../../../bd/design_1/ip/design_1_mipi_csi2_rx_subsyst_0_0/sim/design_1_mipi_csi2_rx_subsyst_0_0.v" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/hdl/verilog/design_1_v_demosaic_0_0_AXIvideo2MultiBayer3.v" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/hdl/verilog/design_1_v_demosaic_0_0_Debayer.v" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/hdl/verilog/design_1_v_demosaic_0_0_DebayerG.v" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/hdl/verilog/design_1_v_demosaic_0_0_DebayerG_DIV1_TABLE.v" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/hdl/verilog/design_1_v_demosaic_0_0_DebayerG_DIV2_TABLE.v" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/hdl/verilog/design_1_v_demosaic_0_0_DebayerG_linebuf_bkb.v" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/hdl/verilog/design_1_v_demosaic_0_0_DebayerRandBatG.v" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/hdl/verilog/design_1_v_demosaic_0_0_DebayerRatBorBatR.v" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/hdl/verilog/design_1_v_demosaic_0_0_DebayerRatBorBatRkbM.v" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/hdl/verilog/design_1_v_demosaic_0_0_fifo_w8_d2_A.v" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/hdl/verilog/design_1_v_demosaic_0_0_fifo_w8_d2_A_x.v" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/hdl/verilog/design_1_v_demosaic_0_0_fifo_w16_d2_A.v" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/hdl/verilog/design_1_v_demosaic_0_0_fifo_w16_d2_A_x.v" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/hdl/verilog/design_1_v_demosaic_0_0_MultiPixStream2AXIvi.v" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/hdl/verilog/design_1_v_demosaic_0_0_reg_unsigned_short_s.v" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/hdl/verilog/design_1_v_demosaic_0_0_v_demosaic_CTRL_s_axi.v" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/hdl/verilog/design_1_v_demosaic_0_0_v_demosaic_mac_mujbC.v" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/hdl/verilog/design_1_v_demosaic_0_0_v_demosaic_mul_mug8j.v" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/hdl/verilog/design_1_v_demosaic_0_0_v_demosaic_mul_muhbi.v" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/hdl/verilog/design_1_v_demosaic_0_0_v_demosaic_mul_muibs.v" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/hdl/verilog/design_1_v_demosaic_0_0_v_demosaic_mux_32mb6.v" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/hdl/verilog/design_1_v_demosaic_0_0_v_demosaic_mux_53fYi.v" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/hdl/verilog/design_1_v_demosaic_0_0_ZipperRemoval.v" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/hdl/verilog/design_1_v_demosaic_0_0_v_demosaic.v" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/hdl/verilog/design_1_v_demosaic_0_0_v_demosaic_mac_muibs.v" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/hdl/verilog/design_1_v_demosaic_0_0_v_demosaic_mul_mujbC.v" \

vlog -work v_demosaic_v1_0_3  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ip/design_1_v_demosaic_0_0/hdl/v_demosaic_v1_0_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/sim/design_1_v_demosaic_0_0.v" \
"../../../bd/design_1/ip/design_1_v_gamma_lut_0_0/hdl/verilog/design_1_v_gamma_lut_0_0_AXIvideo2MultiPixStr.v" \
"../../../bd/design_1/ip/design_1_v_gamma_lut_0_0/hdl/verilog/design_1_v_gamma_lut_0_0_fifo_w8_d2_A.v" \
"../../../bd/design_1/ip/design_1_v_gamma_lut_0_0/hdl/verilog/design_1_v_gamma_lut_0_0_Gamma.v" \
"../../../bd/design_1/ip/design_1_v_gamma_lut_0_0/hdl/verilog/design_1_v_gamma_lut_0_0_Gamma_lut_0_0_V.v" \
"../../../bd/design_1/ip/design_1_v_gamma_lut_0_0/hdl/verilog/design_1_v_gamma_lut_0_0_MultiPixStream2AXIvi.v" \
"../../../bd/design_1/ip/design_1_v_gamma_lut_0_0/hdl/verilog/design_1_v_gamma_lut_0_0_reg_unsigned_short_s.v" \
"../../../bd/design_1/ip/design_1_v_gamma_lut_0_0/hdl/verilog/design_1_v_gamma_lut_0_0_v_gamma_lut_CTRL_s_axi.v" \
"../../../bd/design_1/ip/design_1_v_gamma_lut_0_0/hdl/verilog/design_1_v_gamma_lut_0_0_v_gamma_lut.v" \

vlog -work v_gamma_lut_v1_0_3  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ip/design_1_v_gamma_lut_0_0/hdl/v_gamma_lut_v1_0_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_v_gamma_lut_0_0/sim/design_1_v_gamma_lut_0_0.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_0/bd_0/ip/ip_0/hdl/verilog/bd_d92b_csc_0_AXIvideo2MultiPixStr.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_0/bd_0/ip/ip_0/hdl/verilog/bd_d92b_csc_0_fifo_w8_d2_A.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_0/bd_0/ip/ip_0/hdl/verilog/bd_d92b_csc_0_MultiPixStream2AXIvi.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_0/bd_0/ip/ip_0/hdl/verilog/bd_d92b_csc_0_reg_unsigned_short_s.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_0/bd_0/ip/ip_0/hdl/verilog/bd_d92b_csc_0_v_csc_core.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_0/bd_0/ip/ip_0/hdl/verilog/bd_d92b_csc_0_v_csc_CTRL_s_axi.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_0/bd_0/ip/ip_0/hdl/verilog/bd_d92b_csc_0_v_csc_mac_muladd_cud.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_0/bd_0/ip/ip_0/hdl/verilog/bd_d92b_csc_0_v_csc_mac_muladd_dEe.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_0/bd_0/ip/ip_0/hdl/verilog/bd_d92b_csc_0_v_csc_mul_mul_8nsbkb.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_0/bd_0/ip/ip_0/hdl/verilog/bd_d92b_csc_0_v_csc.v" \

vlog -work v_csc_v1_0_11  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ip/design_1_v_proc_ss_0_0/bd_0/ip/ip_0/hdl/v_csc_v1_0_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_0/bd_0/ip/ip_0/sim/bd_d92b_csc_0.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_0/bd_0/sim/bd_d92b.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_0/sim/design_1_v_proc_ss_0_0.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_2/hdl/verilog/bd_19ea_vsc_0_AXIvideo2MultiPixStr.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_2/hdl/verilog/bd_19ea_vsc_0_Block_crit_edge69_s.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_2/hdl/verilog/bd_19ea_vsc_0_fifo_w8_d2_A.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_2/hdl/verilog/bd_19ea_vsc_0_fifo_w8_d3_A.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_2/hdl/verilog/bd_19ea_vsc_0_fifo_w11_d3_A.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_2/hdl/verilog/bd_19ea_vsc_0_MultiPixStream2AXIvi.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_2/hdl/verilog/bd_19ea_vsc_0_reg_unsigned_short_s.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_2/hdl/verilog/bd_19ea_vsc_0_v_vscaler_CTRL_s_axi.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_2/hdl/verilog/bd_19ea_vsc_0_v_vscaler_mac_mulsc4.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_2/hdl/verilog/bd_19ea_vsc_0_v_vscaler_mac_multde.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_2/hdl/verilog/bd_19ea_vsc_0_v_vscaler_mac_muludo.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_2/hdl/verilog/bd_19ea_vsc_0_v_vscaler_mac_mulvdy.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_2/hdl/verilog/bd_19ea_vsc_0_v_vscaler_mac_mulwdI.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_2/hdl/verilog/bd_19ea_vsc_0_v_vscaler_mac_mulxdS.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_2/hdl/verilog/bd_19ea_vsc_0_v_vscaler_mux_83_rcU.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_2/hdl/verilog/bd_19ea_vsc_0_vscale_core_polypbkb.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_2/hdl/verilog/bd_19ea_vsc_0_vscale_core_polyphas.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_2/hdl/verilog/bd_19ea_vsc_0_vscale_core_polypjbC.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_2/hdl/verilog/bd_19ea_vsc_0_v_vscaler.v" \

vlog -work v_vscaler_v1_0_11  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_2/hdl/v_vscaler_v1_0_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_2/sim/bd_19ea_vsc_0.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_AXIvideo2MultiPixStr.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_Block_crit_edge23_s.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_Block_proc.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_Block_proc115.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_fifo_w1_d2_A.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_fifo_w1_d4_A.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_fifo_w1_d5_A.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_fifo_w8_d2_A.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_fifo_w8_d4_A.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_fifo_w8_d7_A.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_fifo_w11_d2_A.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_fifo_w12_d2_A.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_fifo_w12_d4_A.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_fifo_w16_d2_A.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_hscale_core_polyphas.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_hscale_core_polypkbM.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_hscale_core_polyplbW.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_hscale_core_polypmb6.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_hscale_polyphase.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_MultiPixStream2AXIvi.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_MultiPixStream2AXLf8.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_reg_ap_uint_18_s.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_start_for_Block_cPgM.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_start_for_Block_pNgs.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_start_for_Block_pOgC.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_start_for_MultiPiQgW.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_start_for_v_csc_cRg6.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_start_for_v_hcresShg.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_csc_core.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hcresampler_core11.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hcresampler_core11_1.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler_am_submJfO.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler_CTRL_s_axi.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler_entry334.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler_mac_muldEe.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler_mac_mulDeQ.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler_mac_muleOg.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler_mac_mulfYi.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler_mac_mulg8j.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler_mac_mulGfk.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler_mac_mulhbi.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler_mac_mulHfu.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler_mac_mulibs.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler_mac_muljbC.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler_mul_mulCeG.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler_mul_mulEe0.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler_mul_mulFfa.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler_mul_mulIfE.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler_mux_63_Mgi.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler_mux_83_KfY.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler_mux_134bkb.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler_mux_164cud.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler_mac_mulFfa.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler_mac_mulIfE.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler_mul_mulGfk.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler_mul_mulHfu.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/verilog/bd_19ea_hsc_0_v_hscaler_mac_mulEe0.v" \

vlog -work v_hscaler_v1_0_11  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/hdl/v_hscaler_v1_0_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/sim/bd_19ea_hsc_0.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_4/hdl/tdata_bd_19ea_input_size_set_0.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_4/hdl/tuser_bd_19ea_input_size_set_0.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_4/hdl/tstrb_bd_19ea_input_size_set_0.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_4/hdl/tkeep_bd_19ea_input_size_set_0.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_4/hdl/tid_bd_19ea_input_size_set_0.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_4/hdl/tdest_bd_19ea_input_size_set_0.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_4/hdl/tlast_bd_19ea_input_size_set_0.v" \

vlog -work axis_subset_converter_v1_1_17  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5a7d/hdl/axis_subset_converter_v1_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_4/hdl/top_bd_19ea_input_size_set_0.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_4/sim/bd_19ea_input_size_set_0.v" \

vcom -work xil_defaultlib -93 \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_5/sim/bd_19ea_rst_axis_0.vhd" \

vcom -work axi_lite_ipif_v3_0_4 -93 \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/cced/hdl/axi_lite_ipif_v3_0_vh_rfs.vhd" \

vcom -work interrupt_control_v3_1_4 -93 \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/8e66/hdl/interrupt_control_v3_1_vh_rfs.vhd" \

vcom -work axi_gpio_v2_0_19 -93 \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/c193/hdl/axi_gpio_v2_0_vh_rfs.vhd" \

vcom -work xil_defaultlib -93 \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_6/sim/bd_19ea_reset_sel_axis_0.vhd" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_7/sim/bd_19ea_axis_register_slice_0_0.v" \

vlog -work axis_data_fifo_v1_1_18  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5738/hdl/axis_data_fifo_v1_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_8/sim/bd_19ea_axis_fifo_0.v" \

vlog -work xlslice_v1_0_1  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/f3db/hdl/xlslice_v1_0_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_9/sim/bd_19ea_xlslice_0_0.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_10/sim/bd_19ea_xlslice_1_0.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_1/sim/bd_19ea_xbar_0.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/sim/bd_19ea.v" \

vlog -work axi_clock_converter_v2_1_16  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e9a5/hdl/axi_clock_converter_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_13/sim/bd_19ea_auto_cc_2.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_12/sim/bd_19ea_auto_cc_1.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_11/sim/bd_19ea_auto_cc_0.v" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/design_1_v_proc_ss_0_1_sim_netlist.v" \
"../../../bd/design_1/ip/design_1_v_frmbuf_wr_0_0/hdl/verilog/design_1_v_frmbuf_wr_0_0_AXIvideo2MultiPixStr.v" \
"../../../bd/design_1/ip/design_1_v_frmbuf_wr_0_0/hdl/verilog/design_1_v_frmbuf_wr_0_0_Block_proc4.v" \
"../../../bd/design_1/ip/design_1_v_frmbuf_wr_0_0/hdl/verilog/design_1_v_frmbuf_wr_0_0_Block_proc4_BYTESbkb.v" \
"../../../bd/design_1/ip/design_1_v_frmbuf_wr_0_0/hdl/verilog/design_1_v_frmbuf_wr_0_0_Block_proc4_MEMORcud.v" \
"../../../bd/design_1/ip/design_1_v_frmbuf_wr_0_0/hdl/verilog/design_1_v_frmbuf_wr_0_0_Block_proc58.v" \
"../../../bd/design_1/ip/design_1_v_frmbuf_wr_0_0/hdl/verilog/design_1_v_frmbuf_wr_0_0_Bytes2AXIMMvideo.v" \
"../../../bd/design_1/ip/design_1_v_frmbuf_wr_0_0/hdl/verilog/design_1_v_frmbuf_wr_0_0_fifo_w8_d2_A.v" \
"../../../bd/design_1/ip/design_1_v_frmbuf_wr_0_0/hdl/verilog/design_1_v_frmbuf_wr_0_0_fifo_w12_d2_A.v" \
"../../../bd/design_1/ip/design_1_v_frmbuf_wr_0_0/hdl/verilog/design_1_v_frmbuf_wr_0_0_fifo_w16_d2_A.v" \
"../../../bd/design_1/ip/design_1_v_frmbuf_wr_0_0/hdl/verilog/design_1_v_frmbuf_wr_0_0_fifo_w32_d4_A.v" \
"../../../bd/design_1/ip/design_1_v_frmbuf_wr_0_0/hdl/verilog/design_1_v_frmbuf_wr_0_0_fifo_w128_d480_B.v" \
"../../../bd/design_1/ip/design_1_v_frmbuf_wr_0_0/hdl/verilog/design_1_v_frmbuf_wr_0_0_MultiPixStream2Bytes.v" \
"../../../bd/design_1/ip/design_1_v_frmbuf_wr_0_0/hdl/verilog/design_1_v_frmbuf_wr_0_0_reg_unsigned_short_s.v" \
"../../../bd/design_1/ip/design_1_v_frmbuf_wr_0_0/hdl/verilog/design_1_v_frmbuf_wr_0_0_v_frmbuf_wr_CTRL_s_axi.v" \
"../../../bd/design_1/ip/design_1_v_frmbuf_wr_0_0/hdl/verilog/design_1_v_frmbuf_wr_0_0_v_frmbuf_wr_mm_video_m_axi.v" \
"../../../bd/design_1/ip/design_1_v_frmbuf_wr_0_0/hdl/verilog/design_1_v_frmbuf_wr_0_0_v_frmbuf_wr_mul_mdEe.v" \
"../../../bd/design_1/ip/design_1_v_frmbuf_wr_0_0/hdl/verilog/design_1_v_frmbuf_wr_0_0_v_frmbuf_wr.v" \

vlog -work v_frmbuf_wr_v2_1_0  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ip/design_1_v_frmbuf_wr_0_0/hdl/v_frmbuf_wr_v2_1_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_v_frmbuf_wr_0_0/sim/design_1_v_frmbuf_wr_0_0.v" \
"../../../bd/design_1/ip/design_1_xlslice_0_1/sim/design_1_xlslice_0_1.v" \
"../../../bd/design_1/ip/design_1_xlslice_1_0/sim/design_1_xlslice_1_0.v" \
"../../../bd/design_1/ip/design_1_xlslice_2_0/sim/design_1_xlslice_2_0.v" \
"../../../bd/design_1/ip/design_1_xlslice_2_1/sim/design_1_xlslice_2_1.v" \
"../../../bd/design_1/ip/design_1_xlslice_2_2/sim/design_1_xlslice_2_2.v" \
"../../../bd/design_1/ip/design_1_clk_wiz_0_0/design_1_clk_wiz_0_0_clk_wiz.v" \
"../../../bd/design_1/ip/design_1_clk_wiz_0_0/design_1_clk_wiz_0_0.v" \

vcom -work xil_defaultlib -93 \
"../../../bd/design_1/ip/design_1_proc_sys_reset_1_0/sim/design_1_proc_sys_reset_1_0.vhd" \
"../../../bd/design_1/ip/design_1_proc_sys_reset_2_0/sim/design_1_proc_sys_reset_2_0.vhd" \

vcom -work axi_intc_v4_1_11 -93 \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/2fec/hdl/axi_intc_v4_1_vh_rfs.vhd" \

vcom -work xil_defaultlib -93 \
"../../../bd/design_1/ip/design_1_axi_intc_0_0/sim/design_1_axi_intc_0_0.vhd" \

vlog -work xlconcat_v2_1_1  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/2f66/hdl/xlconcat_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_xlconcat_0_0/sim/design_1_xlconcat_0_0.v" \
"../../../bd/design_1/ip/design_1_xbar_0/sim/design_1_xbar_0.v" \
"../../../bd/design_1/ip/design_1_xbar_1/sim/design_1_xbar_1.v" \
"../../../bd/design_1/sim/design_1.v" \

vlog -work axi_protocol_converter_v2_1_17  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ccfb/hdl/axi_protocol_converter_v2_1_vl_rfs.v" \

vlog -work blk_mem_gen_v8_4_1  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/67d8/simulation/blk_mem_gen_v8_4.v" \

vlog -work axi_dwidth_converter_v2_1_17  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/2839/hdl/axi_dwidth_converter_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/5bb9/hdl/verilog" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/e4d1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b833/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/0ab1/hdl" "+incdir+../../../../ultra96-hw-simplified.srcs/sources_1/bd/design_1/ipshared/b65a" "+incdir+/tools/Xilinx/Vivado/2018.2/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_auto_ds_4/sim/design_1_auto_ds_4.v" \
"../../../bd/design_1/ip/design_1_auto_pc_5/sim/design_1_auto_pc_5.v" \
"../../../bd/design_1/ip/design_1_auto_ds_3/sim/design_1_auto_ds_3.v" \
"../../../bd/design_1/ip/design_1_auto_pc_4/sim/design_1_auto_pc_4.v" \
"../../../bd/design_1/ip/design_1_auto_ds_2/sim/design_1_auto_ds_2.v" \
"../../../bd/design_1/ip/design_1_auto_pc_3/sim/design_1_auto_pc_3.v" \
"../../../bd/design_1/ip/design_1_auto_ds_1/sim/design_1_auto_ds_1.v" \
"../../../bd/design_1/ip/design_1_auto_pc_2/sim/design_1_auto_pc_2.v" \
"../../../bd/design_1/ip/design_1_auto_ds_0/sim/design_1_auto_ds_0.v" \
"../../../bd/design_1/ip/design_1_auto_pc_1/sim/design_1_auto_pc_1.v" \
"../../../bd/design_1/ip/design_1_auto_pc_0/sim/design_1_auto_pc_0.v" \

 \
"../../../bd/design_1/ip/design_1_v_demosaic_0_0/src/v_demosaic.cpp" \
"../../../bd/design_1/ip/design_1_v_gamma_lut_0_0/src/v_gamma_lut.cpp" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_0/bd_0/ip/ip_0/src/v_csc.cpp" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_2/src/v_vscaler.cpp" \
"../../../bd/design_1/ip/design_1_v_proc_ss_0_1/bd_0/ip/ip_3/src/v_hscaler.cpp" \
"../../../bd/design_1/ip/design_1_v_frmbuf_wr_0_0/src/v_frmbuf_wr.cpp" \

vlog -work xil_defaultlib \
"glbl.v"

